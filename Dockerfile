FROM node:12.13.1
# Install yarrn
RUN curl -o- -L https://yarnpkg.com/install.sh | bash
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

WORKDIR /usr/src/app

# Install package
RUN npm cache verify
RUN npm i -g @adonisjs/cli
RUN npm i -g npm
COPY package*.json ./
RUN npm install -f
COPY . ./

# Added ace commands
#RUN node ace
#RUN node ace migration:run --force

EXPOSE 3333
CMD [ "npm", "run", "start" ]
# CMD ["yarn", "-s", "build", "-l", "3005"]
