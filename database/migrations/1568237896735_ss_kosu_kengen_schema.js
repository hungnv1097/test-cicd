'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SSKosuKengenSchema extends Schema {
  up () {
    this.table('s_s_kosu_kengen', (table) => {
      this.raw("ALTER TABLE s_s_kosu_kengen DROP COLUMN id CASCADE;")
      this.raw("ALTER TABLE s_s_kosu_kengen ADD CONSTRAINT foreign_key_to_sm_han FOREIGN KEY (han_id) REFERENCES s_m_han (id);")
    })
  }

  down () {
    this.table('s_s_kosu_kengen', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SSKosuKengenSchema
