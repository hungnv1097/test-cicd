'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SMSyokouteiSyuruiSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_syurui DROP CONSTRAINT s_m_syokoutei_syurui_pkey");
      this.raw(`ALTER TABLE s_m_syokoutei_syurui ADD PRIMARY KEY (syo_koutei_syurui_seq, seihin_code, koutei_code)`)
    })
  }

  down () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SMSyokouteiSyuruiSchema
