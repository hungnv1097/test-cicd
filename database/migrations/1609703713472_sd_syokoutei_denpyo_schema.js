'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SDSyokouteiDenpyoSchema extends Schema {
  up () {
    this.table('s_d_syokoutei_denpyo', (table) => {
      // alter table
      this.raw("ALTER TABLE s_d_syokoutei_denpyo ADD COLUMN syo_koutei_syurui_seq INTEGER")
      this.raw("ALTER TABLE s_d_syokoutei_denpyo ADD COLUMN syo_koutei_koho_seq INTEGER")
    })
  }

  down () {
    this.table('s_d_syokoutei_denpyo', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_d_syokoutei_denpyo DROP COLUMN syo_koutei_syurui_seq")
      this.raw("ALTER TABLE s_d_syokoutei_denpyo DROP COLUMN syo_koutei_koho_seq")
    })
  }
}

module.exports = SDSyokouteiDenpyoSchema
