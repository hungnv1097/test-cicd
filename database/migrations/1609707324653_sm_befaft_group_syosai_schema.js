'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SMBefaftGroupSyosaiSchema extends Schema {
  up() {
    this.table('s_m_befaft_group_syosai', (table) => {
      // alter table
      // this.raw("ALTER TABLE s_m_befaft_group_syosai ADD COLUMN syo_koutei_syurui_seq INTEGER")
      this.raw("ALTER TABLE s_m_befaft_group_syosai ADD PRIMARY KEY (befaft_group_id, seihin_code, koutei_code, syo_koutei_code)")
    })
  }

  down() {
    this.table('s_m_befaft_group_syosai', (table) => {
      // reverse alternations
      // this.raw("ALTER TABLE s_m_befaft_group_syosai DROP COLUMN syo_koutei_syurui_seq")
      // this.raw("ALTER TABLE s_m_befaft_group_syosai DROP COLUMN syo_koutei_koho_seq")
    })
  }
}

module.exports = SMBefaftGroupSyosaiSchema
