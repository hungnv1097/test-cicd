'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WorksetSchema extends Schema {
  up () {
    this.table('workset', (table) => {
      this.raw("ALTER TABLE workset ADD CONSTRAINT workset_id_unique UNIQUE (workset_id);")
    })
  }

  down () {
    this.table('worksets', (table) => {
      // reverse alternations
    })
  }
}

module.exports = WorksetSchema
