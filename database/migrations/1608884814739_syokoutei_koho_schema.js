'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SyokouteiKohoSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_koho', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_koho ADD COLUMN created_at timestamp")
      this.raw("ALTER TABLE s_m_syokoutei_koho ADD COLUMN updated_at timestamp")
    })
  }

  down () {
    this.table('s_m_syokoutei_koho', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_m_syokoutei_koho DROP COLUMN created_at")
      this.raw("ALTER TABLE s_m_syokoutei_koho DROP COLUMN updated_at")
    })
  }
}

module.exports = SyokouteiKohoSchema
