'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SmSyokouteiSyuruiJunjoSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_syurui ADD COLUMN syokoutei_syurui_junjo INTEGER");
    })
  }

  down () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_m_syokoutei_syurui DROP COLUMN syokoutei_syurui_junjo");
    })
  }

}

module.exports = SmSyokouteiSyuruiJunjoSchema
