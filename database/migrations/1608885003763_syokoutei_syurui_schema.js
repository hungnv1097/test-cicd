'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SyokouteiSyuruiSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_syurui ADD COLUMN created_at timestamp")
      this.raw("ALTER TABLE s_m_syokoutei_syurui ADD COLUMN updated_at timestamp")
    })
  }

  down () {
    this.table('s_m_syokoutei_syurui', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_m_syokoutei_syurui DROP COLUMN created_at")
      this.raw("ALTER TABLE s_m_syokoutei_syurui DROP COLUMN updated_at")
    })
  }
}

module.exports = SyokouteiSyuruiSchema
