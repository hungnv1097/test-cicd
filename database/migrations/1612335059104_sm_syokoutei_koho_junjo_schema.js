'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SmSyokouteiKohoJunjoSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_koho', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_koho ADD COLUMN syokoutei_koho_junjo INTEGER");
    })
  }

  down () {
    this.table('s_m_syokoutei_koho', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_m_syokoutei_koho DROP COLUMN syokoutei_koho_junjo");
    })
  }

}

module.exports = SmSyokouteiKohoJunjoSchema
