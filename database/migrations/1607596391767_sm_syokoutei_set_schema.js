'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SmSyokouteiSetSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_set', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_set ADD COLUMN syo_koutei_syurui_seq INTEGER")
      this.raw("ALTER TABLE s_m_syokoutei_set ADD COLUMN syo_koutei_koho_seq INTEGER")
    })
  }

  down () {
    this.table('s_m_syokoutei_set', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_m_syokoutei_set DROP COLUMN syo_koutei_syurui_seq")
      this.raw("ALTER TABLE s_m_syokoutei_set DROP COLUMN syo_koutei_koho_seq")
    })
  }
}

module.exports = SmSyokouteiSetSchema
