'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SMSyokouteiKohoSchema extends Schema {
  up () {
    this.table('s_m_syokoutei_koho', (table) => {
      // alter table
      this.raw("ALTER TABLE s_m_syokoutei_koho DROP CONSTRAINT s_m_syokoutei_koho_pkey");
      this.raw(`ALTER TABLE s_m_syokoutei_koho ADD PRIMARY KEY (syo_koutei_syurui_seq,syo_koutei_koho_seq, seihin_code, koutei_code)`)
    })
  }

  down () {
    this.table('s_m_syokoutei_koho', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SMSyokouteiKohoSchema
