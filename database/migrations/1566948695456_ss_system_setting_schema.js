'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SSSystemSettingSchema extends Schema {
  up () {
    this.create('ss_system_settings', (table) => {
      table.increments()
      table.string('user_id', 40).notNullable().references('user_id').inTable('login')
      table.string('nafuda_riyo_settei', 1).defaultTo('0')
      table.string('hitobetu_riyo_settei', 1).defaultTo('0')
      table.string('text_mode_settei', 1).defaultTo('0')
      table.timestamps()
    })
  }

  down () {
    this.drop('ss_system_settings')
  }
}

module.exports = SSSystemSettingSchema
