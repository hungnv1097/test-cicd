'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SDSyoKoteiMainGanntJSchema extends Schema {
  up () {
    this.table('s_d_syo_kotei_main_gannt_j', (table) => {
      // alter table
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j ADD COLUMN daisu integer DEFAULT 0")
    })
  }

  down () {
    this.table('s_d_syo_kotei_main_gannt_j', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j DROP COLUMN daisu")
    })
  }
}

module.exports = SDSyoKoteiMainGanntJSchema
