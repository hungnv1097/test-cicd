'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SyokouteiSyuruiSchema extends Schema {
  up () {
    this.create('s_m_syokoutei_syurui', (table) => {
      table.increments('syo_koutei_syurui_seq')
      table.string('seihin_code', 20).notNullable()
      table.string('koutei_code', 8).notNullable()
      table.string('syo_koutei_syurui_name', 128).notNullable()
    })
  }

  down () {
    this.drop('s_m_syokoutei_syurui')
  }
}

module.exports = SyokouteiSyuruiSchema
