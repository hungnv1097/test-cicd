'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SsSystemSettingsSchema extends Schema {
  up () {
    this.table('ss_system_settings', (table) => {
      this.raw("ALTER TABLE ss_system_settings ADD COLUMN han_id INTEGER NOT NULL")
      this.raw("ALTER TABLE ss_system_settings DROP CONSTRAINT ss_system_settings_pkey")
      this.raw("ALTER TABLE ss_system_settings ADD PRIMARY KEY (user_id)")
      this.raw("ALTER TABLE ss_system_settings RENAME TO s_s_kosu_kengen;")
    })
  }

  down () {
    this.table('ss_system_settings', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SsSystemSettingsSchema
