"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SsKihonSettingSchema extends Schema {
  up() {
    this.table("s_s_kihon_setting", (table) => {
      this.raw(
        "ALTER TABLE s_s_kihon_setting ADD COLUMN direct_text_input_flag boolean DEFAULT false"
      );
    });
  }

  down() {
    this.table("s_s_kihon_setting", (table) => {
      this.raw(
        "ALTER TABLE s_s_kihon_setting DROP COLUMN direct_text_input_flag"
      );
    });
  }
}

module.exports = SsKihonSettingSchema;
