'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SyokouteiKohoSchema extends Schema {
  up() {
    this.create('s_m_syokoutei_koho', (table) => {
      table.increments('syo_koutei_koho_seq')
      table.string('seihin_code', 20).notNullable()
      table.string('koutei_code', 8).notNullable()
      table.string('syo_koutei_koho_name', 128).notNullable()
      table.integer('syo_koutei_syurui_seq')
    })
  }

  down() {
    this.drop('s_m_syokoutei_koho')
  }
}

module.exports = SyokouteiKohoSchema
