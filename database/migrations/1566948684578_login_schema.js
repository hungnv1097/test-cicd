'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LoginSchema extends Schema {
  up () {
    this.table('logins', (table) => {
      this.raw("ALTER TABLE login ADD CONSTRAINT user_id_unique UNIQUE (user_id);")
      this.raw("ALTER TABLE login ADD COLUMN han_id INTEGER")
      this.raw("ALTER TABLE login ADD CONSTRAINT foreign_key_to_sm_han FOREIGN KEY (han_id) REFERENCES s_m_han (id);")
    })
  }

  down () {
    this.table('logins', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LoginSchema
