'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SMHanSchema extends Schema {
  up () {
    this.table('sm_hans', (table) => {
      this.raw("ALTER TABLE s_m_han DROP CONSTRAINT s_m_han_pkc CASCADE"),
      this.raw("ALTER TABLE s_m_han ADD PRIMARY KEY (id)")
    })
  }

  down () {
    this.table('sm_hans', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SMHanSchema
