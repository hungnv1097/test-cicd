'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SMHanSchema extends Schema {
  up () {
    this.table('s_m_han', (table) => {
      this.raw("ALTER TABLE s_m_han ADD COLUMN id SERIAL UNIQUE")
    })
  }

  down () {
    this.table('s_m_han', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SMHanSchema
