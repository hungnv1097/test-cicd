'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SdSyoKoteiMainGanntSchema extends Schema {
  up () {
    this.table('s_d_syo_kotei_main_gannt', (table) => {
      // alter table
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt ADD COLUMN syo_koutei_syurui_seq INTEGER")
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt ADD COLUMN syo_koutei_koho_seq INTEGER")
    })
  }

  down () {
    this.table('s_d_syo_kotei_main_gannt', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt DROP COLUMN syo_koutei_syurui_seq")
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt DROP COLUMN syo_koutei_koho_seq")
    })
  }
}

module.exports = SdSyoKoteiMainGanntSchema
