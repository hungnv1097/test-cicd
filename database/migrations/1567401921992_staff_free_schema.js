'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StaffFreeSchema extends Schema {
  up () {
    this.create('staff_free', (table) => {
      table.integer('workset_id').notNullable().references('workset_id').inTable('workset')
      table.integer('syain_id').notNullable()
      table.boolean('free_flag').notNullable().defaultTo(true);
      table.timestamps()
      table.primary(['workset_id', 'syain_id'])
    })
  }

  down () {
    this.table('staff_free', (table) => {
      // reverse alternations
    })
  }
}

module.exports = StaffFreeSchema
