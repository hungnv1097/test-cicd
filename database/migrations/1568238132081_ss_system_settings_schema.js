'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SSSystemSettingsSchema extends Schema {
  up () {
    this.create('s_s_system_setting', (table) => {
      table.string('user_id', 40).notNullable().references('user_id').inTable('login')
      table.string('nafuda_riyo_settei', 1).defaultTo('0')
      table.string('hitobetu_riyo_settei', 1).defaultTo('0')
      table.string('text_mode_settei', 1).defaultTo('0')
      table.primary(['nafuda_riyo_settei', 'hitobetu_riyo_settei', 'text_mode_settei'], 's_s_system_settings_pkes')
    })
  }

  down () {
    this.drop('s_s_system_setting')
  }
}

module.exports = SSSystemSettingsSchema
