'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SdSyoKoteiMainGanntJSchema extends Schema {
  up () {
    this.table('s_d_syo_kotei_main_gannt_j', (table) => {
      // alter table
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j ADD COLUMN syo_koutei_syurui_seq INTEGER")
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j ADD COLUMN syo_koutei_koho_seq INTEGER")
    })
  }

  down () {
    this.table('s_d_syo_kotei_main_gannt_j', (table) => {
      // reverse alternations
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j DROP COLUMN syo_koutei_syurui_seq")
      this.raw("ALTER TABLE s_d_syo_kotei_main_gannt_j DROP COLUMN syo_koutei_koho_seq")
    })
  }
}

module.exports = SdSyoKoteiMainGanntJSchema
