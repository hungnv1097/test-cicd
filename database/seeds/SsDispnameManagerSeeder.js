"use strict";

/*
|--------------------------------------------------------------------------
| SsDispnameManagerSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
/** @type {import('@adonisjs/lucid/src/Database')} */
const Database = use("Database");

class SsDispnameManagerSeeder {
  async run() {
    await Database.table("s_s_dispname_manager").insert([
      {
        gamen_code: "MAIN",
        komoku_code: "syokoutei_syurui",
        hyoji_name: "小工程種類",
        default_name: "小工程種類",
        hyouji_flag: "true",
      },
      {
        gamen_code: "MAIN",
        komoku_code: "syokoutei_koho",
        hyoji_name: "小工程候補",
        default_name: "小工程候補",
        hyouji_flag: "true",
      },
    ]);
  }
}

module.exports = SsDispnameManagerSeeder;
