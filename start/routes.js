"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.get("/", () => {
  return { info: "Api" };
});

Route.get("sm-syokoutei-syurui/list", "SmSyokouteiSyuruiController.list");
Route.post("sm-syokoutei-syurui/create", "SmSyokouteiSyuruiController.create");
Route.post("sm-syokoutei-syurui/update", "SmSyokouteiSyuruiController.update");
Route.post("sm-syokoutei-syurui/delete", "SmSyokouteiSyuruiController.delete");
Route.post("sm-syokoutei-syurui/update-junjo", "SmSyokouteiSyuruiController.updateJunjo")

Route.get("sm-syokoutei-koho/list", "SmSyokouteiKohoController.list");
Route.post("sm-syokoutei-koho/create", "SmSyokouteiKohoController.create");
Route.post("sm-syokoutei-koho/update", "SmSyokouteiKohoController.update");
Route.post("sm-syokoutei-koho/delete", "SmSyokouteiKohoController.delete");
Route.post("sm-syokoutei-koho/update-junjo", "SmSyokouteiKohoController.updateJunjo");

Route.post("sd-syo-kotei-denpyo/create", "SdSyoKoteiDenpyoController.create");
Route.post("sd-syo-kotei-denpyo/update", "SdSyoKoteiDenpyoController.update");
Route.get("sd-syo-kotei-denpyo/list", "SdSyoKoteiDenpyoController.list"); 
Route.get("sd-syo-kotei-denpyo/detail", "SdSyoKoteiDenpyoController.detail");
Route.post("sd-syo-kotei-denpyo/delete", "SdSyoKoteiDenpyoController.delete");

Route.post("sm-seihin-mst/create", "SmSeihinMstController.create");
Route.post("sm-seihin-mst/update", "SmSeihinMstController.update");
Route.get("sm-seihin-mst/list", "SmSeihinMstController.list");
Route.get("sm-seihin-mst/list-main", "SmSeihinMstController.listMain");
Route.get("sm-seihin-mst/detail", "SmSeihinMstController.detail");
Route.post("sm-seihin-mst/delete", "SmSeihinMstController.delete");
Route.post("sm-seihin-mst/change-gannt-color", "SmSeihinMstController.changeGanntColor");
Route.post("sm-seihin-mst/delete-product-setting", "SmSeihinMstController.deleteSeihin_mst");

Route.post("st-seisansiji/create", "StSeisansijiController.create");
Route.post("st-seisansiji/update", "StSeisansijiController.update");
Route.post("st-seisansiji/lock", "StSeisansijiController.lock");
Route.get("st-seisansiji/list", "StSeisansijiController.list");
Route.get(
  "st-seisansiji/group-by-syokoutei",
  "StSeisansijiController.groupBySyokoutei"
);
Route.post(
  "st-seisansiji/update-group-by-syokoutei",
  "StSeisansijiController.updateGroupBySyokoutei"
  );
Route.get(
  "st-seisansiji/group-by-souchi",
  "StSeisansijiController.groupBySouchi"
);
Route.get("st-seisansiji/detail", "StSeisansijiController.detail");
Route.post("st-seisansiji/delete", "StSeisansijiController.delete");
Route.post("st-seisansiji/map-koutei", "StSeisansijiController.mapKoutei");
Route.get(
  "st-seisansiji/list-map-koutei",
  "StSeisansijiController.listMapKoutei"
);
Route.post("st-seisansiji/copy", "StSeisansijiController.copy");
Route.post("st-seisansiji/lock-by-date", "StSeisansijiController.lockByDate");
Route.get("st-seisansiji/del-cache", "StSeisansijiController.delCache");

Route.post(
  "sd-syo-kotei-main-gannt/create",
  "SdSyoKoteiMainGanntController.create"
);
Route.post(
  "sd-syo-kotei-main-gannt/update",
  "SdSyoKoteiMainGanntController.update"
);
Route.post(
  "sd-syo-kotei-main-gannt/multiple-create-update",
  "SdSyoKoteiMainGanntController.multipleCreateUpdate"
);
Route.get("sd-syo-kotei-main-gannt/list", "SdSyoKoteiMainGanntController.list");
Route.get(
  "sd-syo-kotei-main-gannt/detail",
  "SdSyoKoteiMainGanntController.detail"
);
Route.post(
  "sd-syo-kotei-main-gannt/delete",
  "SdSyoKoteiMainGanntController.delete"
);

Route.post(
  "sd-syo-kotei-main-gannt-j/create",
  "SdSyoKoteiMainGanntJController.create"
);
Route.post(
  "sd-syo-kotei-main-gannt-j/update",
  "SdSyoKoteiMainGanntJController.update"
);
Route.get(
  "sd-syo-kotei-main-gannt-j/list",
  "SdSyoKoteiMainGanntJController.list"
);
Route.get(
  "sd-syo-kotei-main-gannt-j/detail",
  "SdSyoKoteiMainGanntJController.detail"
);
Route.post(
  "sd-syo-kotei-main-gannt-j/delete",
  "SdSyoKoteiMainGanntJController.delete"
);

Route.post("sm-syokoutei-mst/create", "SmSyokouteiMstController.create");
Route.post("sm-syokoutei-mst/update", "SmSyokouteiMstController.update");
Route.get("sm-syokoutei-mst/list", "SmSyokouteiMstController.list");
Route.get("sm-syokoutei-mst/detail", "SmSyokouteiMstController.detail");
Route.post("sm-syokoutei-mst/delete", "SmSyokouteiMstController.delete");
Route.post(
  "sm-syokoutei-mst/update-sub-process",
  "SmSyokouteiMstController.updateSubProcess"
);

Route.post(
  "sm-syokoutei-set/create-full",
  "SmSyokouteiSetController.createFull"
);
Route.post("sm-syokoutei-set/create", "SmSyokouteiSetController.create");
Route.post("sm-syokoutei-set/update", "SmSyokouteiSetController.update");
Route.get("sm-syokoutei-set/list", "SmSyokouteiSetController.list");
Route.get(
  "sm-syokoutei-set/list-main-gannt",
  "SmSyokouteiSetController.listMainGannt"
);
Route.get("sm-syokoutei-set/detail", "SmSyokouteiSetController.detail");
Route.post("sm-syokoutei-set/delete", "SmSyokouteiSetController.delete");
Route.post(
  "sm-syokoutei-set/update-work-device",
  "SmSyokouteiSetController.updateWorkDevice"
);
Route.post(
  "sm-syokoutei-set/update-tantosya",
  "SmSyokouteiSetController.updateTantosya"
);
Route.post(
  "sm-syokoutei-set/change-code",
  "SmSyokouteiSetController.changeCode"
);
Route.post("sm-syokoutei-set/clone", "SmSyokouteiSetController.clone");
Route.post(
  "sm-syokoutei-set/upsert-befaft",
  "SmSyokouteiSetController.upsertBefaft"
);
Route.post(
  "sm-syokoutei-set/detail-befaft",
  "SmSyokouteiSetController.detailBefaft"
);
Route.post(
  "sm-syokoutei-set/upsert-konsai",
  "SmSyokouteiSetController.upsertKonsai"
);
Route.post(
  "sm-syokoutei-set/detail-konsai",
  "SmSyokouteiSetController.detailKonsai"
);
Route.post(
  "sm-syokoutei-set/update-junjo",
  "SmSyokouteiSetController.updateJunjo"
);
Route.post(
  "sm-syokoutei-set/update-sokudo",
  "SmSyokouteiSetController.updateSokudo"
);


Route.post(
  "sm-syokoutei-set-gannt/create",
  "SmSyokouteiSetGanntController.create"
);
Route.post(
  "sm-syokoutei-set-gannt/update",
  "SmSyokouteiSetGanntController.update"
);
Route.post(
  "sm-syokoutei-set-gannt/update-gannts-time",
  "SmSyokouteiSetGanntController.updateGanntsTime"
);
Route.get("sm-syokoutei-set-gannt/list", "SmSyokouteiSetGanntController.list");
Route.get(
  "sm-syokoutei-set-gannt/detail",
  "SmSyokouteiSetGanntController.detail"
);
Route.post(
  "sm-syokoutei-set-gannt/delete",
  "SmSyokouteiSetGanntController.delete"
);

Route.get("s-m-syokoutei-set-gannt/list", "Sm_SyokouteiSetGanntController.list");
Route.get("s-m-syokoutei-set-gannt/detail", "Sm_SyokouteiSetGanntController.detail");
Route.post("s-m-syokoutei-set-gannt/update", "Sm_SyokouteiSetGanntController.update");
Route.post("s-m-syokoutei-set-gannt/create", "Sm_SyokouteiSetGanntController.create");
Route.post("s-m-syokoutei-set-gannt/delete", "Sm_SyokouteiSetGanntController.delete");

Route.post(
  "sm-syokoutei-syo-list/create",
  "SmSyokouteiSyoListController.create"
);
Route.post(
  "sm-syokoutei-syo-list/update",
  "SmSyokouteiSyoListController.update"
);
Route.get("sm-syokoutei-syo-list/list", "SmSyokouteiSyoListController.list");
Route.get(
  "sm-syokoutei-syo-list/detail",
  "SmSyokouteiSyoListController.detail"
);
Route.get(
  "sm-syokoutei-syo-list/detail2",
  "SmSyokouteiSyoListController.detail2"
);
Route.post(
  "sm-syokoutei-syo-list/delete",
  "SmSyokouteiSyoListController.delete"
);
Route.post(
  "sm-syokoutei-syo-list/upsert-souchi-code-saygo-code",
  "SmSyokouteiSyoListController.upsertSouchiCodeAndSaygoCode"
);

Route.post("sm-koutei-mst/create", "SmKouteiMstController.create");
Route.post("sm-koutei-mst/update", "SmKouteiMstController.update");
Route.get("sm-koutei-mst/list", "SmKouteiMstController.list");
Route.get("sm-koutei-mst/detail", "SmKouteiMstController.detail");
Route.post("sm-koutei-mst/delete", "SmKouteiMstController.delete");
Route.post(
  "sm-koutei-mst/update-sub-process",
  "SmKouteiMstController.updateSubProcess"
);

Route.post("sm-sagyo-mst/create", "SmSagyoMstController.create");
Route.post("sm-sagyo-mst/update", "SmSagyoMstController.update");
Route.get("sm-sagyo-mst/list", "SmSagyoMstController.list");
Route.get("sm-sagyo-mst/detail", "SmSagyoMstController.detail");
Route.post("sm-sagyo-mst/delete", "SmSagyoMstController.delete");

Route.post("sm-souchi-mst/create", "SmSouchiMstController.create");
Route.post("sm-souchi-mst/update", "SmSouchiMstController.update");
Route.get("sm-souchi-mst/list", "SmSouchiMstController.list");
Route.get("sm-souchi-mst/list-main", "SmSouchiMstController.listMain");
Route.get("sm-souchi-mst/list-gannt", "SmSouchiMstController.listGannt");
Route.get("sm-souchi-mst/detail", "SmSouchiMstController.detail");
Route.post("sm-souchi-mst/delete", "SmSouchiMstController.delete");

Route.post("sm-befaft-group/create", "SmBefaftGroupController.create");
Route.post("sm-befaft-group/update", "SmBefaftGroupController.update");
Route.get("sm-befaft-group/list", "SmBefaftGroupController.list");
Route.get("sm-befaft-group/detail", "SmBefaftGroupController.detail");
Route.post("sm-befaft-group/delete", "SmBefaftGroupController.delete");

Route.post(
  "sm-befaft-group-syosai/create",
  "SmBefaftGroupSyosaiController.create"
);
Route.post(
  "sm-befaft-group-syosai/update",
  "SmBefaftGroupSyosaiController.update"
);
Route.get("sm-befaft-group-syosai/list", "SmBefaftGroupSyosaiController.list");
Route.post(
  "sm-befaft-group-syosai/delete",
  "SmBefaftGroupSyosaiController.delete"
);

Route.post("sm-konsai-group/create", "SmKonsaiGroupController.create");
Route.post("sm-konsai-group/update", "SmKonsaiGroupController.update");
Route.get("sm-konsai-group/list", "SmKonsaiGroupController.list");
Route.get("sm-konsai-group/detail", "SmKonsaiGroupController.detail");
Route.post("sm-konsai-group/delete", "SmKonsaiGroupController.delete");

Route.post(
  "sm-konsai-group-syosai/create",
  "SmKonsaiGroupSyosaiController.create"
);
Route.post(
  "sm-konsai-group-syosai/update",
  "SmKonsaiGroupSyosaiController.update"
);
Route.get("sm-konsai-group-syosai/list", "SmKonsaiGroupSyosaiController.list");
Route.post(
  "sm-konsai-group-syosai/delete",
  "SmKonsaiGroupSyosaiController.delete"
);

Route.get("sm-koutei-set/list", "SmKouteiSetController.list");
Route.post("sm-koutei-set/create", "SmKouteiSetController.create");
Route.post("sm-koutei-set/update", "SmKouteiSetController.update");
Route.post("sm-koutei-set/delete", "SmKouteiSetController.delete");
Route.post("sm-koutei-set/change-code", "SmKouteiSetController.changeCode");
Route.post(
  "sm-koutei-set/change-seihin-code",
  "SmKouteiSetController.changeSeihinCode"
);
Route.post("sm-koutei-set/clone", "SmKouteiSetController.clone");
Route.post(
  "sm-koutei-set/clone-by-seihin",
  "SmKouteiSetController.cloneBySeihin"
);
Route.post("sm-koutei-set/update-junjo", "SmKouteiSetController.updateJunjo");

Route.post("ss-dispname-manager/create", "SsDispnameManagerController.create");
Route.post("ss-dispname-manager/update", "SsDispnameManagerController.update");
Route.get("ss-dispname-manager/list", "SsDispnameManagerController.list");
Route.get("ss-dispname-manager/detail", "SsDispnameManagerController.detail");
Route.post("ss-dispname-manager/delete", "SsDispnameManagerController.delete");

Route.post("ss-kihon-setting/create", "SsKihonSettingController.create");
Route.post("ss-kihon-setting/update", "SsKihonSettingController.update");
Route.get("ss-kihon-setting/list", "SsKihonSettingController.list");
Route.get("ss-kihon-setting/detail", "SsKihonSettingController.detail");
Route.post("ss-kihon-setting/delete", "SsKihonSettingController.delete");

Route.post("import/seisansiji", "ImportController.seisansiji");
Route.post("import/souchi", "ImportController.souchi");
Route.post("import/syokoutei", "ImportController.syokoutei");

Route.post("sm-han/login", "SmHanController.login");
Route.post("sm-han/create", "SmHanController.create");
Route.post("sm-han/update", "SmHanController.update");
Route.get("sm-han/list", "SmHanController.list");
Route.get("sm-han/detail", "SmHanController.detail");
Route.post("sm-han/delete", "SmHanController.delete");

Route.post("sm-han-koutei/create", "SmHanKouteiController.create");
Route.post("sm-han-koutei/update", "SmHanKouteiController.update");
Route.get("sm-han-koutei/list", "SmHanKouteiController.list");
Route.get("sm-han-koutei/detail", "SmHanKouteiController.detail");
Route.post("sm-han-koutei/delete", "SmHanKouteiController.delete");

Route.get("permission-setting/list", "PermissionSettingController.list");
Route.post("permission-setting/update", "PermissionSettingController.update");
Route.post("permission-setting/delete", "PermissionSettingController.delete");

Route.get("syain/list", "SyainController.list");
Route.get("role/list", "RoleController.list");
Route.post("user/create", "LoginController.create");
Route.post("user/update", "LoginController.update");

Route.get("staff/list", "StaffFreeController.list");
Route.post("staff/add", "StaffFreeController.addStaff");
Route.get("staff/listAdded", "StaffFreeController.listAdded");
Route.post("staff/change-staff", "StaffFreeController.changeStaff");

Route.get("class/list", "ClassController.list");

Route.post("ss-kadojikoku/create", "SsKadojikokuController.create");
Route.post("ss-kadojikoku/update", "SsKadojikokuController.update");
Route.get("ss-kadojikoku/list", "SsKadojikokuController.list");
Route.get("ss-kadojikoku/detail", "SsKadojikokuController.detail");
Route.post("ss-kadojikoku/delete", "SsKadojikokuController.delete");

Route.post("ss-course-rec/create", "SsCourseRecController.create");
Route.post("ss-course-rec/update", "SsCourseRecController.update");
Route.get("ss-course-rec/list", "SsCourseRecController.list");
Route.get("ss-course-rec/detail", "SsCourseRecController.detail");
Route.post("ss-course-rec/delete", "SsCourseRecController.delete");

Route.post("ss-course/create", "SsCourseController.create");
Route.post("ss-course/update", "SsCourseController.update");
Route.get("ss-course/list", "SsCourseController.list");
Route.get("ss-course/detail", "SsCourseController.detail");
Route.post("ss-course/delete", "SsCourseController.delete");

Route.post("s_d_syokoutei_koho_denpyo/update", "SdSyokouteiKohoDenpyoController.update");
Route.post("s_d_syokoutei_koho_denpyo/create", "SdSyokouteiKohoDenpyoController.create");