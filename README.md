# Setup Enviroment
```bash
sudo yum install epel-release
sudo yum install ansible -y
ansible-galaxy install -r requirements.yml
ansible-playbook  playbook.yml
```
Restore Database
```bash
pg_restore -h localhost -U kado -W -C -d kadodb_koutei dbfile/20190611_kadodb_koutei.dat
pg_restore -h localhost -U postgres -W -C -d kadodb_koutei kadodb_koutei_20191021_bk.dat

Run immport SQL
```bash
psql -h localhost -U kado -d kadodb_koutei -f dbfile/update_new_sql.sql
```

# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
npm install
cp .env.example .env
```

Change config in .env

```bash
npm start
```

## Api doc

http://${HOST}:${PORT}/docs
