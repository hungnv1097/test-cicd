 CREATE OR REPLACE VIEW public.souchi_work_log AS
  SELECT a.souchi_code, a.souchi_name, b.seihin_code, b.koutei_code,
     c.koteikeikaku_no, c.main_sub, c.proc_from, c.proc_to, d.start_datetime,
     d.nouki_datetime, sm.mae_setup_time, sm.std_setup_time, sm.ato_setup_time, a.daisu
    FROM s_m_souchi_mst a
    LEFT JOIN s_m_syokoutei_syo_list b ON a.souchi_code::text = b.souchi_code::text
    LEFT JOIN s_d_syo_kotei_main_gannt c ON b.seihin_code::text = c.seihin_code::text AND b.koutei_code::text = c.koutei_code::text AND b.syo_koutei_code::text = c.syo_koutei_code::text
    LEFT JOIN s_t_seisansiji d ON c.koteikeikaku_no::text = d.koteikeikaku_no::text
    LEFT JOIN s_m_syokoutei_set sm ON b.seihin_code::text = sm.seihin_code::text AND b.koutei_code::text = sm.koutei_code::text AND b.syo_koutei_code::text = sm.syo_koutei_code::text
   WHERE d.koteikeikaku_no IS NOT NULL;

 CREATE OR REPLACE VIEW public.souchi_main_gannt AS
  SELECT a.souchi_code, a.souchi_name, b.seihin_code, b.koutei_code,
    c.koteikeikaku_no, c.main_sub, c.proc_from, c.proc_to, c.std_proc_syohin_su, c.proc_sokudo, c.color,
    d.start_datetime, d.nouki_datetime
   FROM s_m_souchi_mst a
   LEFT JOIN s_m_syokoutei_syo_list b ON a.souchi_code::text = b.souchi_code::text
   LEFT JOIN s_d_syo_kotei_main_gannt c ON b.seihin_code::text = c.seihin_code::text AND b.koutei_code::text = c.koutei_code::text AND b.syo_koutei_code::text = c.syo_koutei_code::text
   LEFT JOIN s_t_seisansiji d ON c.koteikeikaku_no::text = d.koteikeikaku_no::text
  WHERE d.koteikeikaku_no IS NOT NULL;
