module.exports = {
    TYPE_OF_MAIN_GANNT: {
        after: 'a',
        main: 'm',
        before: 'b'
    },
    DEFAULT_NAME_KOHO: '候補1',
    FORMAT_DATE_TIME: "YYYY-MM-DD HH:mm:ss",
    MESS_CAN_NOT_MOVE: "前処理と後処理、必要な時間間隔の時間が足りないため、変更できません",
    TIME_ZONE: '+0700', // VN
    // TIME_ZONE: '+0900' // Japan
}