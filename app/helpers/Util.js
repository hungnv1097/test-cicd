"use strict";
const Constants = require("./Constants");
const moment = require("moment");
const _ = require("lodash")

class Util {
  /**
   * 
   * @param {*} date 
   * @param {*} hours 
   * @param {*} sub flag sub is subtract ---- not sub is add
   */
  static getString (obj, propertyPath = undefined, defaultValue = undefined) {
    try {
      // propertyPath phải là string hoặc undefined
      if (!_.isUndefined(propertyPath) && !_.isString(propertyPath)) {
        return undefined;
      }
      if (!_.isNil(propertyPath)) {
        if (_.isNil(obj)) {
          return defaultValue;
        }
        if (_.isObject(obj)) {
          var properties = propertyPath.split(".");
          // tìm property value từ property path
          let result = properties.reduce((prev, curr) => prev && prev[curr], obj);
          return _.isString(result) ? result : defaultValue;
        }
      } else if (_.isString(obj)) {
        return obj;
      }
    } catch (err) {
      console.error(err);
    }
    return defaultValue;
  };
  static addDateWithHour(date, hours, sub) {
    if (sub)
      return moment(date)
        .subtract(hours, "hours").format(Constants.FORMAT_DATE_TIME);
    return moment(date).add(hours, "hours").format(Constants.FORMAT_DATE_TIME);
  }

  static diffTwoDateAsHour(a, b) {
    try {
      return moment.duration(moment(b).diff(a)).asHours();
    } catch (error) {
      return 0;
    }
  }
  static compareTwoDate(a, b) {
    if (this.diffTwoDateAsHour(b, a) <= 0) return false
    return true
  }
  static convertToJSON(data) {
    return JSON.parse(JSON.stringify(data).replace(/\:null/gi, ':""')) || [];
  }

  static getCode(s) {
    if (s === undefined) {
      return Util.randomStr(2) + (Util.getTimeCurrent() + "").substring(5);
    } else {
      return s + (Util.getTimeCurrent() + "").substring(5);
    }
  }

  static getTimeCurrent() {
    return Math.floor(Date.now() / 1000);
  }

  // getDateTimeByFormat(new Date(), 'HH:mm:ss M/d/y')
  static getDateTimeByFormat(date, format, utc) {
    let MMMM = [
      "\x00",
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let MMM = [
      "\x01",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let dddd = [
      "\x02",
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    let ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    function ii(i, len) {
      let s = i + "";
      len = len || 2;
      while (s.length < len) {
        s = "0" + s;
      }
      return s;
    }

    let y = utc ? date.getUTCFullYear() : date.getFullYear();
    format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
    format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
    format = format.replace(/(^|[^\\])y/g, "$1" + y);

    let M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
    format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
    format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
    format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
    format = format.replace(/(^|[^\\])M/g, "$1" + M);

    let d = utc ? date.getUTCDate() : date.getDate();
    format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
    format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
    format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
    format = format.replace(/(^|[^\\])d/g, "$1" + d);

    let H = utc ? date.getUTCHours() : date.getHours();
    format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
    format = format.replace(/(^|[^\\])H/g, "$1" + H);

    let h = H > 12 ? H - 12 : H == 0 ? 12 : H;
    format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
    format = format.replace(/(^|[^\\])h/g, "$1" + h);

    let m = utc ? date.getUTCMinutes() : date.getMinutes();
    format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
    format = format.replace(/(^|[^\\])m/g, "$1" + m);

    let s = utc ? date.getUTCSeconds() : date.getSeconds();
    format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
    format = format.replace(/(^|[^\\])s/g, "$1" + s);

    let f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
    format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])f/g, "$1" + f);

    let T = H < 12 ? "AM" : "PM";
    format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
    format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

    let t = T.toLowerCase();
    format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
    format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

    let tz = -date.getTimezoneOffset();
    let K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
    if (!utc) {
      tz = Math.abs(tz);
      let tzHrs = Math.floor(tz / 60);
      let tzMin = tz % 60;
      K += ii(tzHrs) + ":" + ii(tzMin);
    }
    format = format.replace(/(^|[^\\])K/g, "$1" + K);

    let day = (utc ? date.getUTCDay() : date.getDay()) + 1;
    format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
    format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

    format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
    format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

    format = format.replace(/\\(.)/g, "$1");

    return format;
  }

  static getDayInWeek() {
    return new Date().getDay() - 1;
  }

  static nextMinutes(date, next, format, utc) {
    date.setMinutes(date.getMinutes() + next);
    let newDate = Util.getDateTimeByFormat(date, format, utc);
    return newDate;
  }

  static nextDate(date, next, format, utc) {
    date.setDate(date.getDate() + next);
    let newDate = Util.getDateTimeByFormat(date, format, utc);
    return newDate;
  }

  static randomStr(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  static randomNumber(length) {
    var result = "";
    var characters = "0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  static daysInMonthFromTimestamp(timestamp) {
    let dateObj = new Date(timestamp * 1000);
    return Util.daysInMonth(dateObj.getMonth(), dateObj.getFullYear());
  }

  static daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  static getSetGanntFrom({
    post_start_bef,
    post_start_aft,
    post_end_bef,
    post_end_aft,
  }) {
    return function (gannt) {
      if (post_start_bef != undefined && post_start_bef != "") {
        return gannt.proc_from;
      }
      if (post_start_aft != undefined && post_start_aft != "") {
        return gannt.proc_from + +post_start_aft;
      }
      if (post_end_bef != undefined && post_end_bef != "") {
        return gannt.proc_to;
      }
      if (post_end_aft != undefined && post_end_aft != "") {
        return gannt.proc_to + +post_end_aft;
      }
      return gannt.proc_from;
    };
  }

  static generateNextSetGannt(
    data,
    ganntsByProcess,
    gannts,
    newSetGannts,
    procMinute,
    prevSubProcceses
  ) {
    const getFrom = Util.getSetGanntFrom(prevSubProcceses);

    const ganntsOfPrev = ganntsByProcess.filter(
      (item) =>
        item.syo_koutei_code === prevSubProcceses.syo_koutei_code &&
        item.syo_koutei_junjo === prevSubProcceses.syo_koutei_junjo
    );

    const hashOfGanntsOfPrev = ganntsOfPrev.reduce((hs, item) => {
      hs[item.main_sub] = item;
      return hs;
    }, {});

    if (!gannts.length) {
      const mainSub = "m";
      const from = !newSetGannts.b
        ? getFrom(
          hashOfGanntsOfPrev.a || hashOfGanntsOfPrev.m || hashOfGanntsOfPrev.b
        )
        : newSetGannts.b.proc_to;

      const to = from + procMinute;

      newSetGannts[mainSub] = newSetGannts[mainSub]
        ? {
          ...newSetGannts[mainSub],
          proc_from: from,
          proc_to: to,
        }
        : {
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          proc_from: from,
          proc_to: to,
          main_sub: mainSub,
        };

      return Object.keys(newSetGannts).map((k) => newSetGannts[k]);
    }

    gannts.forEach((gannt) => {
      let from, to;
      if (gannt.main_sub === "b") {
        from = getFrom(
          hashOfGanntsOfPrev.a || hashOfGanntsOfPrev.m || hashOfGanntsOfPrev.b
        );
      } else if (gannt.main_sub === "m") {
        from = !newSetGannts.b
          ? getFrom(
            hashOfGanntsOfPrev.a ||
            hashOfGanntsOfPrev.m ||
            hashOfGanntsOfPrev.b
          )
          : newSetGannts.b.proc_to;
      } else {
        from = !newSetGannts.m
          ? getFrom(
            hashOfGanntsOfPrev.a ||
            hashOfGanntsOfPrev.m ||
            hashOfGanntsOfPrev.b
          )
          : newSetGannts.m.proc_to;
      }
      to = from + procMinute;

      newSetGannts[gannt.main_sub] = newSetGannts[gannt.main_sub]
        ? {
          ...newSetGannts[gannt.main_sub],
          proc_from: from,
          proc_to: to,
        }
        : {
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          proc_from: from,
          proc_to: to,
          main_sub: gannt.main_sub,
        };
    });
    return Object.keys(newSetGannts).map((k) => newSetGannts[k]);
  }

  static generateFirstSetGannt(data, gannts, newSetGannts, procMinute) {
    if (!gannts.length) {
      const mainSub = "m";
      const from = newSetGannts.b ? newSetGannts.b.proc_to : 0;
      const to = from + procMinute;
      newSetGannts[mainSub] = newSetGannts[mainSub]
        ? {
          ...newSetGannts[mainSub],
          proc_from: from,
          proc_to: to,
        }
        : {
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          proc_from: from,
          proc_to: to,
          main_sub: mainSub,
        };

      return Object.keys(newSetGannts).map((k) => newSetGannts[k]);
    }

    gannts.forEach((gannt) => {
      let from, to;
      if (gannt.main_sub === "b") {
        from = 0;
        to = procMinute;
      } else if (gannt.main_sub === "m") {
        from = newSetGannts.b ? newSetGannts.b.proc_to : 0;
        to = from + procMinute;
      } else {
        from = newSetGannts.m ? newSetGannts.m.proc_to : 0;
        to = from + procMinute;
      }
      newSetGannts[gannt.main_sub] = newSetGannts[gannt.main_sub]
        ? {
          ...newSetGannts[gannt.main_sub],
          proc_from: from,
          proc_to: to,
        }
        : {
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          proc_from: from,
          proc_to: to,
          main_sub: gannt.main_sub,
        };
    });
    return Object.keys(newSetGannts).map((k) => newSetGannts[k]);
  }

  static formatTime(instance) {
    if (instance.proc_from) {
      instance.proc_from = moment(instance.proc_from).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }
    if (instance.proc_to) {
      instance.proc_to = moment(instance.proc_to).format("YYYY-MM-DD HH:mm:ss");
    }
    if (instance.start_datetime) {
      instance.start_datetime = moment(instance.start_datetime).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }
    if (instance.nouki_datetime) {
      instance.nouki_datetime = moment(instance.nouki_datetime).format(
        "YYYY-MM-DD HH:mm:ss"
      );
    }
    return instance;
  }

  static convertSecondToDayTime(start_datetime, value) {
    const totalHour = value / 3600 / 24;
    const day = Math.floor(totalHour);
    let hour = Math.floor((totalHour - day) * 24);
    let minute = parseFloat(((totalHour - day) * 24) - hour).toFixed(4) * 60
    if (minute === 60) {
      hour += 1;
      minute = 0;
    }

    const newStart = moment(start_datetime);

    newStart.set({ date: newStart.date() + day });
    newStart.add(hour, 'hours');
    newStart.add(minute, 'minute')

    return newStart;
  }
}

module.exports = Util;
