'use strict'
const SmHanKoutei = use('App/Models/SmHanKoutei')
const SmKouteiMst = use('App/Models/SmKouteiMst')
const SmHan = use('App/Models/SmHan')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmHanKouteis
 */
class SmHanKouteiController {
  /**
   * Show a list of all SmHanKouteis.
   * GET SmHanKouteis
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-han-koutei/list:
   *   get:
   *     security:
   *     tags:
   *       - SmHanKoutei
   *     summary: Sample API list SmHanKoutei
   *     parameters:
   *       - name: han_name
   *         description: han_name
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanKouteiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SmHanKoutei.query()
      if(!isEmpty(params.han_name)){
        query.where('han_name', 'LIKE', '%'+params.han_name+'%')
      }
      if(!isEmpty(params.koutei_code)){
        query.where('koutei_code', 'LIKE', '%'+params.koutei_code+'%')
      }
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmHanKoutei.
   * GET SmHanKouteis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-han-koutei/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmHanKoutei
   *     summary: Sample API
   *     parameters:
   *       - name: han_name
   *         description: page
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanKouteiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        han_name: 'required|string',
        koutei_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmHanKoutei
        .query()
        .where({'han_name': data.han_name, 'koutei_code': data.koutei_code})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmHanKoutei.
   * POST SmHanKouteis
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han-koutei/create:
   *   post:
   *     description: Api create SmHanKoutei
   *     security:
   *     tags:
   *       - SmHanKoutei
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanKouteiCreate
   *          schema:
   *            $ref: '#/definitions/SmHanKouteiCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanKouteiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        han_name: 'required|string',
        koutei_code: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      const checkHan = await SmHan.query().where({
        'han_name': data.han_name
      }).first()
      if(isEmpty(checkHan)){
        return response.status(501).json({
          success: false,
          message: 'han_name exist',
          statusCode: 501,
        })
      }

      //check code in db
      const check = await SmKouteiMst.query().where({
        'koutei_code': data.koutei_code
      }).first()
      if(isEmpty(check)){
        return response.status(501).json({
          success: false,
          message: 'koutei_code exist',
          statusCode: 501,
        })
      }

      let res = await SmHanKoutei.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmHanKoutei details.
   * PUT or PATCH SmHanKouteis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han-koutei/update:
   *   post:
   *     description: Api update SmHanKoutei
   *     security:
   *     tags:
   *       - SmHanKoutei
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanKouteiCreate
   *          schema:
   *            $ref: '#/definitions/SmHanKouteiUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanKouteiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        han_name: 'required|string',
        koutei_code: 'required|string'
      }
      let rulesUpdate = {
        data_update: 'object',
      }
      const data = request.only(Object.keys(rules))
      const dataUpdate = request.only(Object.keys(rulesUpdate))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      if(isEmpty(dataUpdate.data_update)){
        let responseData = {
          success: false,
          message: 'data update empty',
        }
        return response.json(responseData)
      }
      await SmHanKoutei.query().where(data).update(dataUpdate.data_update)
      const res = await SmHanKoutei.query().where(dataUpdate.data_update).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmHanKoutei with id.
   * DELETE SmHanKouteis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han-koutei/delete:
   *   post:
   *     description: Api delete SmHanKoutei
   *     security:
   *     tags:
   *       - SmHanKoutei
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanKouteiDelete
   *          schema:
   *            $ref: '#/definitions/SmHanKouteiDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        han_name: 'required|string',
        koutei_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmHanKoutei
        .query()
        .where({
          'han_name': data.han_name,
          'koutei_code': data.koutei_code,
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmHanKouteiController
