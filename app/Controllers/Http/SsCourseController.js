"use strict";


const SsCourse = use("App/Models/SsCourse");
const SsCourseRec = use("App/Models/SsCourseRec");

const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const moment = require('moment');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsCourses
 */
class SsCourseController {
  /**
   * Show a list of all SsCourses.
   * GET SsCourses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /ss-course/list:
   *   get:
   *     security:
   *     tags:
   *       - SsCourse
   *     summary: Sample API list SsCourse
   *     parameters:
   *       - name: shikibetu
   *         description: shikibetu
   *         in: query
   *         type: string
   *       - name: start_set_date
   *         description: start_set_date
   *         in: query
   *         type: string
   *       - name: end_set_date
   *         description: end_set_date
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SsCourse.query();
      if (!isEmpty(params.shikibetu)) {
        query.where("shikibetu", "LIKE", "%" + params.shikibetu + "%");
      }
      if (!isEmpty(params.start_set_date)) {
        query.where("set_date", ">=", params.start_set_date);
      }
      if (!isEmpty(params.end_set_date)) {
        query.where("set_date", "<=", params.end_set_date);
      }
      let data = await query.paginate(page, perPage);
      data = data.toJSON();
      const courseRecConds = [];
      data.data.forEach(item => {
        courseRecConds.push({
          shikibetu: item.shikibetu,
          set_date:moment(item.set_date).format('YYYY-MM-DD')
        });
      });
      let shikibetus = {};
      if (courseRecConds.length) {
        const ssCourseRecQuery = SsCourseRec.query();

        const list = await ssCourseRecQuery
          .whereRaw(
            `
                ${courseRecConds
                  .map(
                    cond =>
                      `(shikibetu = '${cond.shikibetu}' AND set_date = '${cond.set_date}')`
                  )
                  .join(" OR ")}
            `,
            [],
          )
          .fetch();

        shikibetus = list.toJSON().reduce((hs, item) => {
          const k = `${item.shikibetu} ___ ${item.set_date}`;
          hs[k] = hs[k] || [];
          hs[k].push(item);
          return hs;
        }, {});
      }
      const data_result = data.data.map(item => ({
        ...item,
        shikibetuData:
          shikibetus[`${item.shikibetu} ___ ${item.set_date}`] || []
      }))

      data_result.forEach((record) =>{
        record.shikibetuData.sort((a, b) => a.course_time_from - b.course_time_from)
      })
      return {
        success: true,
        pagination: {
          perPage: data.perPage,
          page: data.page,
          lastPage: data.lastPage,
          total: parseInt(data.total)
        },
        data: data_result
      };
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new SsCourse.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /ss-course/create:
   *   post:
   *     description: Api create SsCourse
   *     security:
   *     tags:
   *       - SsCourse
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseCreate
   *          schema:
   *            $ref: '#/definitions/SsCourseCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      const data = request.only(Object.keys(SsCourse.rules));
      const validation = await validateAll(data, SsCourse.rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const ssCourse = await SsCourse.query()
        .where({ set_date: data.set_date })
        .first();

      if (ssCourse) {
        return response.json({
          success: false,
          message: "Duplicate ssCourse. Please select another date.",
          statusCode: 409
        });
      }

      let res = await SsCourse.create(data);
      let responseData = {
        success: true,
        message: "create success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SsCourse.
   * GET SsCourses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-course/detail:
   *   get:
   *     security:
   *     tags:
   *       - SsCourse
   *     summary: Sample API
   *     parameters:
   *       - name: shikibetu
   *         description: shikibetu
   *         in: query
   *         type: string
   *       - name: set_date
   *         description: set_date
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response }) {
    try {
      let rules = {
        shikibetu: "required|string",
        set_date: "required|string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SsCourse.query()
        .where({ shikibetu: data.shikibetu, set_date: data.set_date })
        .fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SsCourse details.
   * PUT or PATCH SsCourses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-course/update:
   *   post:
   *     description: Api update SsCourse
   *     security:
   *     tags:
   *       - SsCourse
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseUpdate
   *          schema:
   *            $ref: '#/definitions/SsCourseUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rulesUpdate = {
        data_update: "object"
      };
      const data = request.only(Object.keys(SsCourse.rulesUpdate));
      const dataUpdate = request.only(Object.keys(rulesUpdate));
      const validation = await validateAll(data, SsCourse.rulesUpdate);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      if (isEmpty(dataUpdate.data_update)) {
        let responseData = {
          success: false,
          message: "data update empty"
        };
        return response.json(responseData);
      }
      await SsCourse.query()
        .where(data)
        .update(dataUpdate.data_update);
      const res = await SsCourse.query()
        .where({ ...data, ...dataUpdate.data_update })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a SsCourse with id.
   * DELETE SsCourses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-course/delete:
   *   post:
   *     description: Api delete SsCourse
   *     security:
   *     tags:
   *       - SsCourse
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseDelete
   *          schema:
   *            $ref: '#/definitions/SsCourseDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        shikibetu: "required|string",
        set_date: "required|string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      await SsCourse.query()
        .where({
          shikibetu: data.shikibetu,
          set_date: new Date(data.set_date)
        })
        .delete();

      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SsCourseController;
