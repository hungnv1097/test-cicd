"use strict";
const SmBefaftGroupSyosai = use("App/Models/SmBefaftGroupSyosai");
const SmBefaftGroup = use("App/Models/SmBefaftGroup");
const SmKouteiMst = use("App/Models/SmKouteiMst");
const Util = use("App/helpers/Util");
const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmBefaftGroupSyosais
 */
class SmBefaftGroupSyosaiController {
  /**
   * Show a list of all SmBefaftGroupSyosais.
   * GET SmBefaftGroupSyosais
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-befaft-group-syosai/list:
   *   get:
   *     security:
   *     tags:
   *       - SmBefaftGroupSyosai
   *     summary: Sample API list SmBefaftGroupSyosai
   *     parameters:
   *       - name: befaft_group_id
   *         description: befaft_group_id
   *         in: query
   *         type: integer
   *       - name: syo_koutei_code
   *         description: syo_koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;
      const query = SmBefaftGroupSyosai.query();
      const befaftGroupQuery = SmBefaftGroup.query();
      if (!isEmpty(params.befaft_group_id)) {
        query.where({ befaft_group_id: params.befaft_group_id });
      }
      if (!isEmpty(params.syo_koutei_code)) {
        query.where(
          "syo_koutei_code",
          "ILIKE",
          "%" + params.syo_koutei_code + "%"
        );
      }
      let befaftGroupsId;
      if (!isEmpty(params.befaft_group_name)) {
        befaftGroupQuery.where(
          "befaft_group_name",
          "LIKE",
          "%" + params.befaft_group_name + "%"
        );
        const befaftGroups = (await befaftGroupQuery.fetch()).toJSON();
        befaftGroupsId = befaftGroups.map(
          befaftGroup => befaftGroup.befaft_group_id
        );
      }
      if (befaftGroupsId) {
        if (!befaftGroupsId.length) {
          return {
            success: true,
            pagination: {
              perPage,
              page,
              lastPage: page - 1,
              total: 0
            },
            data: []
          };
        }
        query.where("befaft_group_id", "IN", befaftGroupsId);
      }
      let data = await query.paginate(page, perPage);
      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmBefaftGroupSyosai.
   * GET SmBefaftGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * Create/save a new SmBefaftGroupSyosai.
   * POST SmBefaftGroupSyosais
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group-syosai/create:
   *   post:
   *     description: Api create SmBefaftGroupSyosai
   *     security:
   *     tags:
   *       - SmBefaftGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupSyosaiCreate
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupSyosaiCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      const rules = {
        befaft_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        const responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }


      // const befaftGroupSyosai = await SmBefaftGroupSyosai.query().where({ befaft_group_id: data.befaft_group_id }).fetch();
      // if (isEmpty(befaftGroupSyosai.rows)) {
      //   return response.json({
      //     success: false,
      //     message: "befaft_group_id not exist",
      //     statusCode: 409
      //   });
      // }

      // const befaftGroup = await SmBefaftGroup.query().where({ befaft_group_id: data.befaft_group_id }).fetch();
      // if (isEmpty(befaftGroup.rows)) {
      //   return response.json({
      //     success: false,
      //     message: "befaft_group_id not exist",
      //     statusCode: 501
      //   });
      // }

      const kouteiMst = await SmKouteiMst.query().where({ koutei_code: data.koutei_code }).fetch();
      if (isEmpty(kouteiMst.rows)) {
        return response.json({
          success: false,
          message: "koutei_code not exist",
          statusCode: 501
        });
      }

      const checkSmBefaftGroupSyosai = await SmBefaftGroupSyosai.query().where(data).fetch();
      if (!isEmpty(checkSmBefaftGroupSyosai.rows)) {
        return response.json({
          success: false,
          message: "BefaftGroupSyosai exist",
          statusCode: 501
        });
      }

      const res = await SmBefaftGroupSyosai.create(data);
      const responseData = {
        success: true,
        message: "create success",
        data: res
      };

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SmBefaftGroupSyosai details.
   * PUT or PATCH SmBefaftGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group-syosai/update:
   *   post:
   *     description: Api update SmBefaftGroupSyosai
   *     security:
   *     tags:
   *       - SmBefaftGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupSyosaiCreate
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupSyosaiUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        befaft_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      let rulesUpdate = {
        data_update: "object"
      };
      const data = request.only(Object.keys(rules));
      const dataUpdate = request.only(Object.keys(rulesUpdate));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      //check bảng
      let befaftGroup = await SmBefaftGroup.query()
        .where({ befaft_group_id: data.befaft_group_id })
        .fetch();
      if (isEmpty(befaftGroup.rows)) {
        return response.json({
          success: false,
          message: "befaft_group_id not exist",
          statusCode: 501
        });
      }

      let kouteiMst = await SmKouteiMst.query()
        .where({ koutei_code: data.koutei_code })
        .fetch();
      if (isEmpty(kouteiMst.rows)) {
        return response.json({
          success: false,
          message: "syo_koutei_code not exist",
          statusCode: 501
        });
      }

      let checkSmBefaftGroupSyosai = await SmBefaftGroupSyosai.query()
        .where({ ...data, ...dataUpdate.data_update })
        .fetch();
      if (!isEmpty(checkSmBefaftGroupSyosai.rows)) {
        return response.json({
          success: false,
          message: "BefaftGroupSyosai exist",
          statusCode: 501
        });
      }

      await SmBefaftGroupSyosai.query()
        .where(data)
        .update(dataUpdate.data_update);
      const res = await SmBefaftGroupSyosai.query()
        .where({ ...data, ...dataUpdate.data_update })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a SmBefaftGroupSyosai with id.
   * DELETE SmBefaftGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group-syosai/delete:
   *   post:
   *     description: Api delete SmBefaftGroupSyosai
   *     security:
   *     tags:
   *       - SmBefaftGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupSyosaiDelete
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupSyosaiDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ request, response }) {
    try {
      const datas = request.body;
      let rules = {
        befaft_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      for(let data of datas) {
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      await SmBefaftGroupSyosai.query()
        .where({
          befaft_group_id: data.befaft_group_id,
          syo_koutei_code: data.syo_koutei_code,
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .delete();
      }
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SmBefaftGroupSyosaiController;
