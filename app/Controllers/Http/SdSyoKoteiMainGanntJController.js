'use strict'
const Database = use("Database");
const Util = use('App/helpers/Util')
const SdSyoKoteiMainGanntJ = use('App/Models/SdSyoKoteiMainGanntJ')

const { validateAll } = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')
const Redis = use('Redis')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SdSyoKoteiMainGanntJs
 */
class SdSyoKoteiMainGanntJController {
  /**
   * Show a list of all SdSyoKoteiMainGanntJs.
   * GET SdSyoKoteiMainGanntJs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-gannt-j/list:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGanntJ
   *     summary: Sample API list SdSyoKoteiMainGanntJ
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: koteikeikaku_no
   *         in: query
   *         type: string
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntJResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SdSyoKoteiMainGanntJ.query()
      if (!isEmpty(params.koteikeikaku_no)) {
        query.where('koteikeikaku_no', 'LIKE', '%' + params.koteikeikaku_no + '%')
      }
      if (!isEmpty(params.seihin_code)) {
        query.where('seihin_code', 'LIKE', '%' + params.seihin_code + '%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Create/save a new SdSyoKoteiMainGanntJ.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-gannt-j/create:
   *   post:
   *     description: Api create SdSyoKoteiMainGanntJ
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGanntJ
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntJCreate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntJCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntJResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        koteikeikaku_no: "required|string|max:20",
        daisu: 'required|integer',
        syo_koutei_syurui_seq: 'required|integer',
        syo_koutei_koho_seq: 'required|integer',
        proc_from: "required|date",
        proc_to: "required|date",
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        main_sub: "required|string",
        syo_koutei_seq: "required|date",
        syo_koutei_junjo: "required|date",
      }
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiMainGanntJ.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SdSyoKoteiMainGanntJ.
   * GET SdSyoKoteiMainGanntJs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt-j/detail:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGanntJ
   *     summary: Sample API
   *     parameters:
   *       - name: seq
   *         description: seq
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntJResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response }) {
    try {
      let rules = {
        seq: 'integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiMainGanntJ
        .query()
        .where({ seq: data.seq })
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Update SdSyoKoteiMainGanntJ details.
   * PUT or PATCH SdSyoKoteiMainGanntJs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt-j/update:
   *   post:
   *     description: Api update SdSyoKoteiMainGanntJ
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGanntJ
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntJUpdate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntJUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntJResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rulesUpdate = {
        seq: 'integer',
        daisu: 'required|integer',
        syo_koutei_syurui_seq: 'required|integer',
        syo_koutei_koho_seq: 'required|integer',
        proc_from: "required|date",
        proc_to: "required|date",
        main_sub: "string",
      }
      const data = request.only(Object.keys(rulesUpdate))
      const validation = await validateAll(data, rulesUpdate)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      let trx = await Database.beginTransaction();
      await SdSyoKoteiMainGanntJ.query().where({
        'seq': data.seq,
      }).update(data, trx)
      const res = await SdSyoKoteiMainGanntJ.query().where({
        'seq': data.seq
      }).fetch()

      trx.commit();

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SdSyoKoteiMainGanntJ with id.
   * DELETE SdSyoKoteiMainGanntJs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt-j/delete:
   *   post:
   *     description: Api delete SdSyoKoteiMainGanntJ
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGanntJ
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntJDelete
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntJDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seq: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SdSyoKoteiMainGanntJ
        .query()
        .where({
          'seq': data.seq
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SdSyoKoteiMainGanntJController
