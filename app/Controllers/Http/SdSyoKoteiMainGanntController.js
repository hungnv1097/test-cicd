"use strict";
const Database = use("Database");

const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");
const SdSyoKoteiMainGanntJ = use("App/Models/SdSyoKoteiMainGanntJ");
const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");

const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Util = require("../../helpers/Util");

const Logger = use("Logger");
const Redis = use("Redis");
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with sdsyokoteimaingannts
 */
class SdSyoKoteiMainGanntController {
  /**
   * Show a list of all sdsyokoteimaingannts.
   * GET sdsyokoteimaingannts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/list:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Sample API list SdSyoKoteiMainGannt
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: koteikeikaku_no
   *         in: query
   *         type: string
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all();

      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SdSyoKoteiMainGannt.query();
      if (!isEmpty(params.koteikeikaku_no)) {
        query.where(
          "koteikeikaku_no",
          "LIKE",
          "%" + params.koteikeikaku_no + "%"
        );
      }
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }

      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new sdsyokoteimaingannt.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/create:
   *   post:
   *     description: Api create SdSyoKoteiMainGannt
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntCreate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        // koteikeikaku_no: "required|string|max:20",
        koteikeikaku_no: "string|max:20",
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        proc_from: "required|date",
        proc_to: "required|date",
        main_sub: "required|string|max:1",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const existSyokouteset = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existSyokouteset) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No syokoutei set exist`
        });
      }

      // const existMainGannts = (
      //   await SdSyoKoteiMainGannt.query()
      //     .where({
      //       seihin_code: data.seihin_code,
      //       koutei_code: data.koutei_code,
      //       syo_koutei_code: data.syo_koutei_code,
      //       // koteikeikaku_no: data.koteikeikaku_no,
      //       syo_koutei_seq: existSyokouteset.syo_koutei_seq
      //     })
      //     .limit(3)
      //     .fetch()
      // ).toJSON();

      // const MainSubs = ["m", "a", "b"];

      // const hashByMainSub = existMainGannts.reduce((hs, item) => {
      //   hs[item.main_sub] = true;
      //   return hs;
      // }, {});

      // const mainSub = MainSubs.find(ms => !hashByMainSub[ms]);

      let trx = await Database.beginTransaction();
      let res = await SdSyoKoteiMainGannt.create(
        {
          ...data,
          lock_flag: true,
          syo_koutei_seq: existSyokouteset.syo_koutei_seq,
          syo_koutei_junjo: existSyokouteset.syo_koutei_junjo,
          main_sub: data.main_sub
        },
        trx
      );

      await SdSyoKoteiMainGanntJ.create(
        {
          ...data,
          syo_koutei_seq: existSyokouteset.syo_koutei_seq,
          syo_koutei_junjo: existSyokouteset.syo_koutei_junjo,
          main_sub: data.main_sub
        },
        trx
      );
      trx.commit();

      let newListSetGannts = [];
      let { std_proc_syohin_su, std_proc_sokudo } = existSyokouteset;
      //   標準処理製品数/標準処理速度
      if (std_proc_syohin_su != undefined && std_proc_sokudo != undefined) {
        std_proc_syohin_su = +std_proc_syohin_su;
        std_proc_sokudo = +std_proc_sokudo;

        let procMinute = Math.ceil(std_proc_syohin_su / std_proc_sokudo);

        const ganntsByProcess = (
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: existSyokouteset.seihin_code,
              koutei_code: existSyokouteset.koutei_code
            })
            .fetch()
        ).toJSON();

        let isFirstGannt = false;
        if (!ganntsByProcess.length) {
          isFirstGannt = true;
        } else {
          const minSeq =
            ganntsByProcess.length == 1
              ? ganntsByProcess[0]
              : ganntsByProcess.reduce((min, item) => {
                  return item.syo_koutei_junjo < min.syo_koutei_junjo
                    ? item
                    : min;
                }, ganntsByProcess[0]);

          isFirstGannt =
            existSyokouteset.syo_koutei_junjo <= minSeq.syo_koutei_junjo;
        }

        const ganntsOrder = {
          b: 1,
          m: 2,
          a: 3
        };

        const subProcceses = (
          await SmSyokouteiSet.query()
            .whereRaw(
              `
                    seihin_code = ? and koutei_code = ? and syo_koutei_junjo > ?
                    `,
              [
                existSyokouteset.seihin_code,
                existSyokouteset.koutei_code,
                existSyokouteset.syo_koutei_junjo
              ]
            )
            .orderBy("syo_koutei_junjo", "asc")
            .fetch()
        ).toJSON();

        let gannts = (
          await SdSyoKoteiMainGannt.query()
            .whereRaw(
              `
                seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                    ${[
                      `'${existSyokouteset.syo_koutei_code}'`,
                      ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
                    ].join(",")}
                )
                `,
              [existSyokouteset.seihin_code, existSyokouteset.koutei_code]
            )
            .fetch()
        ).toJSON();

        gannts = [...gannts, res.toJSON()];

        gannts.sort((a, b) => {
          return ganntsOrder[a.main_sub] - ganntsOrder[b.main_sub];
        });

        const ganntsBySubProcesses = gannts.reduce((hs, item) => {
          hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
          hs[item.syo_koutei_code].push(item);
          return hs;
        }, {});

        const setGannts = (
          await SmSyokouteiSetGannt.query()
            .whereRaw(
              `
                  seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                      ${[
                        `'${existSyokouteset.syo_koutei_code}'`,
                        ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
                      ].join(",")}
                  )
                  `,
              [existSyokouteset.seihin_code, existSyokouteset.koutei_code]
            )
            .fetch()
        ).toJSON();

        const setGanntsBySubProcesses = setGannts.reduce((hs, item) => {
          hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
          hs[item.syo_koutei_code].push(item);
          return hs;
        }, {});

        const newSetGannts = (
          setGanntsBySubProcesses[existSyokouteset.syo_koutei_code] || []
        ).reduce((hs, item) => {
          hs[item.main_sub] = item;
          return hs;
        }, {});

        const hash = ganntsByProcess.reduce((hs, item) => {
          hs[
            `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`
          ] = item;
          return hs;
        }, {});

        if (isFirstGannt) {
          newListSetGannts = Util.generateFirstSetGannt(
            existSyokouteset,
            ganntsBySubProcesses[existSyokouteset.syo_koutei_code] || [],
            newSetGannts,
            procMinute
          );
        } else {
          const prevSubProcceses = (
            await SmSyokouteiSet.query()
              .whereRaw(
                `
                            seihin_code = ? and koutei_code = ? and syo_koutei_junjo < ?
                            `,
                [
                  existSyokouteset.seihin_code,
                  existSyokouteset.koutei_code,
                  existSyokouteset.syo_koutei_junjo
                ]
              )
              .orderBy("syo_koutei_junjo", "desc")
              .limit(1)
              .fetch()
          ).toJSON();
          if (!prevSubProcceses.length) {
            let responseData = {
              success: false,
              statusCode: 501,
              error: "Got some error, prev set gannt not exist"
            };
            return response.json(responseData);
          }
          newListSetGannts = Util.generateNextSetGannt(
            existSyokouteset,
            ganntsByProcess,
            ganntsBySubProcesses[existSyokouteset.syo_koutei_code] || [],
            newSetGannts,
            procMinute,
            prevSubProcceses[0]
          );
          if (!newListSetGannts) {
            let responseData = {
              success: false,
              statusCode: 501,
              error: "Got some error, prev set gannt not exist"
            };
            return response.json(responseData);
          }
        }

        let prevSubProcceses = null;
        for (const subProcess of subProcceses) {
          if (
            subProcess.std_proc_sokudo &&
            subProcess.std_proc_syohin_su != undefined &&
            subProcess.std_proc_syohin_su !== ""
          ) {
            prevSubProcceses =
              prevSubProcceses ||
              (
                await SmSyokouteiSet.query()
                  .whereRaw(
                    `
                          seihin_code = ? and koutei_code = ? and syo_koutei_junjo < ?
                          `,
                    [
                      subProcess.seihin_code,
                      subProcess.koutei_code,
                      subProcess.syo_koutei_junjo
                    ]
                  )
                  .orderBy("syo_koutei_junjo", "desc")
                  .limit(1)
                  .fetch()
              ).toJSON()[0];

            if (
              prevSubProcceses.seihin_code === existSyokouteset.seihin_code &&
              prevSubProcceses.koutei_code === existSyokouteset.koutei_code &&
              prevSubProcceses.syo_koutei_code ===
                existSyokouteset.syo_koutei_code
            ) {
              prevSubProcceses = {
                ...prevSubProcceses,
                ...existSyokouteset
              };
            }

            newListSetGannts.forEach(item => {
              const k = `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`;
              hash[k] = {
                ...hash[k],
                ...item
              };
            });

            const _newListSetGannts = Util.generateNextSetGannt(
              subProcess,
              Object.keys(hash).map(k => hash[k]),
              ganntsBySubProcesses[subProcess.syo_koutei_code] || [],
              (
                setGanntsBySubProcesses[subProcess.syo_koutei_code] || []
              ).reduce((hs, item) => {
                hs[item.main_sub] = item;
                return hs;
              }, {}),
              Math.ceil(
                +subProcess.std_proc_syohin_su / +subProcess.std_proc_sokudo
              ),
              prevSubProcceses
            );
            if (!_newListSetGannts) {
              let responseData = {
                success: false,
                statusCode: 501,
                error: "Got some error, prev set gannt not exist"
              };
              return response.json(responseData);
            }
            newListSetGannts = [...newListSetGannts, ..._newListSetGannts];

            prevSubProcceses = subProcess;
          }
        }
      }

      trx = await Database.beginTransaction();
      let lastSeq = (await SmSyokouteiSetGannt.query().getMax("seq")) || 24;
      for (const gannt of newListSetGannts) {
        const exist = (
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .limit(1)
            .fetch()
        ).toJSON();
        if (exist[0]) {
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .update(gannt, trx);
        } else {
          gannt.seq = lastSeq + 1;
          const newd = (await SmSyokouteiSetGannt.create(gannt, trx)).toJSON();
          lastSeq = newd.seq;
        }
      }
      trx.commit();
      let responseData = {
        success: true,
        message: "create success",
        data: {...res, proc_from: new Date(res.proc_from), proc_to: new Date(res.proc_to)}
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new sdsyokoteimaingannt.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/multiple-create-update:
   *   post:
   *     description: Api create/update list SdSyoKoteiMainGannt
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Api create/update list SdSyoKoteiMainGannt
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntMultipleCreateUpdate
   *          schema:
   *              type: object
   *              $ref: '#/definitions/SdSyoKoteiMainGanntMultipleCreateUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async multipleCreateUpdate({ request, response }) {
    try {
      let rules = {
        syokotei_main_gannt_list: "array"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let seihin_code = null;
      let koteikeikaku_no = null;
      // return response.status(200).json(data.syokotei_main_gannt_list);
      for (let i in data.syokotei_main_gannt_list) {
        const mainGannt = data.syokotei_main_gannt_list[i];
        delete mainGannt.key;
        Logger.info("Processing %s", i, mainGannt);
        if (koteikeikaku_no == null) {
          koteikeikaku_no = mainGannt.koteikeikaku_no;
        }
        if (seihin_code == null) {
          seihin_code = mainGannt.seihin_code;
        }
        //Check Maingannt is new or update
        const gannt = await SdSyoKoteiMainGannt.findOrCreate(
          {
            koteikeikaku_no: mainGannt.koteikeikaku_no,
            seihin_code: mainGannt.seihin_code,
            koutei_code: mainGannt.koutei_code,
            syo_koutei_code: mainGannt.syo_koutei_code,
            syo_koutei_seq: mainGannt.syo_koutei_seq,
            syo_koutei_junjo: mainGannt.syo_koutei_junjo,
            seq: !!mainGannt.seq ? mainGannt.seq : 0
          },
          mainGannt
        );

        const ganntj = await SdSyoKoteiMainGanntJ.findOrCreate(
          {
            koteikeikaku_no: mainGannt.koteikeikaku_no,
            seihin_code: mainGannt.seihin_code,
            koutei_code: mainGannt.koutei_code,
            syo_koutei_code: mainGannt.syo_koutei_code,
            syo_koutei_seq: mainGannt.syo_koutei_seq,
            syo_koutei_junjo: mainGannt.syo_koutei_junjo
          },
          mainGannt
        );

        const updatedGannt = await SdSyoKoteiMainGannt.query()
          .where({
            koteikeikaku_no: gannt.koteikeikaku_no,
            seihin_code: gannt.seihin_code,
            koutei_code: gannt.koutei_code,
            syo_koutei_code: gannt.syo_koutei_code,
            syo_koutei_seq: gannt.syo_koutei_seq,
            syo_koutei_junjo: gannt.syo_koutei_junjo,
            // syo_koutei_syurui_seq: gannt.syo_koutei_syurui_seq,
            // syo_koutei_koho_seq: gannt.syo_koutei_koho_seq,
            seq: gannt.seq
          })
          .update(mainGannt);
      }

      const mainGannts = await SdSyoKoteiMainGannt.query()
        .where({ seihin_code: seihin_code, koteikeikaku_no: koteikeikaku_no })
        .fetch();

      let responseData = {
        success: true,
        message: "create success",
        data: mainGannts
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single sdsyokoteimaingannt.
   * GET sdsyokoteimaingannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/detail:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Sample API
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: page
   *         in: query
   *         type: string
   *       - name: koteikeikaku_seq
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response }) {
    try {
      let rules = {
        seq: "integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SdSyoKoteiMainGannt.query()
        .where({ seq: data.seq })
        .fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update sdsyokoteimaingannt details.
   * PUT or PATCH sdsyokoteimaingannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/update:
   *   post:
   *     description: Api update SdSyoKoteiMainGannt
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntUpdate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        seq: "required|number",
        proc_from: "required|date",
        proc_to: "required|date",
        main_sub: "required|string|max:1"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const existMainGannt = (
        await SdSyoKoteiMainGannt.query()
          .where({
            seq: data.seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existMainGannt) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No main gannt exist`
        });
      }

      const existMainSubMainGannt = (
        await SdSyoKoteiMainGannt.query()
          .where({
            koteikeikaku_no: existMainGannt.koteikeikaku_no,
            seihin_code: existMainGannt.seihin_code,
            koutei_code: existMainGannt.koutei_code,
            syo_koutei_code: existMainGannt.syo_koutei_code,
            main_sub: data.main_sub
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existMainSubMainGannt) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Main gannt main sub duplicate`
        });
      }

      await SdSyoKoteiMainGannt.query()
        .where({
          seq: data.seq
        })
        .update(data);

      const res = (
        await SdSyoKoteiMainGannt.query()
          .where({
            seq: data.seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a sdsyokoteimaingannt with id.
   * DELETE sdsyokoteimaingannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-gannt/delete:
   *   post:
   *     description: Api delete SdSyoKoteiMainGannt
   *     security:
   *     tags:
   *       - SdSyoKoteiMainGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainGanntDelete
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainGanntDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      await SdSyoKoteiMainGannt.query()
        .where({
          seq: data.seq
        })
        .delete();
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SdSyoKoteiMainGanntController;
