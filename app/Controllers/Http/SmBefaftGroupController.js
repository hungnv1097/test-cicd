'use strict'
const SmBefaftGroup = use('App/Models/SmBefaftGroup')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmBefaftGroups
 */
class SmBefaftGroupController {
  /**
   * Show a list of all SmBefaftGroups.
   * GET SmBefaftGroups
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-befaft-group/list:
   *   get:
   *     security:
   *     tags:
   *       - SmBefaftGroup
   *     summary: Sample API list SmBefaftGroup
   *     parameters:
   *       - name: befaft_group_name
   *         description: befaft_group_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response, view}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SmBefaftGroup.query()
      if(!isEmpty(params.befaft_group_name)){
        query.where('befaft_group_name', 'LIKE', '%'+params.befaft_group_name+'%')
      }
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmBefaftGroup.
   * GET SmBefaftGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-befaft-group/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmBefaftGroup
   *     summary: Sample API
   *     parameters:
   *       - name: befaft_group_id
   *         description: page
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response, view}) {
    try {
      let rules = {
        befaft_group_id: 'required|integer'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmBefaftGroup
        .query()
        .where({'befaft_group_id': data.befaft_group_id})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmBefaftGroup.
   * POST SmBefaftGroups
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group/create:
   *   post:
   *     description: Api create SmBefaftGroup
   *     security:
   *     tags:
   *       - SmBefaftGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupCreate
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      let rules = {
        befaft_group_name: 'required|string|max:64'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      
      let search_befaft_group_name = await SmBefaftGroup.query().where({befaft_group_name:data.befaft_group_name}).fetch();
      if(!isEmpty(search_befaft_group_name.rows)) {
        return response.status(400).json({
          success: false,
          message: "befaft_group_name already exist",
          statusCode: 409
        });
      }
      let res = await SmBefaftGroup.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmBefaftGroup details.
   * PUT or PATCH SmBefaftGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group/update:
   *   post:
   *     description: Api update SmBefaftGroup
   *     security:
   *     tags:
   *       - SmBefaftGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupCreate
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmBefaftGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      let rules = {
        befaft_group_id: 'required|integer',
        befaft_group_name: 'string|max:64'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmBefaftGroup.query().where({
        'befaft_group_id': data.befaft_group_id
      }).update(data)
      const res = await SmBefaftGroup.query().where({
        'befaft_group_id': data.befaft_group_id
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmBefaftGroup with id.
   * DELETE SmBefaftGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-befaft-group/delete:
   *   post:
   *     description: Api delete SmBefaftGroup
   *     security:
   *     tags:
   *       - SmBefaftGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmBefaftGroupDelete
   *          schema:
   *            $ref: '#/definitions/SmBefaftGroupDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      let rules = {
        befaft_group_id: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmBefaftGroup
        .query()
        .where({
          'befaft_group_id': data.befaft_group_id
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmBefaftGroupController
