'use strict'

const Util = use('App/helpers/Util')
const SdSyoKoteiDenpyo = use('App/Models/SdSyoKoteiDenpyo')

const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')
const Redis = use('Redis')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SdSyoKoteiDenpyos
 */
class SdSyoKoteiDenpyoController {
  /**
   * Show a list of all SdSyoKoteiDenpyos.
   * GET SdSyoKoteiDenpyos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /sd-syo-kotei-denpyo/list:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiDenpyo
   *     summary: Sample API list SdSyoKoteiDenpyo
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: koteikeikaku_no
   *         in: query
   *         type: string
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiDenpyoResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SdSyoKoteiDenpyo.query()
      if (!isEmpty(params.koteikeikaku_no)) {
        query.where('koteikeikaku_no', 'LIKE', '%' + params.koteikeikaku_no + '%')
      }
      if (!isEmpty(params.seihin_code)) {
        query.where('seihin_code', 'LIKE', '%' + params.seihin_code + '%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Create/save a new SdSyoKoteiDenpyo.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sd-syo-kotei-denpyo/create:
   *   post:
   *     description: Api create SdSyoKoteiDenpyo
   *     security:
   *     tags:
   *       - SdSyoKoteiDenpyo
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiDenpyoCreate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiDenpyoCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiDenpyoResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      const data = request.only(Object.keys(SdSyoKoteiDenpyo.rules))
      const validation = await validateAll(data, SdSyoKoteiDenpyo.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiDenpyo.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SdSyoKoteiDenpyo.
   * GET SdSyoKoteiDenpyos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sd-syo-kotei-denpyo/detail:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiDenpyo
   *     summary: Sample API
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiDenpyoResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response}) {
    try {
      let rules = {
        koteikeikaku_no: 'string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiDenpyo
        .query()
        .where({koteikeikaku_no: data.koteikeikaku_no})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Update SdSyoKoteiDenpyo details.
   * PUT or PATCH SdSyoKoteiDenpyos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-denpyo/update:
   *   post:
   *     description: Api update SdSyoKoteiDenpyo
   *     security:
   *     tags:
   *       - SdSyoKoteiDenpyo
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiDenpyoUpdate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiDenpyoUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiDenpyoResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      const data = request.only(Object.keys(SdSyoKoteiDenpyo.rulesUpdate))
      const validation = await validateAll(data, SdSyoKoteiDenpyo.rulesUpdate)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SdSyoKoteiDenpyo.query().where({
        koteikeikaku_no: data.koteikeikaku_no,
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        syo_koutei_seq: data.syo_koutei_seq,
        syo_koutei_junjo: data.syo_koutei_junjo,
      }).update({tantosya: data.tantosya})

      const res = await SdSyoKoteiDenpyo.query().where({
        koteikeikaku_no: data.koteikeikaku_no,
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        syo_koutei_seq: data.syo_koutei_seq,
        syo_koutei_junjo: data.syo_koutei_junjo
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SdSyoKoteiDenpyo with id.
   * DELETE SdSyoKoteiDenpyos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-denpyo/delete:
   *   post:
   *     description: Api delete SdSyoKoteiDenpyo
   *     security:
   *     tags:
   *       - SdSyoKoteiDenpyo
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiDenpyoDelete
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiDenpyoDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      const data = request.only(Object.keys(SdSyoKoteiDenpyo.rulesDelete))
      const validation = await validateAll(data, SdSyoKoteiDenpyo.rulesDelete)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SdSyoKoteiDenpyo
        .query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SdSyoKoteiDenpyoController
