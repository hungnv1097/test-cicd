"use strict";

const SmSyokouteiSyurui = use("App/Models/SmSyokouteiSyurui");
const Database = use('Database')
const { validateAll } = use('Validator')
class SmSyokouteiSyuruiController {

    async list() {
        return await Database.select('*').from('s_m_syokoutei_syurui');
    }

    async create({ request, response}) {
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_name: 'string|max:128',
            }
            const data = request.only(Object.keys(rules)) 
            const validation = await validateAll(data, rules)
            if (validation.fails()) {
                let responseData = {
                    success: false,
                    message: validation.messages()[0].message,
                    statusCode: 501,
                    errors: validation.messages()
                }
                return response.json(responseData)
            }
            let res = await SmSyokouteiSyurui.create(data);
            let responseData = {
                success: true,
                message: 'create success',
                data: res
            }

            return response.json(responseData)
        }catch(err){
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async update({request, response}) {
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
                syo_koutei_syurui_name: 'string|max:128'
            };
            const data = request.only(Object.keys(rules)) 
            const validation = await validateAll(data, rules)
            if (validation.fails()) {
                let responseData = {
                    success: false,
                    message: validation.messages()[0].message,
                    statusCode: 501,
                    errors: validation.messages()
                }
                return response.json(responseData)
            }

            await SmSyokouteiSyurui.query().where({
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
            }).update({syo_koutei_syurui_name: data.syo_koutei_syurui_name});
            let res = await SmSyokouteiSyurui.query().where({
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
            }).fetch();
            let responseData = {
                data: res.rows[0],
                success: true,
                message: 'Updated success'
            }
            return response.json(responseData)

        } catch(err) {
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async delete({request, response}){
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
            }
            let datas = request.body;
            if(datas.length == 1) {
                await SmSyokouteiSyurui.query().where({
                    seihin_code: datas[0].seihin_code,
                    koutei_code: datas[0].koutei_code,
                    syo_koutei_syurui_seq: datas[0].syo_koutei_syurui_seq,
                }).delete();
            } else {
                for(let data of datas ) {
                    const validation = await validateAll(data, rules);
                    if (validation.fails()) {
                        let responseData = {
                            success: false,
                            message: validation.messages()[0].message,
                            statusCode: 501,
                            errors: validation.messages()
                        }
                        return response.json(responseData)
                    }
                    await SmSyokouteiSyurui.query().where({
                        seihin_code: data.seihin_code,
                        koutei_code: data.koutei_code,
                        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                    }).delete();
                }
            }
            const res = await Database.select('*').from('s_m_syokoutei_syurui');
            let responseData = {
                data: res,
                success: true,
                message: 'deleted success',
            }
            return response.json(responseData)
        }catch(err){
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async updateJunjo({ request, response }) {
        try {
            let rules = {
                seihin_code: "required|string",
                koutei_code: "required|string",
                syo_koutei_syurui_seq: "required|integer",
                syokoutei_syurui_junjo: "integer"
            };
            const data = request.body;

            for (const dt of data) {
                const validation = await validateAll(dt, rules);
                if (validation.fails()) {
                    let responseData = {
                        success: false,
                        message: validation.messages()[0].message,
                        statusCode: 501,
                        errors: validation.messages()
                    };
                    return response.json(responseData);
                }
            }

            const trx = await Database.beginTransaction();
            await Promise.all(
                data.map(dt =>
                    SmSyokouteiSyurui.query()
                        .where({
                            seihin_code: dt.seihin_code,
                            koutei_code: dt.koutei_code,
                            syo_koutei_syurui_seq: dt.syo_koutei_syurui_seq,
                            syokoutei_syurui_junjo: dt.syokoutei_syurui_junjo
                        })
                        .update(dt, trx)
                )
            );

            trx.commit();

            let responseData = {
                success: true,
                message: "update junjo success"
            };
            return response.json(responseData);
        } catch (error) {
            console.error(error + "");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                error: `An error has occured ${error}`
            });
        }
    }

}

module.exports = SmSyokouteiSyuruiController;