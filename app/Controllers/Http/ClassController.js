'use strict'

const Class = use('App/Models/Class');
const isEmpty = require('lodash.isempty');

/**
 * Resourceful controller for interacting with Class
 */
class ClassController {
  async list({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = Class.query()

      if (!isEmpty(params.sort_field) && !isEmpty(params.sort_order)) {
        query.orderBy(params.sort_field, params.sort_order)
      }

      if (!isEmpty(params.workset_id)) {
        query.where('workset_id', params.workset_id)
      }

      const data = await query.paginate(page, perPage);

      const res = {
        success: true,
        message: "Success",
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.toJSON().data
      }

      return response.json(res)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = ClassController
