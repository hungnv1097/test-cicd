'use strict'

const Util = use('App/helpers/Util')
const SsCourse = use('App/Models/SsCourse')
const SsCourseRec = use('App/Models/SsCourseRec')

const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsCourseRecs
 */
class SsCourseRecController {
  /**
   * Show a list of all SsCourseRecs.
   * GET SsCourseRecs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /ss-course-rec/list:
   *   get:
   *     security:
   *     tags:
   *       - SsCourseRec
   *     summary: Sample API list SsCourseRec
   *     parameters:
   *       - name: shikibetu
   *         description: shikibetu
   *         in: query
   *         type: string
   *       - name: no
   *         description: no
   *         in: query
   *         type: integer
   *       - name: set_date
   *         description: set_date
   *         in: query
   *         type: string
   *       - name: start_set_date
   *         description: start_set_date
   *         in: query
   *         type: string
   *       - name: end_set_date
   *         description: end_set_date
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseRecResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SsCourseRec.query()
      if (!isEmpty(params.shikibetu)) {
        query.where('shikibetu', 'LIKE', '%' + params.shikibetu + '%')
      }
      if (!isEmpty(params.no)) {
        query.where({'no': params.no})
      }
      if (!isEmpty(params.set_date)) {
        query.where({'set_date': params.set_date})
      }
      if (!isEmpty(params.start_set_date)) {
        query.where('set_date', '>=', params.start_set_date)
      }
      if (!isEmpty(params.end_set_date)) {
        query.where('set_date', '<=', params.end_set_date)
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Create/save a new SsCourseRec.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /ss-course-rec/create:
   *   post:
   *     description: Api create SsCourseRec
   *     security:
   *     tags:
   *       - SsCourseRec
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseRecCreate
   *          schema:
   *            $ref: '#/definitions/SsCourseRecCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseRecResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      const data = request.only(Object.keys(SsCourseRec.rules))
      const validation = await validateAll(data, SsCourseRec.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }


      const check = await SsCourse.query().where({
        'shikibetu': data.shikibetu,
        'set_date': new Date(data.set_date_original),
      }).first()
      if(isEmpty(check)){
        let responseData = {
          success: false,
          message: 'SsCourse not found.',
        }
        return response.json(responseData)
      }

      delete data.set_date_original;

      let res = await SsCourseRec.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SsCourseRec.
   * GET SsCourseRecs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-course-rec/detail:
   *   get:
   *     security:
   *     tags:
   *       - SsCourseRec
   *     summary: Sample API
   *     parameters:
   *       - name: no
   *         description: no
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseRecResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response}) {
    try {
      let rules = {
        no: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsCourseRec
        .query()
        .where({'no': data.no})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Update SsCourseRec details.
   * PUT or PATCH SsCourseRecs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-course-rec/update:
   *   post:
   *     description: Api update SsCourseRec
   *     security:
   *     tags:
   *       - SsCourseRec
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseRecUpdate
   *          schema:
   *            $ref: '#/definitions/SsCourseRecUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsCourseRecResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      const data = request.only(Object.keys(SsCourseRec.rulesUpdate))
      const validation = await validateAll(data, SsCourseRec.rulesUpdate)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SsCourseRec.query().where({
        'no': data.no
      }).update(data)
      const res = await SsCourseRec.query().where({
        'no': data.no
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SsCourseRec with id.
   * DELETE SsCourseRecs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-course-rec/delete:
   *   post:
   *     description: Api delete SsCourseRec
   *     security:
   *     tags:
   *       - SsCourseRec
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsCourseRecDelete
   *          schema:
   *            $ref: '#/definitions/SsCourseRecDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      let rules = {
        no: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SsCourseRec
        .query()
        .where({
          'no': data.no
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SsCourseRecController
