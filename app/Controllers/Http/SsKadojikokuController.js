'use strict'
const SsKadojikoku = use('App/Models/SsKadojikoku')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Database = use('Database')
const _ = require('lodash')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsKadojikokus
 */
class SsKadojikokuController {
  /**
   * Show a list of all SsKadojikokus.
   * GET SsKadojikokus
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-kadojikoku/list:
   *   get:
   *     security:
   *     tags:
   *       - SsKadojikoku
   *     summary: Sample API list SsKadojikoku
   *     parameters:
   *       - name: start_date
   *         description: Start date
   *         in: query
   *         type: string
   *       - name: end_date
   *         description: End date
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKadojikokuResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SsKadojikoku.query()
      if (!isEmpty(params.start_date)) {
        query.where('set_date', '>=', params.start_date)
      }

      if (!isEmpty(params.end_date)) {
        query.where('set_date', '<=', params.end_date)
      }
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SsKadojikoku.
   * GET SsKadojikokus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-kadojikoku/detail:
   *   get:
   *     security:
   *     tags:
   *       - SsKadojikoku
   *     summary: Sample API
   *     parameters:
   *       - name: set_date
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKadojikokuResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        set_date: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsKadojikoku
        .query()
        .where({'set_date': data.set_date})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SsKadojikoku.
   * POST SsKadojikokus
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-kadojikoku/create:
   *   post:
   *     description: Api create SsKadojikoku
   *     security:
   *     tags:
   *       - SsKadojikoku
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKadojikokuCreate
   *          schema:
   *            $ref: '#/definitions/SsKadojikokuCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKadojikokuResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        set_date: 'required|string',
        kadojikoku_from: 'string|max:4',
        kadojikoku_to: 'string|max:4',
        kyukei1_from: 'string|max:4',
        kyukei1_to: 'string|max:4',
        kyukei2_from: 'string|max:4',
        kyukei2_to: 'string|max:4',
        kyukei3_from: 'string|max:4',
        kyukei3_to: 'string|max:4',
        hi_kadobi: 'required|boolean',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      let res = await SsKadojikoku.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SsKadojikoku details.
   * PUT or PATCH SsKadojikokus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-kadojikoku/update:
   *   post:
   *     description: Api update SsKadojikoku
   *     security:
   *     tags:
   *       - SsKadojikoku
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKadojikokuCreate
   *          schema:
   *            $ref: '#/definitions/SsKadojikokuUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKadojikokuResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ request, response }) {
    try {
      let datas = request.body;
      let rules = {
          set_date: 'required|string',
          kadojikoku_from: 'string|max:4',
          kadojikoku_to: 'string|max:4',
          kyukei1_from: 'string|max:4',
          kyukei1_to: 'string|max:4',
          kyukei2_from: 'string|max:4',
          kyukei2_to: 'string|max:4',
          kyukei3_from: 'string|max:4',
          kyukei3_to: 'string|max:4',
          hi_kadobi: 'boolean',
        }
      let dataRes = []
      if(!isEmpty(datas)){
        for (let data of datas) {
          const validation = await validateAll(data, rules)
          if (validation.fails()) {
            let responseData = {
              success: false,
              message: validation.messages()[0].message,
              statusCode: 501,
              errors: validation.messages()
            }
            return response.json(responseData)
          }
          const exited = await Database.from('s_s_kadojikoku').where('set_date', new Date(data.set_date));
          if (exited.length > 0) { // update
            if(data.kadojikoku_from == "0" && data.kadojikoku_to == "2400") {
              data.kadojikoku_from = "";
              data.kadojikoku_to = "";
            }
            await SsKadojikoku.query().where({set_date: data.set_date}).update(data);
          }else { // create

            if(data.kadojikoku_from && data.kadojikoku_to){
              await SsKadojikoku.query().where({set_date: data.set_date}).insert(data);
            } else {
              data.kadojikoku_from = "";
              data.kadojikoku_to = "";
              await SsKadojikoku.query().where({set_date: data.set_date}).insert(data);
            }
          }
          let res = await SsKadojikoku.query().where({set_date: data.set_date}).first()
          dataRes.push(res)
        }
      }

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: dataRes
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SsKadojikoku with id.
   * DELETE SsKadojikokus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-kadojikoku/delete:
   *   post:
   *     description: Api delete SsKadojikoku
   *     security:
   *     tags:
   *       - SsKadojikoku
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKadojikokuDelete
   *          schema:
   *            $ref: '#/definitions/SsKadojikokuDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        set_date: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SsKadojikoku
        .query()
        .where({
          'set_date': data.set_date
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SsKadojikokuController
