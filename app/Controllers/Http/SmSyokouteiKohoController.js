"use strict";

const SmSyokouteiKoho = use("App/Models/SmSyokouteiKoho");
const Database = use('Database')
const { validateAll } = use('Validator')
const SdSyokouteiKohoDenpyo = use('App/Models/SdSyokouteiKohoDenpyo');
class SmSyokouteiKohoController {

    async list() {
        // const query = SmSyokouteiMst.query()
        return await Database.select('*').from('s_m_syokoutei_koho')
    }

    async create({ request, response}) {
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
                syo_koutei_koho_name: 'string|max:128'
            }
            const data = request.only(Object.keys(rules)) 
            const validation = await validateAll(data, rules)
            if (validation.fails()) {
                let responseData = {
                    success: false,
                    message: validation.messages()[0].message,
                    statusCode: 501,
                    errors: validation.messages()
                }
                return response.json(responseData)
            }
            const trx = await Database.beginTransaction();

            let res = await SmSyokouteiKoho.create(data);
            let responseData = {
                success: true,
                message: 'create success',
                data: res
            }

            // find koteikeikaku_no s_t_seisansiji
            let smKakuList = await Database.table('s_t_seisansiji')
            .select('seihin_code','koteikeikaku_no')
            .where({
                seihin_code: data.seihin_code
            })

            // insert to s_d_syokoutei_koho_denpyo
            let smKohoList = await Database.table('s_d_syokoutei_koho_denpyo')
            .select('koutei_code','koteikeikaku_no')
            .where({
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                koutei_code: data.koutei_code,
                seihin_code: data.seihin_code
            })
            .groupBy(['koutei_code', 'koteikeikaku_no'])

            if(smKohoList && smKohoList.length > 0) {
                smKohoList = smKohoList.map(obj => ({ 
                    ...obj, 
                    syo_koutei_syurui_seq: data.syo_koutei_syurui_seq, 
                    seihin_code: data.seihin_code,
                    syo_koutei_koho_seq: res.syo_koutei_koho_seq
                }))

                await SdSyokouteiKohoDenpyo.query().insert(smKohoList, trx)
            } else {
                smKakuList = smKakuList.map(obj => ({ 
                    ...obj, 
                    syo_koutei_syurui_seq: data.syo_koutei_syurui_seq, 
                    koutei_code: data.koutei_code,
                    syo_koutei_koho_seq: res.syo_koutei_koho_seq,
                    saiyo_flag: true
                }))

                await SdSyokouteiKohoDenpyo.query().insert(smKakuList, trx)
            }

            return response.json(responseData)
        } catch(err){
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async update({request, response}) {
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
                syo_koutei_koho_seq: 'integer',
                syo_koutei_koho_name: 'string|max:128'
            }
            const data = request.only(Object.keys(rules)) 
            const validation = await validateAll(data, rules)
            if (validation.fails()) {
                let responseData = {
                    success: false,
                    message: validation.messages()[0].message,
                    statusCode: 501,
                    errors: validation.messages()
                }
                return response.json(responseData)
            }

            await SmSyokouteiKoho.query().where({
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_koho_seq: data.syo_koutei_koho_seq,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
            }).update({syo_koutei_koho_name: data.syo_koutei_koho_name});
            let res = await SmSyokouteiKoho.query().where({
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            }).fetch();
            let responseData = {
                data: res.rows[0],
                success: true,
                message: 'Updated success',
            }
            return response.json(responseData)
        }catch(err){
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async delete({request, response}){
        try{
            let rules = {
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
                syo_koutei_koho_seq: 'integer',
            }
            let datas = request.body;

            // Delete all row have syo_koutei_syurui_seq = syurui in s_d_syokoutei_koho_denpyo
            const deleteKohoDenpyo = async (koho, syurui, koutei) => {
                await SdSyokouteiKohoDenpyo.query().where({
                    syo_koutei_koho_seq: koho,
                    syo_koutei_syurui_seq: syurui,
                    koutei_code: koutei
                }).delete();
            }

            // Update flag true in s_d_syokoutei_koho_denpyo when have syo_koutei_syurui_seq = syurui
            const updateFlagKohoDenpyo = async (syurui, koutei) => {
                let kohoDenpyo = await Database.table('s_d_syokoutei_koho_denpyo').where({
                    syo_koutei_syurui_seq: syurui,
                    koutei_code: koutei
                });
                
                let memoryKaku = []; 

                kohoDenpyo.filter(obj => {
                    if(obj.saiyo_flag) {
                        memoryKaku.push(obj.koteikeikaku_no)
                    }
                })

                let kohoDenpyoUpdate = [];

                kohoDenpyo.map(obj => {
                    if(!memoryKaku.includes(obj.koteikeikaku_no)) {
                        kohoDenpyoUpdate.push({...obj })
                        memoryKaku.push(obj.koteikeikaku_no)
                    }
                })

                for(let koho of kohoDenpyoUpdate ) {
                    await SdSyokouteiKohoDenpyo
                    .query()
                    .where({
                        'syo_koutei_koho_seq': koho.syo_koutei_koho_seq,
                        'koteikeikaku_no': koho.koteikeikaku_no
                    })
                    .update({
                        saiyo_flag: true
                    })
                }
                
            }

            if(datas.length == 1) {
                await SmSyokouteiKoho.query().where({
                    seihin_code: datas[0].seihin_code,
                    koutei_code: datas[0].koutei_code,
                    syo_koutei_koho_seq: datas[0].syo_koutei_koho_seq,
                    syo_koutei_syurui_seq: datas[0].syo_koutei_syurui_seq,
                }).delete();
                await deleteKohoDenpyo(datas[0].syo_koutei_koho_seq, datas[0].syo_koutei_syurui_seq, datas[0].koutei_code)
                await updateFlagKohoDenpyo(datas[0].syo_koutei_syurui_seq, datas[0].koutei_code)
            } else {
                for(let data of datas ) { 
                    const validation = await validateAll(data, rules);
                    if (validation.fails()) {
                        let responseData = {
                            success: false,
                            message: validation.messages()[0].message,
                            statusCode: 501,
                            errors: validation.messages()
                        }
                        return response.json(responseData)
                    }
                    await SmSyokouteiKoho.query().where({
                        seihin_code: data.seihin_code,
                        koutei_code: data.koutei_code,
                        syo_koutei_koho_seq: data.syo_koutei_koho_seq,
                        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                    }).delete();
                    await deleteKohoDenpyo(data.syo_koutei_koho_seq, data.syo_koutei_syurui_seq, data.koutei_code)
                }
                await updateFlagKohoDenpyo(datas[0].syo_koutei_syurui_seq, datas[0].koutei_code)
            }
            
            // await SmSyokouteiKoho.query().where({
            //     seihin_code: data.seihin_code,
            // }).fetch()
            let responseData = {
                success: true,
                message: 'deleted success',
            }
            return response.json(responseData)
        }catch(err){
            console.error(error);
            return response.status(400).send({
                success: false,
                statusCode: 400,
                // error: `エラーを発生しました： ${error}`
            });
        }
    }

    async updateJunjo({ request, response }) {
        try {
            let rules = {
                seihin_code: "required|string",
                koutei_code: "required|string",
                syo_koutei_syurui_seq: "required|integer",
                syo_koutei_koho_seq: "required|integer",
                syokoutei_koho_junjo: "integer"
            };
            const data = request.body;

            for (const dt of data) {
                const validation = await validateAll(dt, rules);
                if (validation.fails()) {
                    let responseData = {
                        success: false,
                        message: validation.messages()[0].message,
                        statusCode: 501,
                        errors: validation.messages()
                    };
                    return response.json(responseData);
                }
            }

            const trx = await Database.beginTransaction();
            await Promise.all(
                data.map(dt =>
                    SmSyokouteiKoho.query()
                        .where({
                            seihin_code: dt.seihin_code,
                            koutei_code: dt.koutei_code,
                            syo_koutei_syurui_seq: dt.syo_koutei_syurui_seq,
                            syo_koutei_koho_seq: dt.syo_koutei_koho_seq,
                            syokoutei_koho_junjo: dt.syokoutei_koho_junjo
                        })
                        .update(dt, trx)
                )
            );

            trx.commit();

            let responseData = {
                success: true,
                message: "update junjo success"
            };
            return response.json(responseData);
        } catch (error) {
            console.error(error + "");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                error: `An error has occured ${error}`
            });
        }
    }

}

module.exports = SmSyokouteiKohoController;