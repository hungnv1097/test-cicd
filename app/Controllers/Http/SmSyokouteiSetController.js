"use strict";
const Database = use("Database");

const SmSeihinMst = use("App/Models/SmSeihinMst");
const SmKouteiMst = use("App/Models/SmKouteiMst");
const SmSyokouteiMst = use("App/Models/SmSyokouteiMst");
const SmSouchiMst = use("App/Models/SmSouchiMst");
const SmSagyoMst = use("App/Models/SmSagyoMst");
const StSeisansiji = use("App/Models/StSeisansiji");
const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");
const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");
const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");
const SdSyoKoteiMainGanntJ = use("App/Models/SdSyoKoteiMainGanntJ");
const SdSyoKoteiMainSyain = use("App/Models/SdSyoKoteiMainSyain");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");

const SmBefaftGroup = use("App/Models/SmBefaftGroup");
const SmBefaftGroupSyosai = use("App/Models/SmBefaftGroupSyosai");

const SmKonsaiGroupSyosai = use("App/Models/SmKonsaiGroupSyosai");
const SmKonsaiGroup = use("App/Models/SmKonsaiGroup");

const SdSyoKoteiDenpyo = use("App/Models/SdSyoKoteiDenpyo");

const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Util = require("../../helpers/Util");

const Redis = use("Redis");
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with smsyokouteisets
 */
class SmSyokouteiSetController {
  /**
   * Show a list of all smsyokouteisets.
   * GET smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-set/list-main-gannt:
   */
  async listMainGannt({ request, response }) {
    try {
      let params = request.all();
      let rules = {
        // koteikeikaku_no: "required|string|max:20",
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const validation = await validateAll(params, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const list = (
        await SmSyokouteiSetGannt.query()
          .where({
            // koteikeikaku_no: params.koteikeikaku_no,
            seihin_code: params.seihin_code,
            koutei_code: params.koutei_code,
            syo_koutei_code: params.syo_koutei_code,
            syo_koutei_koho_seq: params.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: params.syo_koutei_syurui_seq
          })
          .fetch()
      ).toJSON();

      let res = {
        success: true,
        data: list
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API list SmSyokouteiSet
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmSyokouteiSet.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      if (!isEmpty(params.koutei_code)) {
        query.where("koutei_code", "LIKE", "%" + params.koutei_code + "%");
      }
      // let data = await query.with('syo_koutei').with('s_m_syokoutei_syo_list', (builder) => {
      //   builder.with('sagyo').with('souchi')
      // }).paginate(page, perPage)
      let data = await query.paginate(page, perPage);
      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        // data: data.toJSON().data
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmSyokouteiSet.
   * GET SmSyokouteiSets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-set/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_code
   *         description: page
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response, view }) {
    try {
      let params = request.all();
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_koho_seq: "integer",
        syo_koutei_syurui_seq: "integer"
      };
      const validation = await validateAll(params, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      let res = await SmSyokouteiSet.query()
        .where({
          seihin_code: params.seihin_code,
          koutei_code: params.koutei_code,
          syo_koutei_code: params.syo_koutei_code,
          syo_koutei_koho_seq: params.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: params.syo_koutei_syurui_seq
        })
        .first();

      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new smsyokouteiset.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set/create:
   *   post:
   *     description: Api create SmSyokouteiSet
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      const data = request.only(Object.keys(SmSyokouteiSet.rules));
      const validation = await validateAll(data, SmSyokouteiSet.rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SmSyokouteiSet.create(data);

      let responseData = {
        success: true,
        message: "create success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/create-full:
   *   post:
   *     description: Api create SmSyokouteiSet
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async createFull({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        souchi_code: "required_without_any:sagyo_code|string|max:20",
        sagyo_code: "required_without_any:souchi_code|string|max:20",
        tantosya: "required|string",
        befaft_group_id: "integer",
        konsai_group_id: "integer",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };

      /* #region Validate */
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const exitSyokouteiSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON();
      if (exitSyokouteiSet.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Duplicate syokoutei set`
        });
      }

      const existSeihin = (
        await SmSeihinMst.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];
      if (!existSeihin) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Seihin master not exist`
        });
      }

      const existKoutei = (
        await SmKouteiMst.query()
          .where({
            koutei_code: data.koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];
      if (!existKoutei) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Koutei master not exist`
        });
      }

      const existSyokoutei = (
        await SmSyokouteiMst.query()
          .where({
            syo_koutei_code: data.syo_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];
      if (!existSyokoutei) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Syokoutei master not exist`
        });
      }

      if (!isEmpty(data.souchi_code)) {
        const existSouchi = (
          await SmSouchiMst.query()
            .where({
              souchi_code: data.souchi_code,
            })
            .limit(1)
            .fetch()
        ).toJSON()[0];
        if (!existSouchi) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Souchi master not exist`,
          });
        }
      }

      if (!isEmpty(data.sagyo_code)) {
        const existSagyo = (
          await SmSagyoMst.query()
            .where({
              sagyo_code: data.sagyo_code,
            })
            .limit(1)
            .fetch()
        ).toJSON()[0];
        if (!existSagyo) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Sagyo master not exist`,
          });
        }
      }

      if (!isEmpty(data.befaft_group_id)) {
        const existBefaftGroup = (
          await SmBefaftGroup.query()
            .where({
              befaft_group_id: data.befaft_group_id
            })
            .limit(1)
            .fetch()
        ).toJSON()[0];
        if (!existBefaftGroup) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Befaft group not exist`
          });
        }
      }

      if (!isEmpty(data.konsai_group_id)) {
        const existKonsaiGroup = (
          await SmKonsaiGroup.query()
            .where({
              konsai_group_id: data.konsai_group_id
            })
            .limit(1)
            .fetch()
        ).toJSON()[0];
        if (!existKonsaiGroup) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Konsai group not exist`
          });
        }
      }

      const existSeisansiji = await StSeisansiji.query().where({
        seihin_code: data.seihin_code,
      }).fetch();
      if (existSeisansiji.rows.length == 0) {
        return response.status(400).json({
          success: false,
          statusCode: 400,
          error: `Can't not find any seihin_code in StSeisansiji`
        })
      }
      /* #endregion */

      const trx = await Database.beginTransaction();
      const syokoutei = (
        await SmSyokouteiSet.create(
          {
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          },
          trx
        )
      ).toJSON();

      for (let i = 0; i < existSeisansiji.rows.length; i++) {
        await SdSyoKoteiDenpyo.create({
          koteikeikaku_no: existSeisansiji.rows[i].koteikeikaku_no,
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: syokoutei.syo_koutei_seq,
          syo_koutei_junjo: syokoutei.syo_koutei_junjo,
          tantosya: data.tantosya,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        },
          trx
        );
      }

      await SmSyokouteiSyoList.create(
        {
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: syokoutei.syo_koutei_seq,
          sagyo_code: data.sagyo_code ? data.sagyo_code : null,
          souchi_code: data.souchi_code ? data.souchi_code : null,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        },
        trx
      );

      const existBefaftGroupSyosai = await SmBefaftGroupSyosai.query().where({
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        befaft_group_id: data.befaft_group_id,
        syo_koutei_koho_seq: data.syo_koutei_koho_seq,
        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
      }).first();
      if (existBefaftGroupSyosai) {
        return response.status(400).json({
          success: false,
          statusCode: 400,
          error: `Duplicate Befaft_Group_Syosai`
        })
      }

      if (data.befaft_group_id) {
        await SmBefaftGroupSyosai.create(
          {
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            befaft_group_id: data.befaft_group_id,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          },
          trx
        );
      }

      const existSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query().where({
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        konsai_group_id: data.konsai_group_id,
        syo_koutei_koho_seq: data.syo_koutei_koho_seq,
        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
      }).first();
      if (existSmKonsaiGroupSyosai) {
        return response.status(400).json({
          success: false,
          statusCode: 400,
          error: `Duplicate Sm_Konsai_Group_Syosai`
        })
      }
      if (data.konsai_group_id) {
        await SmKonsaiGroupSyosai.create(
          {
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            konsai_group_id: data.konsai_group_id,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          },
          trx
        );
      }
      // await SdSyoKoteiMainSyain.create({
      //   koteikeikaku_no: data.koteikeikaku_no,
      //   seihin_code: data.seihin_code,
      //   koutei_code: data.koutei_code,
      //   syo_koutei_code: data.syo_koutei_code,
      //   syo_koutei_seq: syokoutei.syo_koutei_seq,
      //   syo_koutei_junjo: syokoutei.syo_koutei_junjo
      // });

      trx.commit();

      let responseData = {
        success: true,
        message: "create success",
        data: {
          ...syokoutei,
          koteikeikaku_no: data.koteikeikaku_no,
          syo_kotei_name: existSyokoutei.syo_kotei_name,
        }
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update smsyokouteiset details.
   * PUT or PATCH smsyokouteisets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set/update:
   *   post:
   *     description: Api update SmSyokouteiSet
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Update map subprocess vs master process
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetUpdate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      const data = request.only(Object.keys(SmSyokouteiSet.rules));
      const validation = await validateAll(data, SmSyokouteiSet.rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      let newListSetGannts = [];
      let { std_proc_syohin_su, std_proc_sokudo } = data;
      //   標準処理製品数/標準処理速度
      if (std_proc_syohin_su != undefined && std_proc_sokudo != undefined) {
        std_proc_syohin_su = +std_proc_syohin_su;
        std_proc_sokudo = +std_proc_sokudo;

        let procMinute = Math.ceil(std_proc_syohin_su / std_proc_sokudo);
        const ganntsByProcess = (
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code
            })
            .fetch()
        ).toJSON();

        let isFirstGannt = false;
        if (!ganntsByProcess.length) {
          isFirstGannt = true;
        } else {
          const minSeq =
            ganntsByProcess.length == 1
              ? ganntsByProcess[0]
              : ganntsByProcess.reduce((min, item) => {
                return item.syo_koutei_junjo < min.syo_koutei_junjo
                  ? item
                  : min;
              }, ganntsByProcess[0]);

          isFirstGannt = data.syo_koutei_junjo <= minSeq.syo_koutei_junjo;
        }

        const ganntsOrder = {
          b: 1,
          m: 2,
          a: 3
        };

        const subProcceses = (
          await SmSyokouteiSet.query()
            .whereRaw(
              `
                  seihin_code = ? and koutei_code = ? and syo_koutei_junjo > ?
                  `,
              [data.seihin_code, data.koutei_code, data.syo_koutei_junjo]
            )
            .orderBy("syo_koutei_junjo", "asc")
            .fetch()
        ).toJSON();

        const gannts = (
          await SdSyoKoteiMainGannt.query()
            .whereRaw(
              `
                seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                    ${[
                `'${data.syo_koutei_code}'`,
                ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
              ].join(",")}
                )
                `,
              [data.seihin_code, data.koutei_code]
            )
            .fetch()
        ).toJSON();

        gannts.sort((a, b) => {
          return ganntsOrder[a.main_sub] - ganntsOrder[b.main_sub];
        });

        const ganntsBySubProcesses = gannts.reduce((hs, item) => {
          hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
          hs[item.syo_koutei_code].push(item);
          return hs;
        }, {});

        const setGannts = (
          await SmSyokouteiSetGannt.query()
            .whereRaw(
              `
                seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                    ${[
                `'${data.syo_koutei_code}'`,
                ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
              ].join(",")}
                )
                `,
              [data.seihin_code, data.koutei_code]
            )
            .fetch()
        ).toJSON();

        const setGanntsBySubProcesses = setGannts.reduce((hs, item) => {
          hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
          hs[item.syo_koutei_code].push(item);
          return hs;
        }, {});

        const newSetGannts = (
          setGanntsBySubProcesses[data.syo_koutei_code] || []
        ).reduce((hs, item) => {
          hs[item.main_sub] = item;
          return hs;
        }, {});

        const hash = ganntsByProcess.reduce((hs, item) => {
          hs[
            `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`
          ] = item;
          return hs;
        }, {});

        if (isFirstGannt) {
          newListSetGannts = Util.generateFirstSetGannt(
            data,
            ganntsBySubProcesses[data.syo_koutei_code] || [],
            newSetGannts,
            procMinute
          );
        } else {
          const prevSubProcceses = (
            await SmSyokouteiSet.query()
              .whereRaw(
                `
                        seihin_code = ? and koutei_code = ? and syo_koutei_junjo < ?
                        `,
                [data.seihin_code, data.koutei_code, data.syo_koutei_junjo]
              )
              .orderBy("syo_koutei_junjo", "desc")
              .limit(1)
              .fetch()
          ).toJSON();
          if (!prevSubProcceses.length) {
            let responseData = {
              success: false,
              statusCode: 501,
              error: "Got some error, prev set gannt not exist"
            };
            return response.json(responseData);
          }
          newListSetGannts = Util.generateNextSetGannt(
            data,
            ganntsByProcess,
            ganntsBySubProcesses[data.syo_koutei_code] || [],
            newSetGannts,
            procMinute,
            prevSubProcceses[0]
          );
          if (!newListSetGannts) {
            let responseData = {
              success: false,
              statusCode: 501,
              error: "Got some error, prev set gannt not exist"
            };
            return response.json(responseData);
          }
        }

        let prevSubProcceses = null;
        for (const subProcess of subProcceses) {
          if (
            subProcess.std_proc_sokudo &&
            subProcess.std_proc_syohin_su != undefined &&
            subProcess.std_proc_syohin_su !== ""
          ) {
            prevSubProcceses =
              prevSubProcceses ||
              (
                await SmSyokouteiSet.query()
                  .whereRaw(
                    `
                            seihin_code = ? and koutei_code = ? and syo_koutei_junjo < ?
                            `,
                    [
                      subProcess.seihin_code,
                      subProcess.koutei_code,
                      subProcess.syo_koutei_junjo
                    ]
                  )
                  .orderBy("syo_koutei_junjo", "desc")
                  .limit(1)
                  .fetch()
              ).toJSON()[0];

            if (
              prevSubProcceses.seihin_code === data.seihin_code &&
              prevSubProcceses.koutei_code === data.koutei_code &&
              prevSubProcceses.syo_koutei_code === data.syo_koutei_code
            ) {
              prevSubProcceses = {
                ...prevSubProcceses,
                ...data
              };
            }

            newListSetGannts.forEach(item => {
              const k = `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`;
              hash[k] = {
                ...hash[k],
                ...item
              };
            });

            const _newListSetGannts = Util.generateNextSetGannt(
              subProcess,
              Object.keys(hash).map(k => hash[k]),
              ganntsBySubProcesses[subProcess.syo_koutei_code] || [],
              (
                setGanntsBySubProcesses[subProcess.syo_koutei_code] || []
              ).reduce((hs, item) => {
                hs[item.main_sub] = item;
                return hs;
              }, {}),
              Math.ceil(
                +subProcess.std_proc_syohin_su / +subProcess.std_proc_sokudo
              ),
              prevSubProcceses
            );
            if (!_newListSetGannts) {
              let responseData = {
                success: false,
                statusCode: 501,
                error: "Got some error, prev set gannt not exist"
              };
              return response.json(responseData);
            }

            newListSetGannts = [...newListSetGannts, ..._newListSetGannts];

            prevSubProcceses = subProcess;
          }
        }
      }

      const trx = await Database.beginTransaction();
      await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code
        })
        .update(data, trx);

      let lastSeq = (await SmSyokouteiSetGannt.query().getMax("seq")) || 0;
      for (const gannt of newListSetGannts) {
        const exist = (
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .limit(1)
            .fetch()
        ).toJSON();
        if (exist[0]) {
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .update(gannt, trx);
        } else {
          gannt.seq = lastSeq + 1;
          const newd = (await SmSyokouteiSetGannt.create(gannt, trx)).toJSON();
          lastSeq = newd.seq;
        }
      }
      trx.commit();

      let res = await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code
        })
        .first();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };

      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/update-junjo:
   */
  async updateJunjo({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        syo_koutei_code: "required|string",
        syo_koutei_junjo: "integer"
      };
      const data = request.body;

      for (const dt of data) {
        const validation = await validateAll(dt, rules);
        if (validation.fails()) {
          let responseData = {
            success: false,
            message: validation.messages()[0].message,
            statusCode: 501,
            errors: validation.messages()
          };
          return response.json(responseData);
        }
      }

      const trx = await Database.beginTransaction();
      await Promise.all(
        data.map(async dt => {
          await SmSyokouteiSet.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(dt, trx);

          await SdSyoKoteiMainGannt.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(
              {
                syo_koutei_junjo: dt.syo_koutei_junjo
              },
              trx
            );

          await SdSyoKoteiMainGanntJ.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(
              {
                syo_koutei_junjo: dt.syo_koutei_junjo
              },
              trx
            );

          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(
              {
                syo_koutei_junjo: dt.syo_koutei_junjo
              },
              trx
            );

          await SdSyoKoteiMainSyain.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(
              {
                syo_koutei_junjo: dt.syo_koutei_junjo
              },
              trx
            );

          await SdSyoKoteiDenpyo.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code,
              syo_koutei_code: dt.syo_koutei_code
            })
            .update(
              {
                syo_koutei_junjo: dt.syo_koutei_junjo
              },
              trx
            );
        })
      );

      trx.commit();

      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "update junjo success"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `An error has occured ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/change-code:
   *   post:
   *     description: Api change koute code
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async changeCode({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        syo_koutei_code: "required|string",
        new_syo_koutei_code: "required|string",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const oldSyokoute = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!oldSyokoute) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No Syokoutei set exist`
        });
      }

      const existSyokouteSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.new_syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (existSyokouteSet) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Duplicate Syokoutei set`
        });
      }

      const existMasterSyokoute = (
        await SmSyokouteiMst.query()
          .where({
            syo_koutei_code: data.new_syo_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existMasterSyokoute) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No master syokoutei exist`
        });
      }

      const trx = await Database.beginTransaction();
      await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainGanntJ.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SmSyokouteiSetGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainSyain.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SdSyoKoteiDenpyo.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      await SmSyokouteiSyoList.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .update(
          {
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        );

      trx.commit();

      // <clear cache>
      Redis.keys("SdSyoKoteiMainGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainGanntJ_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainSyain_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiDenpyo_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSyoList_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      // </clear cache>

      const updated = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.new_syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: {
          ...updated,
          ...existMasterSyokoute
        }
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/clone:
   *   post:
   */
  async clone({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        syo_koutei_code: "required|string",

        new_syo_koutei_code: "required|string|max:8",
        new_syo_kotei_name: "required|string"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const oldSyokouteiSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!oldSyokouteiSet) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No syokoutei set exist`
        });
      }

      const oldSyokoteiMainGannt = (
        await SdSyoKoteiMainGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokoteiMainGanntJ = (
        await SdSyoKoteiMainGanntJ.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiSetGannt = (
        await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiMainSyain = (
        await SdSyoKoteiMainSyain.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiSyoList = (
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiDenpyo = (
        await SdSyoKoteiDenpyo.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code
          })
          .fetch()
      ).toJSON();

      const trx = await Database.beginTransaction();

      await SmSyokouteiMst.create(
        {
          syo_koutei_code: data.new_syo_koutei_code,
          syo_kotei_name: data.new_syo_kotei_name
        },
        trx
      );

      delete oldSyokouteiSet.__meta__;
      delete oldSyokouteiSet["s-m-syokoutei-set-gannt-list"];
      delete oldSyokouteiSet["s-m-syokoutei-syo-list"];
      delete oldSyokouteiSet.syo_koutei_seq;
      delete oldSyokouteiSet.syo_koutei_junjo;

      let newSyokouteiSet = (
        await SmSyokouteiSet.create(
          {
            ...oldSyokouteiSet,
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.new_syo_koutei_code
          },
          trx
        )
      ).toJSON();

      newSyokouteiSet = {
        syo_koutei_code: newSyokouteiSet.syo_koutei_code,
        syo_koutei_seq: newSyokouteiSet.syo_koutei_seq,
        syo_koutei_junjo: newSyokouteiSet.syo_koutei_junjo
      };

      await SdSyoKoteiMainGannt.createMany(
        oldSyokoteiMainGannt.map(item => {
          delete item.__meta__;
          delete item["s-d-syo-kotei-main-syain-list"];

          return {
            ...item,
            ...newSyokouteiSet
          };
        }),
        trx
      );

      await SdSyoKoteiMainGanntJ.createMany(
        oldSyokoteiMainGanntJ.map(item => {
          return {
            ...item,
            ...newSyokouteiSet
          };
        }),
        trx
      );

      await SmSyokouteiSetGannt.createMany(
        oldSyokouteiSetGannt.map(item => {
          return {
            ...item,
            ...newSyokouteiSet
          };
        }),
        trx
      );

      await SdSyoKoteiMainSyain.createMany(
        oldSyokouteiMainSyain.map(item => {
          return {
            ...item,
            ...newSyokouteiSet
          };
        }),
        trx
      );

      await SdSyoKoteiDenpyo.createMany(
        oldSyokouteiDenpyo.map(item => {
          return {
            ...item,
            ...newSyokouteiSet
          };
        }),
        trx
      );

      await SmSyokouteiSyoList.createMany(
        oldSyokouteiSyoList.map(item => {
          const newItem = {
            ...item,
            ...newSyokouteiSet
          };
          delete newItem.syo_koutei_junjo;
          return newItem;
        }),
        trx
      );

      trx.commit();

      Redis.keys("SmSyokouteiMst_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SdSyoKoteiMainGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainGanntJ_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainSyain_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiDenpyo_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSyoList_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "clone success",
        data: newSyokouteiSet
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/update-work-device:
   *   post:
   *     description: Api update map work vs device
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Api update map work vs device
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetUpdateWorkDevice
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetUpdateWorkDevice'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async updateWorkDevice({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "string",
        koutei_code: "string",
        syo_koutei_code: "string",
        sagyo_mst_souchi_mst: "array"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const res = await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code
        })
        .fetch();
      //map device,work
      if (!isEmpty(data.sagyo_mst_souchi_mst) && !isEmpty(res.rows)) {
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_seq: res.rows[0].syo_koutei_seq
          })
          .delete();

        let arrSagyoMstSouchiMst = data.sagyo_mst_souchi_mst;
        for (let x in arrSagyoMstSouchiMst) {
          let check = null;
          if (arrSagyoMstSouchiMst[x].seq !== undefined) {
            check = await SmSyokouteiSyoList.query()
              .where({
                // seihin_code: data.seihin_code,
                // koutei_code: data.koutei_code,
                // syo_koutei_code: data.syo_koutei_code,
                // syo_koutei_seq: res.rows[0].syo_koutei_seq,
                // sagyo_code: arrSagyoMstSouchiMst[x].sagyo_code,
                // souchi_code: arrSagyoMstSouchiMst[x].souchi_code,
                seq: arrSagyoMstSouchiMst[x].seq
              })
              .fetch();
          }

          if (check == null || isEmpty(check.rows)) {
            await SmSyokouteiSyoList.create({
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_code: data.syo_koutei_code,
              syo_koutei_seq: res.rows[0].syo_koutei_seq,
              sagyo_code: arrSagyoMstSouchiMst[x].sagyo_code,
              souchi_code: arrSagyoMstSouchiMst[x].souchi_code
            });
          }
        }
      } else if (
        data.sagyo_mst_souchi_mst !== undefined &&
        data.sagyo_mst_souchi_mst == null
      ) {
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_seq: res.rows[0].syo_koutei_seq
          })
          .delete();
      }

      // const resAll = await SmSyokouteiSet.query().with('syo_koutei').with('s_m_syokoutei_syo_list', (builder) => {
      //   builder.with('sagyo').with('souchi')
      // }).where({
      //   'seihin_code': data.seihin_code,
      //   'koutei_code': data.koutei_code,
      //   'syo_koutei_code': data.syo_koutei_code
      // }).fetch()
      const resAll = await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        // data: resAll.toJSON()
        data: resAll.rows
      };

      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSyoList_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a smsyokouteiset with id.
   * DELETE smsyokouteisets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set/delete:
   *   post:
   *     description: Api delete SmSyokouteiSet
   *     security:
   *     tags:
   *       - SmSyokouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetDelete
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        syo_koutei_code: "required|string",
        syo_koutei_seq: "required|integer",
        syo_koutei_junjo: "required|integer",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      let delSmSyokouteiSetGannt = await SmSyokouteiSetGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSmSyokouteiSyoList = await SmSyokouteiSyoList.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSmBefaftGroupSyosai = await SmBefaftGroupSyosai.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSdSyoKoteiDenpyo = await SdSyoKoteiDenpyo.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSdSyoKoteiMainGanntJ = await SdSyoKoteiMainGanntJ.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSmSyokouteiSet = await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();

      let delSdSyoKoteiMainGannt = await SdSyoKoteiMainGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        })
        .delete();


      Redis.keys("*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Upsert souchi code, saygo code.
   * POST
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set/update-tantosya:
   *   post:
   *     description: Api update SmSyokouteiSyoList
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSyoListCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSyoListUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async updateTantosya({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "required|integer",
        syo_koutei_junjo: "required|integer",
        tantosya: "required|string",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      const existSeisansiji = await StSeisansiji.query().where({
        seihin_code: data.seihin_code,
      }).fetch();

      for (let i = 0; i < existSeisansiji.length; i++) {
        let existSyoKouteiDenpyo = (
          await SdSyoKoteiDenpyo.query()
            .where({
              koteikeikaku_no: existSeisansiji[i].koteikeikaku_no,
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_code: data.syo_koutei_code,
              syo_koutei_seq: data.syo_koutei_seq,
              syo_koutei_junjo: data.syo_koutei_junjo,
              syo_koutei_koho_seq: data.syo_koutei_koho_seq,
              syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
            })
            .limit(1)
            .fetch()
        ).toJSON()[0];

        if (!existSyoKouteiDenpyo) {
          existSyoKouteiDenpyo = await SdSyoKoteiDenpyo.create({ koteikeikaku_no: existSeisansiji[i].koteikeikaku_no, ...data });
        } else {
          await SdSyoKoteiDenpyo.query()
            .where({
              koteikeikaku_no: existSeisansiji[i].koteikeikaku_no,
              seihin_code: existSyoKouteiDenpyo.seihin_code,
              koutei_code: existSyoKouteiDenpyo.koutei_code,
              syo_koutei_code: existSyoKouteiDenpyo.syo_koutei_code,
              syo_koutei_seq: existSyoKouteiDenpyo.syo_koutei_seq,
              syo_koutei_junjo: existSyoKouteiDenpyo.syo_koutei_junjo,
              syo_koutei_koho_seq: existSyoKouteiDenpyo.syo_koutei_koho_seq,
              syo_koutei_syurui_seq: existSyoKouteiDenpyo.syo_koutei_syurui_seq
            })
            .update({ ...data, koteikeikaku_no: existSeisansiji[i].koteikeikaku_no });
        }
      }

      // const res = (
      //   await SdSyoKoteiDenpyo.query()
      //     .where({
      //       koteikeikaku_no: existSyoKouteiDenpyo.koteikeikaku_no,
      //       seihin_code: existSyoKouteiDenpyo.seihin_code,
      //       koutei_code: existSyoKouteiDenpyo.koutei_code,
      //       syo_koutei_code: existSyoKouteiDenpyo.syo_koutei_code,
      //       syo_koutei_seq: existSyoKouteiDenpyo.syo_koutei_seq,
      //       syo_koutei_junjo: existSyoKouteiDenpyo.syo_koutei_junjo,
      //       syo_koutei_koho_seq: existSyoKouteiDenpyo.syo_koutei_koho_seq,
      //       syo_koutei_syurui_seq: existSyoKouteiDenpyo.syo_koutei_syurui_seq
      //     })
      //     .fetch()
      // ).toJSON()[0];

      let responseData = {
        success: true,
        message: "データを変更しました",
        // data: res
      };

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/upsert-befaft:
   *   post:
   */
  async upsertBefaft({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        befaft_group_id: "required|integer",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const existSyokouteiSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (!existSyokouteiSet.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Syokoutei set not exist`
        });
      }

      const existSyosai = (
        await SmBefaftGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            befaft_group_id: data.befaft_group_id,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON();

      let responseData = {
        success: true,
        message: "Successfully"
      }

      const trx = await Database.beginTransaction();

      if (existSyosai.length) {
        return response.json(responseData);
        // await SmBefaftGroupSyosai.query()
        //   .where({
        //     seihin_code: data.seihin_code,
        //     koutei_code: data.koutei_code,
        //     syo_koutei_code: data.syo_koutei_code,
        //     befaft_group_id: data.befaft_group_id,
        //     syo_koutei_koho_seq: data.syo_koutei_koho_seq,
        //     syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        //   })
        //   .update(data);

      }

      await SmBefaftGroupSyosai.create({
        befaft_group_id: data.befaft_group_id,
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        syo_koutei_koho_seq: data.syo_koutei_koho_seq,
        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
      }, trx);

      responseData = {
        success: true,
        message: "Update befaft group for syokoutei success",
        data: await SmBefaftGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            befaft_group_id: data.befaft_group_id,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .fetch()
      };

      trx.commit();

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
  /**
   * @swagger
   * /sm-syokoutei-set/detail-befaft:
   *   get:
   */
  async detailBefaft({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let responseData = {
        success: true,
        message: "success",
        data: (
          await SmBefaftGroupSyosai.query()
            .where({
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_code: data.syo_koutei_code
            })
            .limit(1)
            .fetch()
        ).toJSON()[0]
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/upsert-konsai:
   *   post:
   */
  async upsertKonsai({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        konsai_group_id: "required|string",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const existSyokouteiSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (!existSyokouteiSet.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Syokoutei set not exist`
        });
      }

      let existSyosai = (
        await SmKonsaiGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            konsai_group_id: data.konsai_group_id,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (existSyosai.length) {
        return response.json({
          success: true,
          statusCode: 200,
          message: 'Successfully'
        });
        // await SmKonsaiGroupSyosai.query()
        //   .where({
        //     seihin_code: data.seihin_code,
        //     koutei_code: data.koutei_code,
        //     syo_koutei_code: data.syo_koutei_code
        //   })
        //   .update(data);
      } else {
        await SmKonsaiGroupSyosai.create(data);
      }

      let responseData = {
        success: true,
        message: "Update konsai group for syokoutei success",
        data: (
          await SmBefaftGroupSyosai.query()
            .where({
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_code: data.syo_koutei_code
            })
            .limit(1)
            .fetch()
        ).toJSON()[0]
      };

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set/detail-konsai:
   *   get:
   *
   */
  async detailKonsai({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let responseData = {
        success: true,
        message: "success",
        data: (
          await SmKonsaiGroupSyosai.query()
            .where({
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_code: data.syo_koutei_code
            })
            .limit(1)
            .fetch()
        ).toJSON()[0]
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  async updateSokudo({ request, response }) {
    try {
      const data = request.only(Object.keys(SmSyokouteiSet.rules));

      const validation = await validateAll(data, SmSyokouteiSet.rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      const trx = await Database.beginTransaction();
      const existSyokoutei = await SmSyokouteiSet.query().where({
        seihin_code: data.seihin_code,
        koutei_code: data.koutei_code,
        syo_koutei_code: data.syo_koutei_code,
        syo_koutei_koho_seq: data.syo_koutei_koho_seq,
        syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
      },
        trx).first();
      if (existSyokoutei) {
        await SmSyokouteiSet.query().where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        }).update(data, trx);

        const res = await SmSyokouteiSet.query().where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
        }).fetch();

        let responseData = {
          success: true,
          message: 'データを変更しました',
          data: res
        }

        return response.status(200).json(responseData);
      }

    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `An error has occured ${error}`
      });
    }
  }
}

module.exports = SmSyokouteiSetController;
