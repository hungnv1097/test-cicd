"use strict";
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");
const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");

const Database = use("Database");
const Redis = use("Redis");
const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Util = require("../../helpers/Util");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSyokouteiSetGannts
 */
class SmSyokouteiSetGanntController {
  /**
   * Show a list of all SmSyokouteiSetGannts.
   * GET SmSyokouteiSetGannts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API list SmSyokouteiSetGannt
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmSyokouteiSetGannt.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      if (!isEmpty(params.koutei_code)) {
        query.where("koutei_code", "LIKE", "%" + params.koutei_code + "%");
      }
      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmSyokouteiSetGannt.
   * GET SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_code
   *         description: page
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required",
        koutei_code: "required"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SmSyokouteiSetGannt.query()
        .where({ seihin_code: data.seihin_code, koutei_code: data.koutei_code })
        .fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new SmSyokouteiSetGannt.
   * POST SmSyokouteiSetGannts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/create:
   *   post:
   *     description: Api create SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "integer",
        main_sub: "string",
        proc_from: "integer",
        proc_to: "integer",
        std_proc_syohin_su: "integer",
        proc_sokudo: "integer",
        syo_koutei_junjo: "required|integer",
        color: "string",
        syo_koutei_koho_seq: "integer",
        syo_koutei_syurui_seq: "integer"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      // let proc_from = new Date(data.proc_from).getTime();
      // let proc_to = new Date(data.proc_to).getTime();
      // let resultSeconds = Math.round((proc_to - proc_from) / 60000) * 60;

      const resultTimeSyokouteiSetGannt = await Database.select('*')
        .where({ seihin_code: data.seihin_code })
        .from('s_m_syokoutei_set_gannt');

      if (resultTimeSyokouteiSetGannt.length == 0) {
        data.proc_from = 0;
        // data.proc_to = resultSeconds;
      }
      // else {
      //   data.proc_from = resultTimeSyokouteiSetGannt[resultTimeSyokouteiSetGannt.length - 1].proc_to;
      //   data.proc_to = data.proc_from + resultSeconds;
      // }

      let res = await SmSyokouteiSetGannt.create(data);
      let responseData = {
        success: true,
        message: "create success",
        data: {
          ...res.toJSON()
        }
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SmSyokouteiSetGannt details.
   * PUT or PATCH SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/update:
   *   post:
   *     description: Api update SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer",
        seihin_code: "string|max:20",
        koutei_code: "string|max:20",
        syo_koutei_code: "string|max:20",
        syo_koutei_seq: "integer",
        main_sub: "string",
        proc_from: "integer",
        proc_to: "integer",
        std_proc_syohin_su: "integer",
        proc_sokudo: "integer",
        syo_koutei_junjo: "required|integer",
        color: "string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      // let proc_from = new Date(data.proc_from).getTime();
      // let proc_to = new Date(data.proc_to).getTime();
      // let resultSeconds = Math.round((proc_to - proc_from) / 60000) * 60;

      // const resultTimeSyokouteiSetGannt = await Database.select('*')
      //   .where({ seihin_code: data.seihin_code })
      //   .from('s_m_syokoutei_set_gannt');

      // if (resultTimeSyokouteiSetGannt.length == 0) {
      //   data.proc_from = 0;
      // }
      // else {
      //   data.proc_from = resultTimeSyokouteiSetGannt[resultTimeSyokouteiSetGannt.length - 1].proc_to;
      //   data.proc_to = data.proc_from + resultSeconds;
      // }

      await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .update(data);
      const res = await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new sdsyokoteimaingannt.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sm-syokoutei-set-gannt/multiple-create-update:
   *   post:
   *     description: Api create/update list SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Api create/update list SmSyokouteiSetGannt
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntMultipleCreateUpdate
   *          schema:
   *              type: object
   *              $ref: '#/definitions/SmSyokouteiSetGanntMultipleCreateUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async multipleCreateUpdate({ request, response }) {
    try {
      let rules = {
        syokoutei_set_gannt_list: "array"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let seq = null;
      for (let i in data.syokoutei_set_gannt_list) {
        const mainGannt = data.syokoutei_set_gannt_list[i];

        //Check Maingannt is new or update
        const gannt = await SmSyokouteiSetGannt.findOrCreate(
          {
            seihin_code: mainGannt.seihin_code,
            koutei_code: mainGannt.koutei_code,
            syo_koutei_code: mainGannt.syo_koutei_code,
            syo_koutei_seq: mainGannt.syo_koutei_seq,
            // main_sub: mainGannt.main_sub,
            // proc_from: mainGannt.proc_from,
            // proc_to: mainGannt.proc_to,
            // std_proc_syohin_su: mainGannt.std_proc_syohin_su,
            // proc_sokudo: mainGannt.proc_sokudo,
            syo_koutei_junjo: mainGannt.syo_koutei_junjo,
            // color: mainGannt.color,
            seq: !!mainGannt.seq ? mainGannt.seq : 0
          },
          mainGannt
        );

        const updatedGannt = await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: mainGannt.seihin_code,
            koutei_code: mainGannt.koutei_code,
            syo_koutei_code: mainGannt.syo_koutei_code,
            syo_koutei_seq: mainGannt.syo_koutei_seq,
            main_sub: mainGannt.main_sub,
            proc_from: mainGannt.proc_from,
            proc_to: mainGannt.proc_to,
            std_proc_syohin_su: mainGannt.std_proc_syohin_su,
            proc_sokudo: mainGannt.proc_sokudo,
            syo_koutei_junjo: mainGannt.syo_koutei_junjo,
            color: mainGannt.color,
            seq: gannt.seq
          })
          .update(mainGannt);

        if (seq == null) {
          seq = mainGannt.seq;
        }
      }

      const mainGannts = await SmSyokouteiSetGannt.query()
        .where({
          seq: seq
        })
        .fetch();

      let responseData = {
        success: true,
        message: "create success",
        data: mainGannts
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/update-gannts-time:
   *   post:
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async updateGanntsTime({ request, response }) {
    try {
      let rules = {
        syokoutei_set_gannt_list: "array"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const syokouteisetGanntList = data.syokoutei_set_gannt_list;
      const lastSyokouteisetGanntList =
        syokouteisetGanntList[syokouteisetGanntList.length - 1];
      const existSyokouteset = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: lastSyokouteisetGanntList.seihin_code,
            koutei_code: lastSyokouteisetGanntList.koutei_code,
            syo_koutei_code: lastSyokouteisetGanntList.syo_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existSyokouteset) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No syokoutei set exist`
        });
      }

      const subProcceses = (
        await SmSyokouteiSet.query()
          .whereRaw(
            `
                  seihin_code = ? and koutei_code = ? and syo_koutei_junjo > ?
                  `,
            [
              existSyokouteset.seihin_code,
              existSyokouteset.koutei_code,
              existSyokouteset.syo_koutei_junjo
            ]
          )
          .orderBy("syo_koutei_junjo", "asc")
          .fetch()
      ).toJSON();

      const gannts = (
        await SdSyoKoteiMainGannt.query()
          .whereRaw(
            `
              seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                  ${[
              `'${existSyokouteset.syo_koutei_code}'`,
              ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
            ].join(",")}
              )
              `,
            [existSyokouteset.seihin_code, existSyokouteset.koutei_code]
          )
          .fetch()
      ).toJSON();

      const ganntsOrder = {
        b: 1,
        m: 2,
        a: 3
      };

      gannts.sort((a, b) => {
        return ganntsOrder[a.main_sub] - ganntsOrder[b.main_sub];
      });

      const ganntsBySubProcesses = gannts.reduce((hs, item) => {
        hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
        hs[item.syo_koutei_code].push(item);
        return hs;
      }, {});

      const trx = await Database.beginTransaction();
      await Promise.all(
        data.syokoutei_set_gannt_list.map(async item => {
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: item.seihin_code,
              koutei_code: item.koutei_code,
              syo_koutei_code: item.syo_koutei_code,
              main_sub: item.main_sub
            })
            .update(
              {
                proc_from: item.proc_from,
                proc_to: item.proc_to
              },
              trx
            );
        })
      );
      trx.commit();

      const ganntsByProcess = (
        await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: existSyokouteset.seihin_code,
            koutei_code: existSyokouteset.koutei_code
          })
          .fetch()
      ).toJSON();

      const hash = ganntsByProcess.reduce((hs, item) => {
        hs[
          `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`
        ] = item;
        return hs;
      }, {});

      const setGannts = (
        await SmSyokouteiSetGannt.query()
          .whereRaw(
            `
                seihin_code = ? and koutei_code = ? and syo_koutei_code IN (
                    ${[
              `'${existSyokouteset.syo_koutei_code}'`,
              ...subProcceses.map(item => `'${item.syo_koutei_code}'`)
            ].join(",")}
                )
                `,
            [existSyokouteset.seihin_code, existSyokouteset.koutei_code]
          )
          .fetch()
      ).toJSON();

      const setGanntsBySubProcesses = setGannts.reduce((hs, item) => {
        hs[item.syo_koutei_code] = hs[item.syo_koutei_code] || [];
        hs[item.syo_koutei_code].push(item);
        return hs;
      }, {});

      let newListSetGannts = [];
      let prevSubProcceses = null;
      for (const subProcess of subProcceses) {
        if (
          subProcess.std_proc_sokudo &&
          subProcess.std_proc_syohin_su != undefined &&
          subProcess.std_proc_syohin_su !== ""
        ) {
          prevSubProcceses =
            prevSubProcceses ||
            (
              await SmSyokouteiSet.query()
                .whereRaw(
                  `
                        seihin_code = ? and koutei_code = ? and syo_koutei_junjo < ?
                        `,
                  [
                    subProcess.seihin_code,
                    subProcess.koutei_code,
                    subProcess.syo_koutei_junjo
                  ]
                )
                .orderBy("syo_koutei_junjo", "desc")
                .limit(1)
                .fetch()
            ).toJSON()[0];

          if (
            prevSubProcceses.seihin_code === existSyokouteset.seihin_code &&
            prevSubProcceses.koutei_code === existSyokouteset.koutei_code &&
            prevSubProcceses.syo_koutei_code ===
            existSyokouteset.syo_koutei_code
          ) {
            prevSubProcceses = {
              ...prevSubProcceses,
              ...lastSyokouteisetGanntList
            };
          }

          newListSetGannts.forEach(item => {
            const k = `${item.seihin_code}-${item.koutei_code}-${item.syo_koutei_code}-${item.main_sub}`;
            hash[k] = {
              ...hash[k],
              ...item
            };
          });

          const _newListSetGannts = Util.generateNextSetGannt(
            subProcess,
            Object.keys(hash).map(k => hash[k]),
            ganntsBySubProcesses[subProcess.syo_koutei_code] || [],
            (setGanntsBySubProcesses[subProcess.syo_koutei_code] || []).reduce(
              (hs, item) => {
                hs[item.main_sub] = item;
                return hs;
              },
              {}
            ),
            Math.ceil(
              +subProcess.std_proc_syohin_su / +subProcess.std_proc_sokudo
            ),
            prevSubProcceses
          );
          if (!_newListSetGannts) {
            let responseData = {
              success: false,
              statusCode: 501,
              error: "Got some error, prev set gannt not exist"
            };
            return response.json(responseData);
          }
          newListSetGannts = [...newListSetGannts, ..._newListSetGannts];

          prevSubProcceses = subProcess;
        }
      }

      const trx2 = await Database.beginTransaction();
      let lastSeq = (await SmSyokouteiSetGannt.query().getMax("seq")) || 0;
      for (const gannt of newListSetGannts) {
        const exist = (
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .limit(1)
            .fetch()
        ).toJSON();
        if (exist[0]) {
          await SmSyokouteiSetGannt.query()
            .where({
              seihin_code: gannt.seihin_code,
              koutei_code: gannt.koutei_code,
              syo_koutei_code: gannt.syo_koutei_code,
              main_sub: gannt.main_sub
            })
            .update(gannt, trx2);
        } else {
          gannt.seq = lastSeq + 1;
          const newd = (await SmSyokouteiSetGannt.create(gannt, trx2)).toJSON();
          lastSeq = newd.seq;
        }
      }
      trx2.commit();

      Redis.keys("SmSyokouteiSetGannt_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "update set gannts success",
        data: null
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a SmSyokouteiSetGannt with id.
   * DELETE SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-set-gannt/delete:
   *   post:
   *     description: Api delete SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntDelete
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .delete();
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SmSyokouteiSetGanntController;
