'use strict'
const SmKouteiMst = use('App/Models/SmKouteiMst')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Redis = use('Redis')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmKouteiMsts
 */
class SmKouteiMstController {
  /**
   * Show a list of all SmKouteiMsts.
   * GET SmKouteiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-koutei-mst/list:
   *   get:
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Tạo sản phẩm
   *     parameters:
   *       - name: kotei_name
   *         description: kotei_name
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmKouteiMst.query()
      if(!isEmpty(params.kotei_name)){
        query.where('kotei_name', 'LIKE', '%'+params.kotei_name+'%')
      }
      if(!isEmpty(params.koutei_code)){
        query.where('koutei_code', 'LIKE', '%'+params.koutei_code+'%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmKouteiMst.
   * GET SmKouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-koutei-mst/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Sample API
   *     parameters:
   *       - name: koutei_code
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        koutei_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmKouteiMst
        .query()
        .where({'koutei_code': data.koutei_code})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmKouteiMst.
   * POST SmKouteiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-koutei-mst/create:
   *   post:
   *     description: Api create SmKouteiMst
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmKouteiMstCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        koutei_code: 'string|max:8',
        kotei_name: 'required|string|max:128',
        biko: 'string|max:512'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      if(isEmpty(data.koutei_code)){
        data.koutei_code = Util.getCode();
      }
      //check code in db
      const check = await SmKouteiMst.query().where({
        'koutei_code': data.koutei_code
      }).fetch()
      if(!isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'koutei_code exist',
          statusCode: 501,
        })
      }

      let res = await SmKouteiMst.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmKouteiMst details.
   * PUT or PATCH SmKouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-koutei-mst/update:
   *   post:
   *     description: Api update SmKouteiMst
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmKouteiMstUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        koutei_code: 'required|string|max:8',
        kotei_name: 'string|max:128',
        biko: 'string|max:512'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmKouteiMst.query().where({
        'koutei_code': data.koutei_code
      }).update(data)
      const res = await SmKouteiMst.query().where({
        'koutei_code': data.koutei_code
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }

      Redis.keys("SmKouteiMst_*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })

      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmKouteiMst with id.
   * DELETE SmKouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-koutei-mst/delete:
   *   post:
   *     description: Api delete SmKouteiMst
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiMstDelete
   *          schema:
   *            $ref: '#/definitions/SmKouteiMstDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        koutei_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmKouteiMst
        .query()
        .where({
          'koutei_code': data.koutei_code
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-koutei-mst/update-sub-process:
   *   post:
   *     description: Api update list SmKouteiMst
   *     security:
   *     tags:
   *       - SmKouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiMstUpdateSetCreate
   *          schema:
   *            $ref: '#/definitions/SmKouteiMstUpdateSetCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async updateSubProcess ({ request, response }) {
    try {
      let rules = {
        koutei_code: 'string|max:8',
        syo_koutei_set: 'array',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      //check code in db
      const check = await SmKouteiMst.query().where({
        'koutei_code': data.koutei_code
      }).fetch()
      if(isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'koutei_code not exist',
          statusCode: 501,
        })
      }

      if(!isEmpty(data.syo_koutei_set)){
        let arrSyoKouteiSet = data.syo_koutei_set
        for (let x in arrSyoKouteiSet) {
          let check = await SmSyokouteiSet.query().where({
            seihin_code: arrSyoKouteiSet[x].seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: arrSyoKouteiSet[x].syo_koutei_code
          }).fetch()

          if(isEmpty(check.rows)){
            await SmSyokouteiSet.create({...arrSyoKouteiSet[x], koutei_code: data.koutei_code})
          }
        }
      }

      let responseData = {
        success: true,
        message: 'create success',
      }

      // Redis.keys("SmKouteiMst_*", async function(err, rows) {
      //   for (var i in rows){
      //     await Redis.del(rows[i])
      //   }
      // })

      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmKouteiMstController
