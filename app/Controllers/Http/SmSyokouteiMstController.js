'use strict'
const SmSyokouteiMst = use('App/Models/SmSyokouteiMst')
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet')

const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSyokouteiMsts
 */
class SmSyokouteiMstController {
  /**
   * Show a list of all SmSyokouteiMsts.
   * GET SmSyokouteiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-mst/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API list SmSyokouteiMst
   *     parameters:
   *       - name: syo_koutei_code
   *         description: syo_koutei_code
   *         in: query
   *         type: string
   *       - name: syo_kotei_name
   *         description: syo_kotei_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmSyokouteiMst.query()
      if(!isEmpty(params.syo_koutei_code)){
        query.where('syo_koutei_code', 'LIKE', '%'+params.syo_koutei_code+'%')
      }
      if(!isEmpty(params.syo_kotei_name)){
        query.where('syo_kotei_name', 'LIKE', '%'+params.syo_kotei_name+'%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmSyokouteiMst.
   * GET SmSyokouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-mst/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API
   *     parameters:
   *       - name: syo_koutei_code
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        syo_koutei_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmSyokouteiMst
        .query()
        .where({'syo_koutei_code': data.syo_koutei_code})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmSyokouteiMst.
   * POST SmSyokouteiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-mst/create:
   *   post:
   *     description: Api create SmSyokouteiMst
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiMstCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        syo_koutei_code: 'string|max:8',
        syo_kotei_name: 'required|string|max:128',
        biko: 'string|max:512'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      if(isEmpty(data.syo_koutei_code)){
        data.syo_koutei_code = Util.getCode();
      }
      //check code in db
      const check = await SmSyokouteiMst.query().where({
        'syo_koutei_code': data.syo_koutei_code
      }).fetch()
      if(!isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'syo_koutei_code exist',
          statusCode: 501,
        })
      }

      let res = await SmSyokouteiMst.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmSyokouteiMst details.
   * PUT or PATCH SmSyokouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-mst/update:
   *   post:
   *     description: Api update SmSyokouteiMst
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiMstUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        syo_koutei_code: 'required|string:max:8',
        syo_kotei_name: 'string|max:128',
        biko: 'string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSyokouteiMst.query().where({
        'syo_koutei_code': data.syo_koutei_code
      }).update(data)
      const res = await SmSyokouteiMst.query().where({
        'syo_koutei_code': data.syo_koutei_code
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmSyokouteiMst with id.
   * DELETE SmSyokouteiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-mst/delete:
   *   post:
   *     description: Api delete SmSyokouteiMst
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiMstDelete
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiMstDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        syo_koutei_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmSyokouteiMst
        .query()
        .where({
          'syo_koutei_code': data.syo_koutei_code
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-syokoutei-mst/update-sub-process:
   *   post:
   *     description: Api update list SmSyokouteiSet
   *     security:
   *     tags:
   *       - SmSyokouteiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiMstUpdateSetCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async updateSubProcess ({ request, response }) {
    try {
      let rules = {
        syo_koutei_code: 'string|max:8',
        syo_koutei_set: 'array',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      //check code in db
      const check = await SmSyokouteiMst.query().where({
        'syo_koutei_code': data.syo_koutei_code
      }).fetch()
      if(isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'syo_koutei_code not exist',
          statusCode: 501,
        })
      }

      if(!isEmpty(data.syo_koutei_set)){
        let arrSyoKouteiSet = data.syo_koutei_set
        for (let x in arrSyoKouteiSet) {
          let check = await SmSyokouteiSet.query().where({
            seihin_code: arrSyoKouteiSet[x].seihin_code,
            koutei_code: arrSyoKouteiSet[x].koutei_code,
            syo_koutei_code: data.syo_koutei_code
          }).fetch()

          if(isEmpty(check.rows)){
            await SmSyokouteiSet.create({...arrSyoKouteiSet[x], syo_koutei_code: data.syo_koutei_code})
          }
        }
      }

      let responseData = {
        success: true,
        message: 'create success',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmSyokouteiMstController
