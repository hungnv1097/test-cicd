"use strict";

const SdSyokouteiKohoDenpyo = use('App/Models/SdSyokouteiKohoDenpyo');
const { validateAll } = use("Validator");

class SdSyokouteiKohoDenpyoController {
    async update({ request, response }) {
        try {
            let rules = {
                koteikeikaku_no: 'string|max:20',
                seihin_code: 'string|max:20',
                koutei_code: 'string|max:8',
                syo_koutei_syurui_seq: 'integer',
                syo_koutei_koho_seq: 'integer'
            }
            const data = request.only(Object.keys(rules));

            await SdSyokouteiKohoDenpyo.query().where({
                koteikeikaku_no: data.koteikeikaku_no,
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                saiyo_flag: true
            }).update({
                saiyo_flag: false
            });

            await SdSyokouteiKohoDenpyo.query().where({
                koteikeikaku_no: data.koteikeikaku_no,
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
                syo_koutei_koho_seq: data.syo_koutei_koho_seq
            }).update({
                saiyo_flag: true
            });

            let res = await SdSyokouteiKohoDenpyo.query().where({
                koteikeikaku_no: data.koteikeikaku_no,
                seihin_code: data.seihin_code,
                koutei_code: data.koutei_code,
                syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
            }).fetch();

            let responseData = {
                data: res.rows[0],
                success: true,
                message: 'Updated success',
            }

            return response.json(responseData)

        } catch(error) {
            return response.status(400).send({
                success: false,
                statusCode: 400,
                error: `エラーを発生しました： ${error}`
            });
        }
    }

    async create({ request, response }) {
        try {
          const data = request.only(Object.keys(SdSyokouteiKohoDenpyo.rules));
          const validation = await validateAll(data, SdSyokouteiKohoDenpyo.rules);
          if (validation.fails()) {
            let responseData = {
              success: false,
              message: validation.messages()[0].message,
              statusCode: 501,
              errors: validation.messages(),
            };
            return response.json(responseData);
          }
          await SdSyokouteiKohoDenpyo.query().insert(data);
          let res = await SdSyokouteiKohoDenpyo.query()
            .where({
              koteikeikaku_no: data.koteikeikaku_no,
              seihin_code: data.seihin_code,
              koutei_code: data.koutei_code,
              syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
            })
            .first();
          const responseData = {
            success: true,
            message: "create success",
            data: res,
          };
          return response.json(responseData);
        } catch (error) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `エラーを発生しました： ${error}`,
          });
        }
      }
    
}

module.exports = SdSyokouteiKohoDenpyoController;
