'use strict'
const SmSouchiMst = use('App/Models/SmSouchiMst')
const SmSouchiWorkLog = use('App/Models/SmSouchiWorkLog')
const SmSouchiMainGannt = use('App/Models/SmSouchiMainGannt')

const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Database = use('Database')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSouchiMsts
 */
class SmSouchiMstController {
  /**
   * Show a list of all SmSouchiMsts.
   * GET SmSouchiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-souchi-mst/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Sample API list SmSouchiMst
   *     parameters:
   *       - name: souchi_code
   *         description: souchi_code
   *         in: query
   *         type: string
   *       - name: souchi_name
   *         description: souchi_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmSouchiMst.query()
      query.orderBy('created_at', 'desc')
      if(!isEmpty(params.souchi_code)){
        query.where('souchi_code', 'LIKE', '%'+params.souchi_code+'%')
      }
      if(!isEmpty(params.souchi_name)){
        query.where('souchi_name', 'LIKE', '%'+params.souchi_name+'%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-souchi-mst/list-main:
   *   get:
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Get list device with gant sum
   *     parameters:
   *       - name: start_datetime
   *         description: Start time
   *         in: query
   *         type: string
   *       - name: end_datetime
   *         description: End Time
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiWorkLogResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async listMain ({ request, response, view }) {
    try {
      let params = request.all()
      const query = SmSouchiWorkLog.query();
      if (!isEmpty(params.start_datetime)) {
        query.where('start_datetime', '>=', params.start_datetime)
      }

      if (!isEmpty(params.end_datetime)) {
        query.where('nouki_datetime', '<=', params.end_datetime)
      }
      let smSouchiWorkLogInstances = await query.select('souchi_code', 'souchi_name', Database.raw('sum(daisu*(EXTRACT(EPOCH from proc_to) - EXTRACT(EPOCH from proc_from)))/sum(daisu*86400) as work_ration'))
        .count('souchi_code as total_souchi')
        .min('start_datetime as min_start_datetime')
        .max('nouki_datetime as max_nouki_datetime')
        .sum('mae_setup_time as sum_mae_setup_time')
        .sum('std_setup_time as sum_std_setup_time')
        .sum('ato_setup_time as sum_ato_setup_time')
        .groupBy('souchi_code', 'souchi_name');

      let res = {
        success: true,
        data: smSouchiWorkLogInstances
      };
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-souchi-mst/list-gannt:
   *   get:
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Get list gannt of device
   *     parameters:
   *       - name: souchi_code
   *         description: Code device
   *         in: query
   *         type: string
   *       - name: start_datetime
   *         description: Start time
   *         in: query
   *         type: string
   *       - name: end_datetime
   *         description: End Time
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiMainGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async listGannt ({ request, response, view }) {
    let params = request.all()
    try {
      let rules = {
        souchi_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      const query = SmSouchiMainGannt.query().where({'souchi_code': data.souchi_code})
      if (!isEmpty(params.start_datetime)) {
        query.where('start_datetime', '>=', params.start_datetime)
      }

      if (!isEmpty(params.end_datetime)) {
        query.where('nouki_datetime', '<=', params.end_datetime)
      }
      let smSouchiMainGanntInstances = await query.fetch();

      let res = {
        success: true,
        data: smSouchiMainGanntInstances
      };
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmSouchiMst.
   * GET SmSouchiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-souchi-mst/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Sample API
   *     parameters:
   *       - name: souchi_code
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        souchi_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmSouchiMst
        .query()
        .where({'souchi_code': data.souchi_code})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmSouchiMst.
   * POST SmSouchiMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-souchi-mst/create:
   *   post:
   *     description: Api create SmSouchiMst
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSouchiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSouchiMstCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        souchi_code: 'string|max:8',
        souchi_name: 'required|string|max:128',
        daisu: 'integer',
        biko: 'string|max:512'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      if(isEmpty(data.souchi_code)){
        data.souchi_code = Util.getCode();
      }
      //check code in db
      const check = await SmSouchiMst.query().where({
        'souchi_code': data.souchi_code
      }).fetch()
      if(!isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'souchi_code exist',
          statusCode: 501,
        })
      }

      let res = await SmSouchiMst.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmSouchiMst details.
   * PUT or PATCH SmSouchiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-souchi-mst/update:
   *   post:
   *     description: Api update SmSouchiMst
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSouchiMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSouchiMstUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSouchiMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        souchi_code: 'required|string:max:8',
        souchi_name: 'string|max:128',
        daisu: 'integer',
        biko: 'string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSouchiMst.query().where({
        'souchi_code': data.souchi_code
      }).update(data)
      const res = await SmSouchiMst.query().where({
        'souchi_code': data.souchi_code
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmSouchiMst with id.
   * DELETE SmSouchiMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-souchi-mst/delete:
   *   post:
   *     description: Api delete SmSouchiMst
   *     security:
   *     tags:
   *       - SmSouchiMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSouchiMstDelete
   *          schema:
   *            $ref: '#/definitions/SmSouchiMstDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        souchi_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmSouchiMst
        .query()
        .where({
          'souchi_code': data.souchi_code
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmSouchiMstController
