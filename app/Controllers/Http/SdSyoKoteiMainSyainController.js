'use strict'

const Util = use('App/helpers/Util')
const SdSyoKoteiMainSyain = use('App/Models/SdSyoKoteiMainSyain')

const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')
const Redis = use('Redis')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SdSyoKoteiMainSyains
 */
class SdSyoKoteiMainSyainController {
  /**
   * Show a list of all SdSyoKoteiMainSyains.
   * GET SdSyoKoteiMainSyains
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-syain/list:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainSyain
   *     summary: Sample API list SdSyoKoteiMainSyain
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: koteikeikaku_no
   *         in: query
   *         type: string
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainSyainResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SdSyoKoteiMainSyain.query()
      if (!isEmpty(params.koteikeikaku_no)) {
        query.where('koteikeikaku_no', 'LIKE', '%' + params.koteikeikaku_no + '%')
      }
      if (!isEmpty(params.seihin_code)) {
        query.where('seihin_code', 'LIKE', '%' + params.seihin_code + '%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Create/save a new SdSyoKoteiMainSyain.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /sd-syo-kotei-main-syain/create:
   *   post:
   *     description: Api create SdSyoKoteiMainSyain
   *     security:
   *     tags:
   *       - SdSyoKoteiMainSyain
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainSyainCreate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainSyainCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainSyainResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      const data = request.only(Object.keys(SdSyoKoteiMainSyain.rules))
      const validation = await validateAll(data, SdSyoKoteiMainSyain.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiMainSyain.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SdSyoKoteiMainSyain.
   * GET SdSyoKoteiMainSyains/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-syain/detail:
   *   get:
   *     security:
   *     tags:
   *       - SdSyoKoteiMainSyain
   *     summary: Sample API
   *     parameters:
   *       - name: seq
   *         description: seq
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainSyainResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response}) {
    try {
      let rules = {
        seq: 'integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SdSyoKoteiMainSyain
        .query()
        .where({seq: data.seq})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Update SdSyoKoteiMainSyain details.
   * PUT or PATCH SdSyoKoteiMainSyains/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-syain/update:
   *   post:
   *     description: Api update SdSyoKoteiMainSyain
   *     security:
   *     tags:
   *       - SdSyoKoteiMainSyain
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainSyainUpdate
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainSyainUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SdSyoKoteiMainSyainResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      const data = request.only(Object.keys(SdSyoKoteiMainSyain.rulesUpdate))
      const validation = await validateAll(data, SdSyoKoteiMainSyain.rulesUpdate)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SdSyoKoteiMainSyain.query().where({
        'seq': data.seq,
      }).update(data)
      const res = await SdSyoKoteiMainSyain.query().where({
        'seq': data.seq
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SdSyoKoteiMainSyain with id.
   * DELETE SdSyoKoteiMainSyains/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sd-syo-kotei-main-syain/delete:
   *   post:
   *     description: Api delete SdSyoKoteiMainSyain
   *     security:
   *     tags:
   *       - SdSyoKoteiMainSyain
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SdSyoKoteiMainSyainDelete
   *          schema:
   *            $ref: '#/definitions/SdSyoKoteiMainSyainDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      let rules = {
        seq: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SdSyoKoteiMainSyain
        .query()
        .where({
          'seq': data.seq
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SdSyoKoteiMainSyainController
