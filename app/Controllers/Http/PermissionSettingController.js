'use strict'


const Database = use('Database')
const Util = use('App/helpers/Util')
const Login = use('App/Models/Login')
const SsKosuKengen = use('App/Models/SsKosuKengen')
const SmHan = use('App/Models/SmHan')

const { validateAll } = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

/**
 * Resourceful controller for interacting with SsPermissionSetting
 */
class PermissionSettingController {
  /**
   * Show a list of all SsPermissionSetting.
   * GET SsPermissionSetting
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /permission-setting/list:
   *   get:
   *     security:
   *     tags:
   *       - SsPermissionSetting
   *     summary: Sample API list SsPermissionSetting
   *     parameters:
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsPermissionSettingResponse'
   *       401:
   *         description: Error response
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const data = await Database.table('login')
        .leftJoin('s_s_kosu_kengen', 'login.user_id', 's_s_kosu_kengen.user_id')
        .leftJoin('s_m_han', 'login.han_id', 's_m_han.id')
        .select([
          'login.user_id',
          'login.mail',
          'login.syain_id',
          'login.role_id',
          'login.password',
          'login.main_class',
          'login.han_id',
          'login.syain_name',
          's_s_kosu_kengen.nafuda_riyo_settei',
          's_s_kosu_kengen.hitobetu_riyo_settei',
          's_s_kosu_kengen.text_mode_settei',
          {'s_m_han.id': 'login.han_id'},
          's_m_han.han_name'
        ])
        .paginate(page, perPage);

      let res = {
        success: true,
        pagination: {
          perPage: data.perPage,
          page: data.page,
          lastPage: data.lastPage,
          total: parseInt(data.total)
        },
        data: data.data
      }

      return response.json(res)
    } catch (error) {

      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  async update({ params, request, response }) {
    try {
      const permissions = request.body;

      const start = async () => {
        await asyncForEach(permissions, async (permission) => {
          const userInfo = await Login.query().where('user_id', permission.user_id).first();

          if (!userInfo) {
            return response.status(404).send({
              success: false,
              statusCode: 404,
              error: `User cannot found.`
            })
          }

          const systemSetting = await SsKosuKengen.query().where('user_id', permission.user_id).first();

          if (!systemSetting) {
            await SsKosuKengen.query().insert({
              user_id: permission.user_id,
              nafuda_riyo_settei: permission.nafuda_riyo_settei && permission.nafuda_riyo_settei!= '0' ? '1' : '0',
              hitobetu_riyo_settei: permission.hitobetu_riyo_settei && permission.hitobetu_riyo_settei!= '0' ? '1' : '0',
              text_mode_settei: permission.text_mode_settei && permission.text_mode_settei!= '0' ? '1' : '0',
              han_id: permission['s_m_han.id']
            })
          } else {
            await SsKosuKengen.query().where({
              user_id: permission.user_id
            }).update({
              nafuda_riyo_settei: permission.nafuda_riyo_settei && permission.nafuda_riyo_settei!= '0' ? '1' : '0',
              hitobetu_riyo_settei: permission.hitobetu_riyo_settei && permission.hitobetu_riyo_settei!= '0' ? '1' : '0',
              text_mode_settei: permission.text_mode_settei && permission.text_mode_settei!= '0' ? '1' : '0',
              han_id: permission['s_m_han.id']
            });
          }

          await Login.query().where({
            user_id: permission.user_id
          }).update({
            password: permission.password,
            han_id: permission['s_m_han.id']
          });
        })

        let responseData = {
          success: true,
          message: 'データを変更しました'
        }

        return response.json(responseData);
      }

      await start();
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  async delete({ params, request, response }) {
    try {
      const permissions = request.body;

      await Promise.all(
        permissions.map(permission => SsKosuKengen.query().where({
          user_id: permission.user_id
        }).delete())
      );

      await Promise.all(
        permissions.map(permission => Login.query().where({
          user_id: permission.user_id
        }).delete())
      );

      let responseData = {
        success: true,
        message: '削除しました。',
      }

      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = PermissionSettingController
