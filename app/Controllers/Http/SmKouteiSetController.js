"use strict";
const Database = use("Database");
const Util = use('App/helpers/Util')

const SmSeihinMst = use("App/Models/SmSeihinMst");

const SmKouteiMst = use("App/Models/SmKouteiMst");
const SmKouteiSet = use("App/Models/SmKouteiSet");

const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");
const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");
const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");
const SdSyoKoteiMainGanntJ = use("App/Models/SdSyoKoteiMainGanntJ");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const SdSyoKoteiMainSyain = use("App/Models/SdSyoKoteiMainSyain");
const StSeisansiji = use("App/Models/StSeisansiji");

const SdSyoKoteiDenpyo = use("App/Models/SdSyoKoteiDenpyo");

const SmBefaftGroupSyosai = use("App/Models/SmBefaftGroupSyosai");
const SmKonsaiGroupSyosai = use("App/Models/SmKonsaiGroupSyosai");

const SmSyokouteiSyurui = use("App/Models/SmSyokouteiSyurui");
const SmSyokouteiKoho = use("App/Models/SmSyokouteiKoho");

const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Redis = use("Redis");
const Logger = use("Logger");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmKouteiSets
 */
class SmKouteiSetController {
  /**
   * @swagger
   * /sm-koutei-set/list:
   *   get:
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API list SmKouteiSet
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;
      const query = SmKouteiSet.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      let data = await query.paginate(page, perPage);
      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/create:
   *   post:
   *     description: Api create SmKouteiSet
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiSetCreate
   *          schema:
   *            $ref: '#/definitions/SmKouteiSetCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string|max:8",
        koutei_junjo: "integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const exists = (
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (exists.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Duplicate process code`
        });
      }

      const seihins = (
        await SmSeihinMst.query()
          .where({
            seihin_code: data.seihin_code
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (!seihins.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Seihin code not exist`
        });
      }

      await SmKouteiSet.create(data);

      const res = (
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      let responseData = {
        success: true,
        message: "create success",
        data: {
          ...res[0],
          seihin_name: seihins[0].name,
          "s-m-syokoutei-list": [],
          "s_m-_syokoutei_list": []
        }
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/change-seihin-code:
   *   post:
   */
  async changeSeihinCode({ request, response }) {
    try {
      let rules = {
        new_seihin_code: "required|string|max:8",
        seihin_code: "required|string"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const existSeihinMst = (
        await SmSeihinMst.query()
          .where({
            seihin_code: data.new_seihin_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existSeihinMst) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No master seihin exist`
        });
      }

      const existSeisans = (
        await StSeisansiji.query()
          .where({
            seihin_code: data.new_seihin_code
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (existSeisans.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Duplicate seihin in seisansiji exist`
        });
      }

      const trx = await Database.beginTransaction();
      await SmKouteiSet.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SdSyoKoteiMainGannt.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SdSyoKoteiMainGanntJ.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SmSyokouteiSetGannt.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SdSyoKoteiMainSyain.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SmSyokouteiSyoList.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await SdSyoKoteiDenpyo.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      await StSeisansiji.query()
        .where({
          seihin_code: data.seihin_code
        })
        .update(
          {
            seihin_code: data.new_seihin_code
          },
          trx
        );

      trx.commit();

      Redis.keys("StSeisansiji_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmKouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SdSyoKoteiMainGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainGanntJ_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainSyain_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiDenpyo_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "Change product code success",
        data: existSeihinMst
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/change-code:
   *   post:
   *     description: Api change koute code
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiSetUpdate
   *          schema:
   *            $ref: '#/definitions/SmKouteiSetUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async changeCode({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        new_koutei_code: "required|string|max:8"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const oldKoutes = (
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON();

      if (!oldKoutes.length) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No koutei set exist`
        });
      }

      const existMasterKoute = (
        await SmKouteiMst.query()
          .where({
            koutei_code: data.new_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existMasterKoute) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No master koutei exist`
        });
      }

      const trx = await Database.beginTransaction();
      await SmKouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainGanntJ.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SmSyokouteiSetGannt.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SdSyoKoteiMainSyain.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SmSyokouteiSyoList.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      await SdSyoKoteiDenpyo.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(
          {
            koutei_code: data.new_koutei_code
          },
          trx
        );

      trx.commit();

      Redis.keys("SmKouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SdSyoKoteiMainGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainGanntJ_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainSyain_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiDenpyo_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      const updated = (
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.new_koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: {
          ...updated,
          ...existMasterKoute
        }
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/clone:
   *   post:
   *     description: Api change koute code
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiSetUpdate
   *          schema:
   *            $ref: '#/definitions/SmKouteiSetUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async clone({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        new_koutei_code: "required|string|max:20",
        new_kotei_name: "required|string|max:128"
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const oldKoutei = (
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!oldKoutei) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `No koutei set exist`
        });
      }

      const oldSyokoteiMainGannt = (
        await SdSyoKoteiMainGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokoteiMainGanntJ = (
        await SdSyoKoteiMainGanntJ.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiSetGannt = (
        await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiMainSyain = (
        await SdSyoKoteiMainSyain.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiSyoList = (
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiSet = (
        await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const oldSyokouteiDenpyo = (
        await SdSyoKoteiDenpyo.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .fetch()
      ).toJSON();

      const trx = await Database.beginTransaction();

      await SmKouteiMst.create(
        {
          koutei_code: data.new_koutei_code,
          kotei_name: data.new_kotei_name
        },
        trx
      );

      const newKouteiSet = (
        await SmKouteiSet.create(
          {
            seihin_code: data.seihin_code,
            koutei_code: data.new_koutei_code
          },
          trx
        )
      ).toJSON();

      const newSyokouteiSet_s = (
        await SmSyokouteiSet.createMany(
          oldSyokouteiSet.map(item => {
            delete item.__meta__;
            delete item["s-m-syokoutei-set-gannt-list"];
            delete item["s-m-syokoutei-syo-list"];

            return {
              ...item,
              koutei_code: data.new_koutei_code
            };
          }),
          trx
        )
      ).reduce((hs, item) => {
        const _item = item.toJSON();
        const key = `${_item.seihin_code} - ${data.koutei_code} - ${_item.syo_koutei_code}`;
        hs[key] = {
          seihin_code: _item.seihin_code,
          koutei_code: _item.koutei_code,
          syo_koutei_code: _item.syo_koutei_code,
          syo_koutei_seq: _item.syo_koutei_seq,
          syo_koutei_junjo: _item.syo_koutei_junjo
        };
        return hs;
      }, {});

      await SdSyoKoteiMainGannt.createMany(
        oldSyokoteiMainGannt.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          delete item.__meta__;
          delete item["s-d-syo-kotei-main-syain-list"];

          return {
            ...item,
            ...newSyokouteiSet_s[key]
          };
        }),
        trx
      );

      await SdSyoKoteiMainGanntJ.createMany(
        oldSyokoteiMainGanntJ.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          return {
            ...item,
            ...newSyokouteiSet_s[key]
          };
        }),
        trx
      );

      await SmSyokouteiSetGannt.createMany(
        oldSyokouteiSetGannt.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          return {
            ...item,
            ...newSyokouteiSet_s[key]
          };
        }),
        trx
      );

      await SdSyoKoteiMainSyain.createMany(
        oldSyokouteiMainSyain.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          return {
            ...item,
            ...newSyokouteiSet_s[key]
          };
        }),
        trx
      );

      await SdSyoKoteiDenpyo.createMany(
        oldSyokouteiDenpyo.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          return {
            ...item,
            ...newSyokouteiSet_s[key]
          };
        }),
        trx
      );

      await SmSyokouteiSyoList.createMany(
        oldSyokouteiSyoList.map(item => {
          const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
          const newItem = {
            ...item,
            ...newSyokouteiSet_s[key]
          };
          delete newItem.syo_koutei_junjo;
          return newItem;
        }),
        trx
      );

      trx.commit();

      Redis.keys("SmKouteiMst_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmKouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      Redis.keys("SdSyoKoteiMainGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainGanntJ_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SmSyokouteiSetGannt_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiMainSyain_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      Redis.keys("SdSyoKoteiDenpyo_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "clone success",
        data: newKouteiSet
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/clone-by-seihin:
   *
   */
  async cloneBySeihin({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        new_seihin_code: "required|string|max:20",
        new_seihin_name: "required|string|max:128",
        gannt_color: "string|max:7",
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      const checkSeihinMst = await SmSeihinMst.query().where({
        seihin_code: data.seihin_code
      }).first();

      if(!checkSeihinMst) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Cant find any seihin_code`
        });
      }
      const checkNewSeihinMst = await SmSeihinMst.query().where({
        seihin_code: data.new_seihin_code
      }).first();
      if(checkNewSeihinMst) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Duplicate new seihin_code`
          });
      }


      let createNewSeihinMst = await SmSeihinMst.create({
        seihin_code: data.new_seihin_code,
        seihin_name: data.new_seihin_name,
        gannt_color: data.gannt_color
      });

      let findSeihin = await Database.table('s_t_seisansiji').where({
        seihin_code: data.seihin_code
      });

      for(let i = 0; i < findSeihin.length; i++) {
          let stSeisansiji_seihin = findSeihin[i];
          await StSeisansiji.create({
            // koteikeikaku_no: stSeisansiji_seihin.koteikeikaku_no,
            koteikeikaku_no: Util.randomStr(10),
            seihin_code: data.new_seihin_code,
            start_datetime: stSeisansiji_seihin.start_datetime,
            nouki_datetime: stSeisansiji_seihin.nouki_datetime,
            suryo: stSeisansiji_seihin.suryo,
            lock_flag: stSeisansiji_seihin.lock_flag,
            kado_jikangai_flag: stSeisansiji_seihin.kado_jikangai_flag
          })
        }
  
      let checkKoutei = await Database.table('s_m_koutei_set').where({
        seihin_code: data.seihin_code
      });

      if(checkKoutei.length > 0) {
        for(let i = 0; i < checkKoutei.length; i++) {
          let KouteiSet = checkKoutei[i];
          let createKoutei = await SmKouteiSet.create({
            seihin_code: data.new_seihin_code,
            koutei_code: KouteiSet.koutei_code
          })
        }
      }

      let checkSmSyurui = await Database.table('s_m_syokoutei_syurui').where({
        seihin_code: data.seihin_code
      });

      if(checkSmSyurui.length > 0) {
        for(let i = 0; i < checkSmSyurui.length; i++) {
          let dataSmSyurui = checkSmSyurui[i];
          await SmSyokouteiSyurui.create({
            syo_koutei_syurui_seq: dataSmSyurui.syo_koutei_syurui_seq,
            seihin_code: data.new_seihin_code,
            koutei_code: dataSmSyurui.koutei_code,
            syo_koutei_syurui_name: dataSmSyurui.syo_koutei_syurui_name
          })
        }
      }

      let checkSmKoho = await Database.table('s_m_syokoutei_koho').where({
        seihin_code: data.seihin_code
      });

      if(checkSmKoho.length > 0) {
        for(let i = 0; i < checkSmKoho.length; i++ ) {
          let dataSmKoho = checkSmKoho[i];
            await SmSyokouteiKoho.create({
              seihin_code: data.new_seihin_code,
              koutei_code: dataSmKoho.koutei_code,
              syo_koutei_syurui_seq: dataSmKoho.syo_koutei_syurui_seq,
              syo_koutei_koho_seq: dataSmKoho.syo_koutei_koho_seq,
              syo_koutei_koho_name: dataSmKoho.syo_koutei_koho_name
            })
        }
      }

      let checkSmSyokouteiSet = await Database.table('s_m_syokoutei_set').where({
        seihin_code: data.seihin_code
      });

      // let checkSeisanji = await Database.table('s_t_seisansiji').where({
      //   seihin_code: data.seihin_code
      // });

      if(checkSmSyokouteiSet.length > 0) {
        for(let i = 0; i < checkSmSyokouteiSet.length; i++ ) {
          let dataSmSyoKouteiSet = checkSmSyokouteiSet[i];
          let syokoutei =  await SmSyokouteiSet.create({
              seihin_code: data.new_seihin_code,
              koutei_code: dataSmSyoKouteiSet.koutei_code,
              syo_koutei_code: dataSmSyoKouteiSet.syo_koutei_code,
              syo_koutei_syurui_seq: dataSmSyoKouteiSet.syo_koutei_syurui_seq,
              syo_koutei_koho_seq: dataSmSyoKouteiSet.syo_koutei_koho_seq,
            });

              //   for(let k = 0; k < checkSeisanji.length; k++) {
              //   const sang = await SdSyoKoteiDenpyo.create({
              //     koteikeikaku_no: checkSeisanji[k].koteikeikaku_no,
              //     seihin_code: data.new_seihin_code,
              //     koutei_code: dataSmSyoKouteiSet.koutei_code,
              //     syo_koutei_code: dataSmSyoKouteiSet.syo_koutei_code,
              //     syo_koutei_seq: syokoutei.syo_koutei_seq,
              //     syo_koutei_junjo: syokoutei.syo_koutei_junjo,
              //     tantosya: dataSmSyoKouteiSet.tantosya,
              //     syo_koutei_syurui_seq: dataSmSyoKouteiSet.syo_koutei_syurui_seq,
              //     syo_koutei_koho_seq: dataSmSyoKouteiSet.syo_koutei_koho_seq,
              //   });
              // }
          }
        }

      let existSmSyokouteiSyoList = await Database.table('s_m_syokoutei_syo_list').where({
        seihin_code: data.seihin_code,
      });

      if(existSmSyokouteiSyoList.length > 0) {
        for(let i = 0; i < existSmSyokouteiSyoList.length; i++ ) {
          let dataSmSyokouteiSyoList = existSmSyokouteiSyoList[i];
            await SmSyokouteiSyoList.create({
              seihin_code: data.new_seihin_code,
              koutei_code: dataSmSyokouteiSyoList.koutei_code,
              syo_koutei_code: dataSmSyokouteiSyoList.syo_koutei_code,
              syo_koutei_seq: dataSmSyokouteiSyoList.syo_koutei_seq,
              sagyo_code: dataSmSyokouteiSyoList.sagyo_code,
              souchi_code: dataSmSyokouteiSyoList.souchi_code,
              syo_koutei_koho_seq: dataSmSyokouteiSyoList.syo_koutei_koho_seq,
              syo_koutei_syurui_seq: dataSmSyokouteiSyoList.syo_koutei_syurui_seq
            })
        }
      }

      let existBefaftGroupSyosai = await Database.table('s_m_befaft_group_syosai').where({
        seihin_code: data.seihin_code,
      });

      if(existBefaftGroupSyosai.length > 0) {
        for(let i = 0; i < existBefaftGroupSyosai.length; i++ ) {
          let dataBefaftGroupSyosai = existBefaftGroupSyosai[i];
            await SmBefaftGroupSyosai.create({
            seihin_code: data.new_seihin_code,
            koutei_code: dataBefaftGroupSyosai.koutei_code,
            syo_koutei_code: dataBefaftGroupSyosai.syo_koutei_code,
            befaft_group_id: dataBefaftGroupSyosai.befaft_group_id,
            syo_koutei_koho_seq: dataBefaftGroupSyosai.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: dataBefaftGroupSyosai.syo_koutei_syurui_seq
            })
        }
      }

      let existSmKonsaiGroupSyosai = await Database.table('s_m_konsai_group_syosai').where({
        seihin_code: data.seihin_code,
      });

      if(existSmKonsaiGroupSyosai.length > 0) {
        for(let i = 0; i < existSmKonsaiGroupSyosai.length; i++ ) {
          let dataSmKonsaiGroupSyosai = existSmKonsaiGroupSyosai[i];
            await SmKonsaiGroupSyosai.create({
              seihin_code: data.new_seihin_code,
              koutei_code: dataSmKonsaiGroupSyosai.koutei_code,
              syo_koutei_code: dataSmKonsaiGroupSyosai.syo_koutei_code,
              konsai_group_id: dataSmKonsaiGroupSyosai.konsai_group_id,
              syo_koutei_koho_seq: dataSmKonsaiGroupSyosai.syo_koutei_koho_seq,
              syo_koutei_syurui_seq: dataSmKonsaiGroupSyosai.syo_koutei_syurui_seq
            })
        }
      }

      let existSmSyokouteiSetGannt = await Database.table('s_m_syokoutei_set_gannt').where({
        seihin_code: data.seihin_code,
      });

      let checkSmSyokouteiSets = await Database.table('s_m_syokoutei_set').where({
        seihin_code: data.new_seihin_code
      });


      if(existSmSyokouteiSetGannt.length > 0) {
          for(let i = 0; i < existSmSyokouteiSetGannt.length; i++ ) {
            let dataSmSyokouteiSetGannt = existSmSyokouteiSetGannt[i];
            for(let j = 0; j < checkSmSyokouteiSets.length; j++ ) {
              let datacheckSmSyokouteiSets = checkSmSyokouteiSets[j];
              if((dataSmSyokouteiSetGannt.koutei_code === datacheckSmSyokouteiSets.koutei_code) && (dataSmSyokouteiSetGannt.syo_koutei_code === datacheckSmSyokouteiSets.syo_koutei_code) && (dataSmSyokouteiSetGannt.syo_koutei_syurui_seq === datacheckSmSyokouteiSets.syo_koutei_syurui_seq) && (dataSmSyokouteiSetGannt.syo_koutei_koho_seq === datacheckSmSyokouteiSets.syo_koutei_koho_seq)) {
                await SmSyokouteiSetGannt.create({
                  koutei_code: datacheckSmSyokouteiSets.koutei_code,
                  main_sub: dataSmSyokouteiSetGannt.main_sub,
                  proc_from: dataSmSyokouteiSetGannt.proc_from,
                  proc_to: dataSmSyokouteiSetGannt.proc_to,
                  seihin_code: data.new_seihin_code,
                  syo_koutei_code: datacheckSmSyokouteiSets.syo_koutei_code,
                  syo_koutei_junjo: datacheckSmSyokouteiSets.syo_koutei_junjo,
                  syo_koutei_koho_seq: datacheckSmSyokouteiSets.syo_koutei_koho_seq,
                  syo_koutei_seq: datacheckSmSyokouteiSets.syo_koutei_seq,
                  syo_koutei_syurui_seq: datacheckSmSyokouteiSets.syo_koutei_syurui_seq
                })
              }
            }
        }
      }

      return response.status(200).json({
        success: true,
        message: 'Created success',
        data: existSmSyokouteiSetGannt,
        data1: checkSmSyokouteiSets
      });      
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/update:
   *   post:
   *     description: Api update SmKouteiSet
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiSetUpdate
   *          schema:
   *            $ref: '#/definitions/SmKouteiSetUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKouteiSetResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:8",
        koutei_junjo: "integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      await SmKouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .update(data);
      const res = await SmKouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * @swagger
   * /sm-koutei-set/update-junjo:
   */
  async updateJunjo({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string",
        koutei_junjo: "integer"
      };
      const data = request.body;

      for (const dt of data) {
        const validation = await validateAll(dt, rules);
        if (validation.fails()) {
          let responseData = {
            success: false,
            message: validation.messages()[0].message,
            statusCode: 501,
            errors: validation.messages()
          };
          return response.json(responseData);
        }
      }

      const trx = await Database.beginTransaction();
      await Promise.all(
        data.map(dt =>
          SmKouteiSet.query()
            .where({
              seihin_code: dt.seihin_code,
              koutei_code: dt.koutei_code
            })
            .update(dt, trx)
        )
      );

      trx.commit();

      Redis.keys("SmKouteiSet_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      let responseData = {
        success: true,
        message: "update junjo success"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `An error has occured ${error}`
      });
    }
  }

  /**
   * Delete a SmKouteiSet with id.
   * DELETE SmKouteiSets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-koutei-set/delete:
   *   post:
   *     description: Api delete SmKouteiSet
   *     security:
   *     tags:
   *       - SmKouteiSet
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKouteiSetDelete
   *          schema:
   *            $ref: '#/definitions/SmKouteiSetDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      // let smSyokouteiSets = await SmSyokouteiSet.query()
      //   .where({
      //     seihin_code: data.seihin_code,
      //     koutei_code: data.koutei_code
      //   })
      //   .fetch();

        let delSmSyokouteiSyurui = await SmSyokouteiSyurui.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
          Logger.info("delSmSyokouteiSyurui", delSmSyokouteiSyurui);

        let delSmSyokouteiKoho = await SmSyokouteiKoho.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
          Logger.info("delSmSyokouteiKoho", delSmSyokouteiKoho);

          let delSdSyoKoteiMainGanntJ = await SdSyoKoteiMainGanntJ.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSdSyoKoteiMainGanntJ", delSdSyoKoteiMainGanntJ);

        
        let delSdSyoKoteiMainGannt = await SdSyoKoteiMainGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSdSyoKoteiMainGannt", delSdSyoKoteiMainGannt);

        let delSmSyokouteiSetGannt = await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSmSyokouteiSetGannt", delSmSyokouteiSetGannt);

        let delSmSyokouteiSyoList = await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSmSyokouteiSyoList", delSmSyokouteiSyoList);

        let delSdSyoKoteiDenpyo = await SdSyoKoteiDenpyo.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSdSyoKoteiDenpyo", delSdSyoKoteiDenpyo);

        let delSmBefaftGroupSyosai = await SmBefaftGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSmBefaftGroupSyosai", delSmBefaftGroupSyosai);

        let delSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code
          })
          .delete();
        Logger.info("delSmKonsaiGroupSyosai", delSmKonsaiGroupSyosai);

        let delSmSyokouteiSet = await SmSyokouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .delete();
      Logger.info("delSmSyokouteiSet", delSmSyokouteiSet);

      let delSmKouteiSet = await SmKouteiSet.query()
        .where({
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .delete();
      Logger.info("delSmKouteiSet", delSmKouteiSet);

      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error("error", error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  // async cloneBySeihin({ request, response }) {
  //   try {
  //     let rules = {
  //       seihin_code: "required|string",
  //       new_seihin_code: "required|string|max:20",
  //       new_seihin_name: "required|string|max:128",
  //       koteikeikaku_no:"required|string",
  //       gannt_color: "string|max:7",
  //     };

  //     const data = request.only(Object.keys(rules));
  //     const validation = await validateAll(data, rules);
  //     if (validation.fails()) {
  //       let responseData = {
  //         success: false,
  //         message: validation.messages()[0].message,
  //         statusCode: 501,
  //         errors: validation.messages()
  //       };
  //       return response.json(responseData);
  //     }

  //     const existSeihinMst = (
  //       await SmSeihinMst.query()
  //         .where({
  //           seihin_code: data.new_seihin_code
  //         })
  //         .limit(1)
  //         .fetch()
  //     ).toJSON()[0];

  //     if (existSeihinMst) {
  //       return response.status(400).send({
  //         success: false,
  //         statusCode: 400,
  //         error: `Duplicate master seihin exist`
  //       });
  //     }

  //     const existSeisans = (
  //       await StSeisansiji.query()
  //         .where({
  //           seihin_code: data.new_seihin_code
  //         })
  //         .limit(1)
  //         .fetch()
  //     ).toJSON()[0];

  //     if (existSeisans) {
  //       return response.status(400).send({
  //         success: false,
  //         statusCode: 400,
  //         error: `Duplicate seihin in seisansiji`
  //       });
  //     }

  //     const oldSeisans = (
  //       await StSeisansiji.query()
  //         .where({
  //           seihin_code: data.seihin_code,
  //           koteikeikaku_no: data.koteikeikaku_no
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldKouteiSets = (
  //       await SmKouteiSet.query()
  //         .where({
  //           seihin_code: data.seihin_code
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokoteiMainGannt = (
  //       await SdSyoKoteiMainGannt.query()
  //         .where({
  //           seihin_code: data.seihin_code,
  //           koteikeikaku_no: data.koteikeikaku_no
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokoteiMainGanntJ = (
  //       await SdSyoKoteiMainGanntJ.query()
  //         .where({
  //           seihin_code: data.seihin_code,
  //           koteikeikaku_no: data.koteikeikaku_no
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokouteiSetGannt = (
  //       await SmSyokouteiSetGannt.query()
  //         .where({
  //           seihin_code: data.seihin_code
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokouteiMainSyain = (
  //       await SdSyoKoteiMainSyain.query()
  //         .where({
  //           seihin_code: data.seihin_code,
  //           koteikeikaku_no: data.koteikeikaku_no
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokouteiSyoList = (
  //       await SmSyokouteiSyoList.query()
  //         .where({
  //           seihin_code: data.seihin_code
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokouteiSet = (
  //       await SmSyokouteiSet.query()
  //         .where({
  //           seihin_code: data.seihin_code
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const oldSyokouteiDenpyo = (
  //       await SdSyoKoteiDenpyo.query()
  //         .where({
  //           seihin_code: data.seihin_code,
  //           koteikeikaku_no: data.koteikeikaku_no
  //         })
  //         .fetch()
  //     ).toJSON();

  //     const trx = await Database.beginTransaction();
  //     const newSmSeihinMst = {
  //       seihin_code: data.new_seihin_code,
  //       seihin_name: data.new_seihin_name,
  //     };
  //     if (data.gannt_color) {
  //       newSmSeihinMst.gannt_color = data.gannt_color;
  //     }

  //     await SmSeihinMst.create(
  //       newSmSeihinMst,
  //       trx
  //     );

  //     const newKoteikeikaku_no = Util.randomStr(10);
  //     await StSeisansiji.createMany(
  //       oldSeisans.map(item => {
  //         delete item["s-d-syo-kotei-main-gaint-j-list"];
  //         delete item["s-d-syo-kotei-main-gaint-list"];
  //         delete item["s-d-syo-koutei-denpyo-list"];
  //         delete item.koteikeikaku_seq;
  //         item.koteikeikaku_no = newKoteikeikaku_no;
  //         return {
  //           ...item,
  //           seihin_code: data.new_seihin_code
  //         };
  //       }),
  //       trx
  //     );

  //     await SmKouteiSet.createMany(
  //       oldKouteiSets.map(item => {
  //         delete item.__meta__;
  //         delete item["kotei-name"];
  //         delete item["s-m-syokoutei-list"];
  //         return {
  //           ...item,
  //           seihin_code: data.new_seihin_code
  //         };
  //       }),
  //       trx
  //     );

  //     const newSyokouteiSet_s = (
  //       await SmSyokouteiSet.createMany(
  //         oldSyokouteiSet.map(item => {
  //           delete item.__meta__;
  //           delete item["s-m-syokoutei-set-gannt-list"];
  //           delete item["s-m-syokoutei-syo-list"];

  //           return {
  //             ...item,
  //             seihin_code: data.new_seihin_code
  //           };
  //         }),
  //         trx
  //       )
  //     ).reduce((hs, item) => {
  //       const _item = item.toJSON();
  //       const key = `${data.seihin_code} - ${_item.koutei_code} - ${_item.syo_koutei_code}`;
  //       hs[key] = {
  //         seihin_code: _item.seihin_code,
  //         koutei_code: _item.koutei_code,
  //         syo_koutei_code: _item.syo_koutei_code,
  //         syo_koutei_seq: _item.syo_koutei_seq,
  //         syo_koutei_junjo: _item.syo_koutei_junjo
  //       };
  //       return hs;
  //     }, {});

  //     await SdSyoKoteiMainGannt.createMany(
  //       oldSyokoteiMainGannt.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         delete item.__meta__;
  //         delete item["s-d-syo-kotei-main-syain-list"];
  //         item.koteikeikaku_no = newKoteikeikaku_no;

  //         return {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //       }),
  //       trx
  //     );

  //     await SdSyoKoteiMainGanntJ.createMany(
  //       oldSyokoteiMainGanntJ.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         item.koteikeikaku_no = newKoteikeikaku_no;
  //         return {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //       }),
  //       trx
  //     );

  //     await SmSyokouteiSetGannt.createMany(
  //       oldSyokouteiSetGannt.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         return {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //       }),
  //       trx
  //     );

  //     await SdSyoKoteiMainSyain.createMany(
  //       oldSyokouteiMainSyain.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         item.koteikeikaku_no = newKoteikeikaku_no;
  //         return {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //       }),
  //       trx
  //     );

  //     await SdSyoKoteiDenpyo.createMany(
  //       oldSyokouteiDenpyo.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         item.koteikeikaku_no = newKoteikeikaku_no;
  //         return {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //       }),
  //       trx
  //     );

  //     await SmSyokouteiSyoList.createMany(
  //       oldSyokouteiSyoList.map(item => {
  //         const key = `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`;
  //         const newItem = {
  //           ...item,
  //           ...newSyokouteiSet_s[key]
  //         };
  //         delete newItem.syo_koutei_junjo;
  //         return newItem;
  //       }),
  //       trx
  //     );

  //     trx.commit();

  //     Redis.keys("StSeisansiji_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SmKouteiSet_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SmSyokouteiSet_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });
  //     Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });
  //     Redis.keys("SdSyoKoteiMainGannt_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SdSyoKoteiMainGanntJ_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SmSyokouteiSetGannt_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SdSyoKoteiMainSyain_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     Redis.keys("SdSyoKoteiDenpyo_*", async function(err, rows) {
  //       for (var i in rows) {
  //         await Redis.del(rows[i]);
  //       }
  //     });

  //     let responseData = {
  //       success: true,
  //       message: "clone success",
  //       data: (
  //         await SmSeihinMst.query()
  //           .where({
  //             seihin_code: data.new_seihin_code
  //           })
  //           .limit(1)
  //           .fetch()
  //       ).toJSON()[0]
  //     };
  //     return response.json(responseData);
  //   } catch (error) {
  //     console.error(error + "");
  //     return response.status(400).send({
  //       success: false,
  //       statusCode: 400,
  //       error: `エラーを発生しました： ${error}`
  //     });
  //   }
  // }

}

module.exports = SmKouteiSetController;
