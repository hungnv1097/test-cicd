'use strict'

const Login = use('App/Models/Login')
const SsKosuKengen = use('App/Models/SsKosuKengen')

const { validateAll } = use('Validator')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsPermissionSetting
 */
class PermissionSettingController {
  async update({ params, request, response }) {
    try {
      const updateData = request.body;

      let rules = {
        user_id: 'required|string'
      }
      let rulesUpdate = {
        data_update: 'object'
      }
      const data = request.only(Object.keys(rules))
      const dataUpdate = request.only(Object.keys(rulesUpdate))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      const updateObj = {
        mail: updateData.data_update.mail != 'null' ? updateData.data_update.mail : '',
        syain_id: parseInt(updateData.data_update.syain_id),
        role_id: parseInt(updateData.data_update.role_id),
        main_class: parseInt(updateData.data_update.main_class),
        syain_name: updateData.data_update.syain_name,
        han_id: updateData.data_update.han_id
      }

      if (updateData.data_update.password) {
        updateObj.password = updateData.data_update.password;
      }

      await Login.query().where({
        user_id: updateData.user_id
      }).update(updateObj).then(async (data) => {
        const updaterPermissionObj = {
          han_id: updateData.data_update.han_id
        };

        if (updateData.data_update.nafuda_riyo_settei !== undefined) {
          updaterPermissionObj.nafuda_riyo_settei = updateData.data_update.nafuda_riyo_settei ? '1' : '0';
        }

        if (updateData.data_update.hitobetu_riyo_settei !== undefined) {
          updaterPermissionObj.hitobetu_riyo_settei = updateData.data_update.hitobetu_riyo_settei ? '1' : '0';
        }

        if (updateData.data_update.text_mode_settei !== undefined) {
          updaterPermissionObj.text_mode_settei = updateData.data_update.text_mode_settei ? '1' : '0';
        }

        await SsKosuKengen.query().where({
          user_id: updateData.data_update.user_id
        }).update(updaterPermissionObj);
      });

      let responseData = {
        success: true,
        message: 'データを変更しました'
      }

      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  async create({ params, request, response }) {
    try {
      const user = request.body;

      const userEntity = await Login.query().where('user_id', user.user_id).fetch();

      if (userEntity && userEntity.lenth > 0) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: `Username already exists.`
        })
      }

      await Login.query().insert({
        user_id: user.user_id,
        mail: user.mail,
        password: user.password,
        syain_id: user.syain_id,
        role_id: user.role_id,
        main_class: user.main_class,
        han_id: user.han_id,
        syain_name: user.syain_name
      }).then(async (data) => {
        await SsKosuKengen.query().insert({
          user_id: user.user_id,
          nafuda_riyo_settei: user.nafuda_riyo_settei ? '1' : '0',
          hitobetu_riyo_settei: user.hitobetu_riyo_settei ?  '1' : '0',
          text_mode_settei: user.text_mode_settei ?  '1' : '0',
          han_id: user.han_id
        });
      });

      let responseData = {
        success: true,
        message: 'create success',
      }

      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = PermissionSettingController
