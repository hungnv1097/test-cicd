'use strict'
const _ = require('lodash')
const isEmpty = require('lodash.isempty')
const Syain = use('App/Models/Syain')

class SyainController {
  async list({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = Syain.query()

      if (!isEmpty(params.sort_field) && !isEmpty(params.sort_order)) {
        query.orderBy(params.sort_field, params.sort_order)
      }

      const data = await query.paginate(page, perPage);

      const res = {
        success: true,
        message: "Success",
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.toJSON().data
      }

      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SyainController
