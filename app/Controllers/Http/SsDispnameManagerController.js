'use strict'
const SsDispnameManager = use('App/Models/SsDispnameManager')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsDispnameManagers
 */
class SsDispnameManagerController {
  /**
   * Show a list of all SsDispnameManagers.
   * GET SsDispnameManagers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-dispname-manager/list:
   *   get:
   *     security:
   *     tags:
   *       - SsDispnameManager
   *     summary: Sample API list SsDispnameManager
   *     parameters:
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsDispnameManagerResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response, view}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SsDispnameManager.query()
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SsDispnameManager.
   * GET SsDispnameManagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-dispname-manager/detail:
   *   get:
   *     security:
   *     tags:
   *       - SsDispnameManager
   *     summary: Sample API
   *     parameters:
   *       - name: gamen_code
   *         description: gamen_code
   *         in: query
   *         type: string
   *       - name: komoku_code
   *         description: komoku_code
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsDispnameManagerResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response, view}) {
    try {
      let rules = {
        gamen_code: 'required|string',
        komoku_code: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsDispnameManager
        .query()
        .where({
          'gamen_code': data.gamen_code,
          'komoku_code': data.komoku_code,
        })
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SsDispnameManager.
   * POST SsDispnameManagers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-dispname-manager/create:
   *   post:
   *     description: Api create SsDispnameManager
   *     security:
   *     tags:
   *       - SsDispnameManager
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsDispnameManagerCreate
   *          schema:
   *            $ref: '#/definitions/SsDispnameManagerCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsDispnameManagerResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      const data = request.only(Object.keys(SsDispnameManager.rules))
      const validation = await validateAll(data, SsDispnameManager.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsDispnameManager.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SsDispnameManager details.
   * PUT or PATCH SsDispnameManagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-dispname-manager/update:
   *   post:
   *     description: Api update SsDispnameManager
   *     security:
   *     tags:
   *       - SsDispnameManager
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsDispnameManagerCreate
   *          schema:
   *            $ref: '#/definitions/SsDispnameManagerUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsDispnameManagerResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      const data = request.only(Object.keys(SsDispnameManager.rules))
      const validation = await validateAll(data, SsDispnameManager.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SsDispnameManager.query().where({
        'gamen_code': data.gamen_code,
        'komoku_code': data.komoku_code,
      }).update(data)
      const res = await SsDispnameManager.query().where({
        'gamen_code': data.gamen_code,
        'komoku_code': data.komoku_code
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SsDispnameManager with id.
   * DELETE SsDispnameManagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-dispname-manager/delete:
   *   post:
   *     description: Api delete SsDispnameManager
   *     security:
   *     tags:
   *       - SsDispnameManager
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsDispnameManagerDelete
   *          schema:
   *            $ref: '#/definitions/SsDispnameManagerDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      let rules = {
        gamen_code: 'required|string',
        komoku_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SsDispnameManager
        .query()
        .where({
          'gamen_code': data.gamen_code,
          'komoku_code': data.komoku_code
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SsDispnameManagerController
