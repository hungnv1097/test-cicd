'use strict'
const SmKonsaiGroup = use('App/Models/SmKonsaiGroup')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmKonsaiGroups
 */
class SmKonsaiGroupController {
  /**
   * Show a list of all SmKonsaiGroups.
   * GET SmKonsaiGroups
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-konsai-group/list:
   *   get:
   *     security:
   *     tags:
   *       - SmKonsaiGroup
   *     summary: Sample API list SmKonsaiGroup
   *     parameters:
   *       - name: konsai_group_name
   *         description: konsai_group_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SmKonsaiGroup.query()
      if(!isEmpty(params.konsai_group_name)){
        query.where('konsai_group_name', 'LIKE', '%'+params.konsai_group_name+'%')
      }
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmKonsaiGroup.
   * GET SmKonsaiGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-konsai-group/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmKonsaiGroup
   *     summary: Sample API
   *     parameters:
   *       - name: konsai_group_id
   *         description: page
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        konsai_group_id: 'required|integer'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmKonsaiGroup
        .query()
        .where({'konsai_group_id': data.konsai_group_id})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmKonsaiGroup.
   * POST SmKonsaiGroups
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group/create:
   *   post:
   *     description: Api create SmKonsaiGroup
   *     security:
   *     tags:
   *       - SmKonsaiGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupCreate
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        konsai_group_name: 'required|string|max:64'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let search_konsai_group_name = await SmKonsaiGroup.query().where({konsai_group_name:data.konsai_group_name}).fetch();
      if(!isEmpty(search_konsai_group_name.rows)) {
        return response.status(400).json({
          success: false,
          message: "konsai_group_name already exist",
          statusCode: 409
        });
      }
      let res = await SmKonsaiGroup.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmKonsaiGroup details.
   * PUT or PATCH SmKonsaiGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group/update:
   *   post:
   *     description: Api update SmKonsaiGroup
   *     security:
   *     tags:
   *       - SmKonsaiGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupCreate
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        konsai_group_id: 'required|integer',
        konsai_group_name: 'string|max:64'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmKonsaiGroup.query().where({
        'konsai_group_id': data.konsai_group_id
      }).update(data)
      const res = await SmKonsaiGroup.query().where({
        'konsai_group_id': data.konsai_group_id
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmKonsaiGroup with id.
   * DELETE SmKonsaiGroups/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group/delete:
   *   post:
   *     description: Api delete SmKonsaiGroup
   *     security:
   *     tags:
   *       - SmKonsaiGroup
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupDelete
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        konsai_group_id: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmKonsaiGroup
        .query()
        .where({
          'konsai_group_id': data.konsai_group_id
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmKonsaiGroupController
