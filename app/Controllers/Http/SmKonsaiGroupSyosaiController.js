"use strict";
const SmKonsaiGroupSyosai = use("App/Models/SmKonsaiGroupSyosai");
const SmKonsaiGroup = use("App/Models/SmKonsaiGroup");
const SmKouteiMst = use("App/Models/SmKouteiMst");
const Util = use("App/helpers/Util");
const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmKonsaiGroupSyosais
 */
class SmKonsaiGroupSyosaiController {
  /**
   * Show a list of all SmKonsaiGroupSyosais.
   * GET SmKonsaiGroupSyosais
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-konsai-group-syosai/list:
   *   get:
   *     security:
   *     tags:
   *       - SmKonsaiGroupSyosai
   *     summary: Sample API list SmKonsaiGroupSyosai
   *     parameters:
   *       - name: konsai_group_id
   *         description: konsai_group_id
   *         in: query
   *         type: integer
   *       - name: syo_koutei_code
   *         description: syo_koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmKonsaiGroupSyosai.query().orderBy('created_at', 'desc');
      const konsaiGroupQuery = SmKonsaiGroup.query();

      if (!isEmpty(params.konsai_group_id)) {
        query.where({ konsai_group_id: params.konsai_group_id });
      }
      if (!isEmpty(params.syo_koutei_code)) {
        query.where(
          "syo_koutei_code",
          "LIKE",
          "%" + params.syo_koutei_code + "%"
        );
      }
      let konsaiGroupsId;
      if (!isEmpty(params.konsai_group_name)) {
        konsaiGroupQuery.where(
          "konsai_group_name",
          "ILIKE",
          "%" + params.konsai_group_name + "%"
        );
        const konsaiGroups = (await konsaiGroupQuery.fetch()).toJSON();
        konsaiGroupsId = konsaiGroups.map(
          konsaiGroup => konsaiGroup.konsai_group_id
        );
      }
      if (konsaiGroupsId) {
        if (!konsaiGroupsId.length) {
          return {
            success: true,
            pagination: {
              perPage,
              page,
              lastPage: page - 1,
              total: 0
            },
            data: []
          };
        }
        query.where("konsai_group_id", "IN", konsaiGroupsId);
      }
      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmKonsaiGroupSyosai.
   * GET SmKonsaiGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * Create/save a new SmKonsaiGroupSyosai.
   * POST SmKonsaiGroupSyosais
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group-syosai/create:
   *   post:
   *     description: Api create SmKonsaiGroupSyosai
   *     security:
   *     tags:
   *       - SmKonsaiGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupSyosaiCreate
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupSyosaiCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        konsai_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      // const konsaiGroupSyosai = await SmKonsaiGroupSyosai.query().where({ konsai_group_id: data.konsai_group_id }).fetch();
      // if (isEmpty(konsaiGroupSyosai.rows)) {
      //   return response.json({
      //     success: false,
      //     message: "konsai_group_id not exist",
      //     statusCode: 409
      //   });
      // }

      // let konsaiGroup = await SmKonsaiGroup.query().where({ konsai_group_id: data.konsai_group_id }).fetch();
      // if (isEmpty(konsaiGroup.rows)) {
      //   return response.json({
      //     success: false,
      //     message: "konsai_group_id not exist",
      //     statusCode: 501
      //   });
      // }

      let kouteiMst = await SmKouteiMst.query().where({ koutei_code: data.koutei_code }).fetch();
      if (isEmpty(kouteiMst.rows)) {
        return response.json({
          success: false,
          message: "syo_koutei_code not exist",
          statusCode: 501
        });
      }

      let checkSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query().where(data).fetch();
      if (!isEmpty(checkSmKonsaiGroupSyosai.rows)) {
        return response.json({
          success: false,
          message: "SmKonsaiGroupSyosai exist",
          statusCode: 501
        });
      }

      let res = await SmKonsaiGroupSyosai.create(data);
      let responseData = {
        success: true,
        message: "create success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SmKonsaiGroupSyosai details.
   * PUT or PATCH SmKonsaiGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group-syosai/update:
   *   post:
   *     description: Api update SmKonsaiGroupSyosai
   *     security:
   *     tags:
   *       - SmKonsaiGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupSyosaiCreate
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupSyosaiUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmKonsaiGroupSyosaiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        konsai_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      let rulesUpdate = {
        data_update: "object"
      };
      const data = request.only(Object.keys(rules));
      const dataUpdate = request.only(Object.keys(rulesUpdate));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      //check bảng
      let konsaiGroup = await SmKonsaiGroup.query()
        .where({ konsai_group_id: data.konsai_group_id })
        .fetch();
      if (isEmpty(konsaiGroup.rows)) {
        return response.json({
          success: false,
          message: "konsai_group_id not exist",
          statusCode: 501
        });
      }

      let kouteiMst = await SmKouteiMst.query()
        .where({ koutei_code: data.koutei_code })
        .fetch();
      if (isEmpty(kouteiMst.rows)) {
        return response.json({
          success: false,
          message: "syo_koutei_code not exist",
          statusCode: 501
        });
      }

      let checkSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query()
        .where({ ...data, ...dataUpdate.data_update })
        .fetch();
      if (!isEmpty(checkSmKonsaiGroupSyosai.rows)) {
        return response.json({
          success: false,
          message: "SmKonsaiGroupSyosai exist",
          statusCode: 501
        });
      }

      await SmKonsaiGroupSyosai.query()
        .where(data)
        .update(dataUpdate.data_update);
      const res = await SmKonsaiGroupSyosai.query()
        .where({ ...data, ...dataUpdate.data_update })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a SmKonsaiGroupSyosai with id.
   * DELETE SmKonsaiGroupSyosais/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-konsai-group-syosai/delete:
   *   post:
   *     description: Api delete SmKonsaiGroupSyosai
   *     security:
   *     tags:
   *       - SmKonsaiGroupSyosai
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmKonsaiGroupSyosaiDelete
   *          schema:
   *            $ref: '#/definitions/SmKonsaiGroupSyosaiDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ request, response }) {
    try {
      const datas = request.body;
      let rules = {
        konsai_group_id: "required|integer",
        syo_koutei_code: "required|string",
        seihin_code: "required|string",
        koutei_code: "required|string"
      };
      for(let data of datas) {
        const validation = await validateAll(data, rules);
        if (validation.fails()) {
          let responseData = {
            success: false,
            message: validation.messages()[0].message,
            statusCode: 501,
            errors: validation.messages()
            
          };
          return response.json(responseData);
          
        }
        await SmKonsaiGroupSyosai.query()
        .where({
          konsai_group_id: data.konsai_group_id,
          syo_koutei_code: data.syo_koutei_code,
          seihin_code: data.seihin_code,
          koutei_code: data.koutei_code
        })
        .delete();
      }
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SmKonsaiGroupSyosaiController;
