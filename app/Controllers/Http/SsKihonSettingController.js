'use strict'

const Util = use('App/helpers/Util')
const SsKihonSetting = use('App/Models/SsKihonSetting')

const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SsKihonSettings
 */
class SsKihonSettingController {
  /**
   * Show a list of all SsKihonSettings.
   * GET SsKihonSettings
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /**
   * @swagger
   * /ss-kihon-setting/list:
   *   get:
   *     security:
   *     tags:
   *       - SsKihonSetting
   *     summary: Sample API list SsKihonSetting
   *     parameters:
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKihonSettingResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SsKihonSetting.query()
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Create/save a new SsKihonSetting.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
   * @swagger
   * /ss-kihon-setting/create:
   *   post:
   *     description: Api create SsKihonSetting
   *     security:
   *     tags:
   *       - SsKihonSetting
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKihonSettingCreate
   *          schema:
   *            $ref: '#/definitions/SsKihonSettingCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKihonSettingResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      const data = request.only(Object.keys(SsKihonSetting.rules))
      const validation = await validateAll(data, SsKihonSetting.rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsKihonSetting.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SsKihonSetting.
   * GET SsKihonSettings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /ss-kihon-setting/detail:
   *   get:
   *     security:
   *     tags:
   *       - SsKihonSetting
   *     summary: Sample API
   *     parameters:
   *       - name: default_workset_id
   *         description: default_workset_id
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKihonSettingResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response }) {
    try {
      let rules = {
        default_workset_id: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SsKihonSetting
        .query()
        .where({'default_workset_id': data.default_workset_id})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Update SsKihonSetting details.
   * PUT or PATCH SsKihonSettings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-kihon-setting/update:
   *   post:
   *     description: Api update SsKihonSetting
   *     security:
   *     tags:
   *       - SsKihonSetting
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKihonSettingUpdate
   *          schema:
   *            $ref: '#/definitions/SsKihonSettingUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SsKihonSettingResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      const data = request.only(Object.keys(SsKihonSetting.rulesUpdate))
      const validation = await validateAll(data, SsKihonSetting.rulesUpdate)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SsKihonSetting.query().where({
        'default_workset_id': data.default_workset_id,
      }).update(data)
      const res = await SsKihonSetting.query().where({
        'default_workset_id': data.default_workset_id
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SsKihonSetting with id.
   * DELETE SsKihonSettings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /ss-kihon-setting/delete:
   *   post:
   *     description: Api delete SsKihonSetting
   *     security:
   *     tags:
   *       - SsKihonSetting
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SsKihonSettingDelete
   *          schema:
   *            $ref: '#/definitions/SsKihonSettingDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        default_workset_id: 'required|integer',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SsKihonSetting
        .query()
        .where({
          'default_workset_id': data.default_workset_id
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SsKihonSettingController
