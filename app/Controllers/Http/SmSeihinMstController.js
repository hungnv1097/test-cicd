'use strict'
const Util = use('App/helpers/Util')
const SmSeihinMst = use('App/Models/SmSeihinMst')
const StSeisansiji = use('App/Models/StSeisansiji')

const SmKouteiSet = use('App/Models/SmKouteiSet')

const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");
const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");
const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");
const SdSyoKoteiMainGanntJ = use("App/Models/SdSyoKoteiMainGanntJ");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const SdSyoKoteiDenpyo = use("App/Models/SdSyoKoteiDenpyo");

const SmBefaftGroupSyosai = use("App/Models/SmBefaftGroupSyosai");
const SmKonsaiGroupSyosai = use("App/Models/SmKonsaiGroupSyosai");

const SmSyokouteiSyurui = use("App/Models/SmSyokouteiSyurui");
const SmSyokouteiKoho = use("App/Models/SmSyokouteiKoho");
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

class SmSeihinMstController {
  /**
   * @swagger
   * /sm-seihin-mst/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: seihin_name
   *         description: seihin_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmSeihinMst.query()
      query.orderBy('created_at', 'desc')

      if (!isEmpty(params.seihin_code)) {
        query.where('seihin_code', 'LIKE', '%' + params.seihin_code + '%')
      }
      if (!isEmpty(params.seihin_name)) {
        query.where('seihin_name', 'LIKE', '%' + params.seihin_name + '%')
      }

      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-seihin-mst/list-main:
   *   get:
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_keyword
   *         description: Product key word search (Name or code)
   *         in: query
   *         type: string
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: seihin_name
   *         description: seihin_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstMainResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async listMain({ request, response }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmSeihinMst.query().with('s_m_koutei_list')
      if (!isEmpty(params.seihin_keyword)) {
        query.leftJoin('s_t_seisansiji', 's_m_seihin_mst.seihin_code', 's_t_seisansiji.seihin_code').where(function () {
          this.where('s_m_seihin_mst.seihin_name', 'LIKE', '%' + params.seihin_keyword + '%')
            .orWhere('s_t_seisansiji.seihin_code', 'LIKE', '%' + params.seihin_keyword + '%')
        })
      }
      if (!isEmpty(params.seihin_code)) {
        query.where('seihin_code', 'LIKE', '%' + params.seihin_code + '%')
      }
      if (!isEmpty(params.seihin_name)) {
        query.where('seihin_name', 'LIKE', '%' + params.seihin_name + '%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.toJSON().data
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-seihin-mst/create:
   *   post:
   *     description: Api create syohin
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSeihinMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSeihinMstCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        seihin_code: 'string|max:20',
        seihin_name: 'required|string|max:128',
        def_kado_jikangai_flag: 'boolean',
        biko: 'string|max:512',
        gannt_color: 'string|max:7'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      if (isEmpty(data.seihin_code)) {
        data.seihin_code = Util.getCode();
      }
      //check code in db
      const check = await SmSeihinMst.query().where({
        'seihin_code': data.seihin_code
      }).fetch()
      if (!isEmpty(check.rows)) {
        return response.status(501).json({
          success: false,
          message: 'seihin_code exist',
          statusCode: 501,
        })
      }

      let res = await SmSeihinMst.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-seihin-mst/update:
   *   post:
   *     description: Api update syohin
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSeihinMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSeihinMstUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ request, response }) {
    try {
      let rules = {
        seihin_code: 'required|string|max:20',
        seihin_name: 'string|max:128',
        def_kado_jikangai_flag: 'boolean',
        biko: 'string|max:512',
        gannt_color: 'string|max:7'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSeihinMst.query().where('seihin_code', data.seihin_code).update(data)
      const res = await SmSeihinMst.query().where('seihin_code', data.seihin_code).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-seihin-mst/change-gannt-color:
   *   post:
   *     description: Api update gantt color
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: seihin_code
   *          type: string
   *        - in: body
   *          name: gantt_color
   *          type: string 
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async changeGanntColor({ request, response }) {
    try {
      let rules = {
        seihin_code: 'required|string|max:20',
        gannt_color: 'required|string|max:7'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSeihinMst.query().where('seihin_code', data.seihin_code).update(data)
      const res = await SmSeihinMst.query().where('seihin_code', data.seihin_code).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmSeihinMst.
   * GET SmSeihinMst/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-seihin-mst/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: page
   *         in: query
   *         type: string
   *       - name: koteikeikaku_seq
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSeihinMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response }) {
    try {
      let rules = {
        seihin_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmSeihinMst
        .query()
        .where({ 'seihin_code': data.seihin_code })
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-seihin-mst/delete:
   *   post:
   *     description: Api delete syohin
   *     security:
   *     tags:
   *       - SmSeihinMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSeihinMstDelete
   *          schema:
   *            $ref: '#/definitions/SmSeihinMstDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ request, response }) {
    try {
      let rules = {
        seihin_code: 'required|string|max:20'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSeihinMst
        .query()
        .where('seihin_code', data.seihin_code)
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  async deleteSeihin_mst({ request, response }) {
    try {
      let rules = {
        seihin_code: 'required|string|max:20'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      const existsKoutei = (await SmKouteiSet.query().where({
        seihin_code: data.seihin_code
      }).fetch()).toJSON();

      const existKoteikeikaku = (await StSeisansiji.query().where({
        seihin_code: data.seihin_code
      }).fetch()).toJSON();

      if (existKoteikeikaku.length > 0) {
        await StSeisansiji.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();
      }

        let delSmSyokouteiKoho = await SmSyokouteiKoho.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmSyokouteiSyurui = await SmSyokouteiSyurui.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSdSyoKoteiMainGanntJ = await SdSyoKoteiMainGanntJ.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSdSyoKoteiMainGannt = await SdSyoKoteiMainGannt.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmSyokouteiSetGannt = await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmSyokouteiSyoList = await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSdSyoKoteiDenpyo = await SdSyoKoteiDenpyo.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmBefaftGroupSyosai = await SmBefaftGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmKonsaiGroupSyosai = await SmKonsaiGroupSyosai.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmSyokouteiSet = await SmSyokouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmKouteiSet = await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();

        let delSmSeihinMst = await SmSeihinMst.query().where({
          seihin_code: data.seihin_code,
        }).delete();

      return response.status(200).json({
        success: true,
        message: "削除しました。"
      });

    } catch (error) {
      return response.status(400).json({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

}

module.exports = SmSeihinMstController
