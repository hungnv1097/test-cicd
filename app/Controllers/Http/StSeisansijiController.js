"use strict";
const Util = require("../../helpers/Util");

const StSeisansiji = use("App/Models/StSeisansiji");

const SmSeihinMst = use("App/Models/SmSeihinMst");
const SmKouteiMst = use("App/Models/SmKouteiMst");
const SmSouchiMst = use("App/Models/SmSouchiMst");

const SmKouteiSet = use("App/Models/SmKouteiSet");
const SmSyokouteiSet = use("App/Models/SmSyokouteiSet");

const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");
const SdSyoKoteiMainGannt = use("App/Models/SdSyoKoteiMainGannt");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const SmSyokouteiSyurui = use("App/Models/SmSyokouteiSyurui");
const SmSyokouteiKoho = use("App/Models/SmSyokouteiKoho");
const SsKihonSetting = use("App/Models/SsKihonSetting");
const SsKadojikoku = use("App/Models/SsKadojikoku");
const SdSyokouteiKohoDenpyo = use("App/Models/SdSyokouteiKohoDenpyo");
const Database = use("Database");
const SdSyoKoteiMainGanntJ = use("App/Models/SdSyoKoteiMainGanntJ");

const _ = require("lodash");
const Redis = use("Redis");

const { validateAll } = use("Validator");
const moment = require("moment");
const isEmpty = require("lodash.isempty");
const Constants = require("../../helpers/Constants");
const { convertSecondToDayTime,convertToJSON,diffTwoDateAsHour } = require("../../helpers/Util");
const { TIME_ZONE, } = require("../../helpers/Constants");
const FORMAT_DATE_TIME = Constants.FORMAT_DATE_TIME

class StSeisansijiController {
  /**
   * @swagger
   * /st-seisansiji/list:
   *   get:
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Danh sách chỉ thị sản xuất
   *     parameters:
   *       - name: seihin_keyword
   *         description: Product key word search (Name or code), Example:  pY27288,bG36579,bG36512
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *       - name: start_datetime
   *         description: Start time
   *         in: query
   *         type: string
   *       - name: nouki_datetime
   *         description: End Time
   *         in: query
   *         type: string
   *       - name: sort_field
   *         description: Field want to sort
   *         in: query
   *         type: string
   *       - name: sort_order
   *         description: Order want to sort (desc, asc)
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;
      const seihinCodes = !isEmpty(params.seihin_keyword)
        ? (
          await SmSeihinMst.query()
            .where(function () {
              this.whereIn("seihin_code", params.seihin_keyword.split(","));
            })
            .orWhere(function () {
              this.whereIn("seihin_name", params.seihin_keyword.split(","));
            })
            .fetch()
        ).toJSON()
        : undefined;

      let query = StSeisansiji.query();

      let strQuery = [];
      let queryParams = [];

      if (seihinCodes) {
        if (!seihinCodes.length) {
          return response.json({
            success: true,
            message: "Success",
            pagination: {
              total: 0,
              perPage,
              page,
            },
            data: [],
          });
        }
        strQuery.push(
          `(${seihinCodes
            .map((item) => {
              queryParams.push(item.seihin_code);
              return `seihin_code = ?`;
            })
            .join(" OR ")})`
        );
      }

      if (!isEmpty(params.sort_field) && !isEmpty(params.sort_order)) {
        query.orderBy(params.sort_field, params.sort_order);
      }
      if (!isEmpty(params.start_datetime) && !isEmpty(params.nouki_datetime)) {
        strQuery.push(
          "(start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime >= ? AND nouki_datetime <= ?)"
        );
        queryParams.push(
          params.start_datetime,
          params.start_datetime,
          params.nouki_datetime,
          params.nouki_datetime,
          params.start_datetime,
          params.nouki_datetime
        );
      }

      query.whereRaw(
        strQuery.map((str) => `(${str})`).join(" and "),
        queryParams
      );

      if (params.mode === 'edit') {
        let listSeihinCode = [];

        const listProductMain = await query
          .select('seihin_code')
          .fetch()

        if (listProductMain && listProductMain.rows.length > 0) {
          listProductMain.rows.forEach(product => {
            listSeihinCode.push(product.seihin_code)
          })
        }

        query = SmSeihinMst.query();
        query.where(function () {
          this.whereIn("seihin_code", listSeihinCode)
        })
      }

      let stSeisansijiList = await query
        .with("seihin", (builder) => {
          builder.with("s_m_koutei_list", (builder1) => {
            builder1.with("s_m_syokoutei_syurui_list", (builder2) => {
              builder2.with("s_m_syokoutei_koho_list", (builder3) => {
                let attr = builder3.parentInstance["$attributes"] || {};
                builder3.with("s_m_syokoutei_list", (builder4) => {
                  builder4
                    .where({ koutei_code: attr.koutei_code })
                    .with("syo_koutei");
                }).with('s_d_syokoutei_koho_denpyo')
              });
            });
          });
        })
        .paginate(page, perPage);

      let list = stSeisansijiList.toJSON().data;

      for (let i = 0; i < list.length; i++) {
        for (let j = 0; j < list[i].seihin.s_m_koutei_list.length; j++) {
          let syoKouteiSyurui = await SmSyokouteiSyurui.query().where({
            seihin_code: list[i].seihin.s_m_koutei_list[j].seihin_code,
            koutei_code: list[i].seihin.s_m_koutei_list[j].koutei_code
          }).fetch()

          list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"] = syoKouteiSyurui.rows;

          for (let k = 0; k < list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"].length; k++) {
            let syoKouteiKoho = await SmSyokouteiKoho.query().where({
              seihin_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k].seihin_code,
              koutei_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k].koutei_code,
              syo_koutei_syurui_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k].syo_koutei_syurui_seq
            })
              .fetch();
            // .with('s_d_syokoutei_koho_denpyo')

            list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"] = syoKouteiKoho.rows;

            for (let l = 0; l < list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"].length; l++) {
              let smSyokouteiSet = await SmSyokouteiSet.query().where({
                seihin_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].seihin_code,
                koutei_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].koutei_code,
                syo_koutei_syurui_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_syurui_seq,
                syo_koutei_koho_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_koho_seq
              })
                .orderBy('created_at', 'asc')
                .with("syo_koutei")
                .fetch();

              if (params.mode !== 'edit') {
                let sdSyoKohoDenpyo = await SdSyokouteiKohoDenpyo.query().where({
                  seihin_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].seihin_code,
                  koutei_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].koutei_code,
                  syo_koutei_syurui_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_syurui_seq,
                  syo_koutei_koho_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_koho_seq,
                  koteikeikaku_no: list[i].koteikeikaku_no,
                }).first();

                list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l]["s_d_syokoutei_koho_denpyo"] = sdSyoKohoDenpyo ? sdSyoKohoDenpyo.$attributes : {}
              }


              list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l]["sm-syokoutei-set"] = smSyokouteiSet.rows;
              // if(params.mode !== 'edit') {
              //   let syokouteiKohoDenpyo = await SdSyokouteiKohoDenpyo.query().where({
              //     koteikeikaku_no: list[i].koteikeikaku_no,
              //     seihin_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].seihin_code,
              //     koutei_code: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].koutei_code,
              //     syo_koutei_syurui_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_syurui_seq,
              //     syo_koutei_koho_seq: list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l].syo_koutei_koho_seq
              //   }).first();

              //   // list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l]["sm-syokoutei-set"][l] = {
              //   //   ...list[i].seihin.s_m_koutei_list[j]["s-m-syokoutei-syurui"][k]["s-m-syokoutei-koho"][l]["sm-syokoutei-set"][l]
              //   // } = syokouteiKohoDenpyo ? syokouteiKohoDenpyo.$attributes : {}

              //   if(list[i].seihin.s_m_koutei_list[j]["s_m_syokoutei_syurui_list"][k]["s_m_syokoutei_koho_list"][l]) {
              //     list[i].seihin.s_m_koutei_list[j]["s_m_syokoutei_syurui_list"][k]["s_m_syokoutei_koho_list"][l]['s_d_syokoutei_koho_denpyo'] = syokouteiKohoDenpyo ? syokouteiKohoDenpyo.$attributes : {}
              //   }
              // }
            }
          }
        }
      }

      // list = list.map(item => {
      //   if (item.seihin) {
      //     return {
      //       ...item,
      //       seihin: {
      //         ...item.seihin,
      //         s_m_koutei_list: item.seihin.s_m_koutei_list.map(process => {
      //           return {
      //             ...process,
      //             ["s-m-syokoutei-list"]: process["s-m-syokoutei-list"].map(
      //               subProcess => {
      //                 return {
      //                   ...subProcess,
      //                 };
      //               }
      //             )
      //           };
      //         })
      //       }
      //     };
      //   }
      //   return item;
      // });

      let res = {
        success: true,
        message: "Success",
        pagination: {
          ...stSeisansijiList.pages,
          total: parseInt(stSeisansijiList.pages.total),
        },
        data: list,
      };

      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  // async groupBySyokoutei({ request, response }) {
  //   try {
  //     let params = request.all();
  //     const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
  //     const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

  //     const seihinCodes = !isEmpty(params.seihin_keyword)
  //       ? (
  //         await SmSeihinMst.query()
  //           .where(function () {
  //             this.where(
  //               "seihin_name",
  //               "LIKE",
  //               "%" + params.seihin_keyword + "%"
  //             ).orWhere(
  //               "seihin_code",
  //               "LIKE",
  //               "%" + params.seihin_keyword + "%"
  //             );
  //           })
  //           .fetch()
  //       ).toJSON()
  //       : undefined;

  //     const query = StSeisansiji.query();
  //     let strQuery = [];
  //     let queryParams = [];

  //     if (seihinCodes) {
  //       if (!seihinCodes.length) {
  //         return response.json({
  //           success: true,
  //           message: "Success",
  //           pagination: {
  //             total: 0,
  //             perPage,
  //             page
  //           },
  //           data: []
  //         });
  //       }
  //       strQuery.push(
  //         `(${seihinCodes
  //           .map(item => {
  //             queryParams.push(item.seihin_code);
  //             return `seihin_code = ?`;
  //           })
  //           .join(" OR ")})`
  //       );
  //     }

  //     if (!isEmpty(params.sort_field) && !isEmpty(params.sort_order)) {
  //       query.orderBy(params.sort_field, params.sort_order);
  //     }
  //     if (!isEmpty(params.start_datetime) && !isEmpty(params.nouki_datetime)) {
  //       strQuery.push(
  //         "(start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime >= ? AND nouki_datetime <= ?)"
  //       );
  //       queryParams.push(
  //         params.start_datetime,
  //         params.start_datetime,
  //         params.nouki_datetime,
  //         params.nouki_datetime,
  //         params.start_datetime,
  //         params.nouki_datetime
  //       );
  //     }

  //     query.whereRaw(
  //       strQuery.map(str => `(${str})`).join(" and "),
  //       queryParams
  //     );
  //     let stSeisansijiList = await query.with("seihin", builder => {
  //         builder.with("s_m_koutei_list");
  //       })
  //       .paginate(page, perPage);

  //     let list = stSeisansijiList.toJSON().data;

  //     // list = list.map(item => {
  //     //   if (item.seihin) {
  //     //     return {
  //     //       ...item,
  //     //       seihin: {
  //     //         ...item.seihin,
  //     //         s_m_koutei_list: item.seihin.s_m_koutei_list.map(process => {
  //     //           return {
  //     //             ...process,
  //     //             ["s-m-syokoutei-list"]: process["s-m-syokoutei-list"].map(
  //     //               subProcess => {
  //     //                 return {
  //     //                   ...subProcess,
  //     //                 };
  //     //               }
  //     //             )
  //     //           };
  //     //         })
  //     //       }
  //     //     };
  //     //   }
  //     //   return item;
  //     // });

  //     let res = {
  //       success: true,
  //       message: "Success",
  //       pagination: {
  //         ...stSeisansijiList.pages,
  //         total: parseInt(stSeisansijiList.pages.total)
  //       },
  //       data: list,
  //     };

  //     return response.json(res);
  //   } catch (error) {
  //     console.error(error);
  //     return response.status(400).send({
  //       success: false,
  //       statusCode: 400,
  //       error: `エラーを発生しました： ${error}`
  //     });
  //   }
  // }

  /**
   * @swagger
   * /st-seisansiji/group-by-souchi:
   *   get:
   */

  async updateGroupBySyokoutei({ request, response }) {
    try {
      /**
       * master data/ data default ======
       */
      const typeOfMainGant = Constants.TYPE_OF_MAIN_GANNT;
      const messCanNotMove = Constants.MESS_CAN_NOT_MOVE;
      const messHasNoGant = "Not have gannt";
      const trx = await Database.beginTransaction();
      const body = request.all();
      const { koutei_code, koteikeikaku_update, koteikeikaku } = body;

      /**
         * #4
         */
      const timeSet = {
        proc_to: moment(koteikeikaku.proc_to).hour(),
        proc_from: moment(koteikeikaku.proc_from).hour(),
        range: Util.diffTwoDateAsHour(
          koteikeikaku.proc_from,
          koteikeikaku.proc_to
        ),
      };

      const timeUpdate = {
        proc_to: moment(koteikeikaku_update.proc_to).hour(),
        proc_from: moment(koteikeikaku_update.proc_from).hour(),
        range: Util.diffTwoDateAsHour(
          koteikeikaku_update.proc_from,
          koteikeikaku_update.proc_to
        ),
      };
      /**
       * #3
       */
      if (timeUpdate.range === timeSet.range) {
        const rangerAffterChange = moment
          .duration(
            moment(koteikeikaku_update.proc_to).diff(koteikeikaku.proc_to)
          )
          .asHours();
        if (
          koteikeikaku.syo_koutei_code !== koteikeikaku_update.syo_koutei_code
        ) {
          /**
           *  =======> case 1, case 2
           */
          const smSyokouteiSetCheck = await SmSyokouteiSet.query()
            .where({
              koutei_code,
              seihin_code: koteikeikaku.seihin_code,
              syo_koutei_code: koteikeikaku_update.syo_koutei_code,
              syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
              syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq
            })
            .fetch();
          const smSyokouteiSetChecktoJson = Util.convertToJSON(smSyokouteiSetCheck);
          if (smSyokouteiSetChecktoJson.length) throw messCanNotMove
          const smSyokouteiSet = await SmSyokouteiSet.query()
            .where({
              koutei_code,
              seihin_code: koteikeikaku.seihin_code,
              syo_koutei_code: koteikeikaku_update.syo_koutei_code,
            })
            .fetch();
          const smSyokouteiSettoJson = Util.convertToJSON(smSyokouteiSet);
          if (!smSyokouteiSettoJson.length) {
            /**
             * #2: CTSX ===== has not product
             */
            const dataSmSyokouteiKohoNew = {
              koutei_code,
              seihin_code: koteikeikaku.seihin_code,
              syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
              syo_koutei_koho_name: Constants.DEFAULT_NAME_KOHO,
            };
            await SmSyokouteiKoho.query().insert(dataSmSyokouteiKohoNew, trx);
            let smSyokouteiKohoWasSave = await SmSyokouteiKoho.query().last();
            smSyokouteiKohoWasSave = Util.convertToJSON(smSyokouteiKohoWasSave)
            const smSyokouteiSetNewData = {
              koutei_code,
              seihin_code: koteikeikaku.seihin_code,
              syo_koutei_code: koteikeikaku_update.syo_koutei_code,
              ///// change khi nao biet no la gi
              syo_koutei_seq: 0,

              syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
              syo_koutei_koho_seq: smSyokouteiKohoWasSave.syo_koutei_koho_seq,
            };

            const newSet = await SmSyokouteiSet.create(smSyokouteiSetNewData, trx);
            await SdSyokouteiKohoDenpyo.query()
              .where({
                syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
                koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
                seihin_code: koteikeikaku.seihin_code,
                koutei_code: koutei_code,
              })
              .update({
                saiyo_flag: false,
              },trx);
            await SdSyokouteiKohoDenpyo.query().insert({
              syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
              syo_koutei_koho_seq: smSyokouteiKohoWasSave.syo_koutei_koho_seq,
              seihin_code: koteikeikaku.seihin_code,
              saiyo_flag: true,
              koutei_code,
              koteikeikaku_no: koteikeikaku.koteikeikaku_no
            }, trx);
            await SdSyokouteiKohoDenpyo.query()
              .where({
                syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
                koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
                seihin_code: koteikeikaku.seihin_code,
                koutei_code: koutei_code,
              })
              .update({
                saiyo_flag: false,
              }, trx);
              const findMaintGannt= await SdSyoKoteiMainGannt.query().where({
                koutei_code,
                seihin_code: koteikeikaku.seihin_code,
                syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
                syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
                syo_koutei_code:koteikeikaku.syo_koutei_code,
                koteikeikaku_no: koteikeikaku.koteikeikaku_no,
              }).fetch()
            let  maintGanntJson  = Util.convertToJSON(findMaintGannt)
            let  maintGanntJsonEx  = Util.convertToJSON(findMaintGannt)
              if(maintGanntJsonEx.length){
                await Promise.all(
                  maintGanntJsonEx.map(async (value) => {
                    await SdSyoKoteiMainGannt
                      .create({
                        koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                        seihin_code: value.seihin_code,
                        koutei_code,
                        syo_koutei_code: smSyokouteiSetNewData.syo_koutei_code,
                        syo_koutei_seq: 0,
                        syo_koutei_junjo: Util.convertToJSON(newSet).syo_koutei_junjo,
                        syo_koutei_koho_seq: smSyokouteiSetNewData.syo_koutei_koho_seq,
                        syo_koutei_syurui_seq: smSyokouteiSetNewData.syo_koutei_syurui_seq,
                        proc_to:moment(value.proc_to).format(FORMAT_DATE_TIME),
                        main_sub: value.main_sub,
                        proc_from: moment(value.proc_from).format(FORMAT_DATE_TIME),
                        lock_flag: true,
                      }, trx);
                    }))
              }
              if(maintGanntJson.length)
              {
                maintGanntJson=  maintGanntJson.map(v =>{
                  return{
                    ...v,
                    proc_to:Util.addDateWithHour(v.proc_to,7),
                    proc_from:Util.addDateWithHour(v.proc_from,7),
                  }
                }).sort((a, b) => {
                  return moment(a.proc_from).diff(moment(b.proc_from))
                });
                const fistElement = moment(maintGanntJson[0].proc_from)
                const fistElementAsZero = moment(maintGanntJson[0].proc_from).set({hour:0,minute:0,second:0})
                const range =Util.diffTwoDateAsHour(fistElementAsZero,fistElement)
              await Promise.all(
                maintGanntJson.map(async (value,index) => {
                    const proc_to = Util.addDateWithHour(value.proc_to,range,"sub")
                    const proc_toAsZero =  moment(value.proc_to).set({hour:0,minute:0,second:0})
                    const proc_from =  Util.addDateWithHour(value.proc_from,range,"sub")
                    const proc_fromAsZero = moment(value.proc_from).set({hour:0,minute:0,second:0})
                    const maxId = await Database.raw(`SELECT MAX(seq) FROM s_m_syokoutei_set_gannt`)
                  await SmSyokouteiSetGannt
                    .create({
                      seihin_code: value.seihin_code,
                      koutei_code,
                      syo_koutei_code: smSyokouteiSetNewData.syo_koutei_code,
                      syo_koutei_seq: 0,
                      syo_koutei_junjo: Util.convertToJSON(newSet).syo_koutei_junjo,
                      syo_koutei_koho_seq: smSyokouteiSetNewData.syo_koutei_koho_seq,
                      syo_koutei_syurui_seq: smSyokouteiSetNewData.syo_koutei_syurui_seq,
                      proc_to:Math.abs(Util.diffTwoDateAsHour(proc_toAsZero,proc_to) * 3600),
                      main_sub: value.main_sub,
                      seq:maxId.rows[0].max + 1 + index,
                      proc_from:Math.abs(Util.diffTwoDateAsHour(proc_fromAsZero,proc_from) * 3600),
                    }, trx);
                })
              );}
            // await SdSyoKoteiMainGannt.create({
            //   koteikeikaku_no: koteikeikaku.koteikeikaku_no,
            //   seihin_code: koteikeikaku.seihin_code,
            //   koutei_code,
            //   syo_koutei_code: koteikeikaku_update.syo_koutei_code,
            //   syo_koutei_seq: 0,
            //   syo_koutei_junjo: Util.convertToJSON(newSet).syo_koutei_junjo,
            //   proc_from: moment(koteikeikaku_update.proc_from).format(FORMAT_DATE_TIME),
            //   proc_to: moment(koteikeikaku_update.proc_to).format(FORMAT_DATE_TIME),
            //   syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
            //   syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq
            // }, trx)
          } else {
            /**
             * #1: CTSX ===== has on product
             */
            const firstSet = smSyokouteiSettoJson[0];
   
            let findGanntChart = await SmSyokouteiSetGannt.query()
              .where({
                koutei_code,
                seihin_code: koteikeikaku_update.seihin_code,
                syo_koutei_syurui_seq: koteikeikaku_update.syo_koutei_syurui_seq,
                syo_koutei_koho_seq: koteikeikaku_update.syo_koutei_koho_seq,
              })
              .fetch();
              findGanntChart = findGanntChart.rows.map(v =>
                {
                  return {
                  ...v.toJSON(),
                  proc_from:convertSecondToDayTime(moment(koteikeikaku_update.proc_from).set({hour:0,minute:0}),v.toJSON().proc_from).format(FORMAT_DATE_TIME),
                  proc_to:convertSecondToDayTime(moment(koteikeikaku_update.proc_from).set({hour:0,minute:0}),v.toJSON().proc_to).format(FORMAT_DATE_TIME)
                }
              }
                ).sort((a, b) => {
                  return moment(a.proc_to).diff(moment(b.proc_to))
                });
                if (findGanntChart.length)
                {const range = Util.diffTwoDateAsHour(koteikeikaku_update.proc_from,findGanntChart[0].proc_from)
                 await SdSyoKoteiMainGannt.query().where({
                    koutei_code,
                    seihin_code: koteikeikaku.seihin_code,
                    syo_koutei_syurui_seq: koteikeikaku_update.syo_koutei_syurui_seq,
                    syo_koutei_koho_seq: koteikeikaku_update.syo_koutei_koho_seq,
                    koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                  }).delete()
                await Promise.all(
                findGanntChart.map(async (value) => {
                  await SdSyoKoteiMainGannt
                    .create({
                      koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                      seihin_code: value.seihin_code,
                      koutei_code,
                      syo_koutei_code: value.syo_koutei_code,
                      syo_koutei_seq: value.syo_koutei_seq,
                      syo_koutei_junjo: value.syo_koutei_junjo,
                      syo_koutei_koho_seq: value.syo_koutei_koho_seq,
                      syo_koutei_syurui_seq: value.syo_koutei_syurui_seq,
                      proc_to: Util.addDateWithHour(value.proc_to,range,"sub"),
                      main_sub: value.main_sub,
                      proc_from: Util.addDateWithHour(value.proc_from,range,"sub"),
                      lock_flag: true,
                    }, trx);
                })
              );}
            await SdSyokouteiKohoDenpyo.query()
              .where({
                syo_koutei_koho_seq: firstSet.syo_koutei_koho_seq,
                koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                syo_koutei_syurui_seq: firstSet.syo_koutei_syurui_seq,
                seihin_code: koteikeikaku.seihin_code,
                koutei_code: koutei_code,
              })
              .update({
                saiyo_flag: true,
              }, trx);
            await SdSyokouteiKohoDenpyo.query()
              .where({
                syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
                koteikeikaku_no: koteikeikaku.koteikeikaku_no,
                syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
                seihin_code: koteikeikaku.seihin_code,
                koutei_code: koutei_code,
              })
              .update({
                saiyo_flag: false,
              }, trx);
          }
        } if (
          timeUpdate.proc_to !== timeSet.proc_to
        ) {
          /**
           *  
           *  case3
           * 
           */

          let findMainGant = await SdSyoKoteiMainGannt.query()
            .where({
              koteikeikaku_no: koteikeikaku.koteikeikaku_no,
              koutei_code: koutei_code,
              syo_koutei_code: koteikeikaku.syo_koutei_code,
            })
            .fetch();

          let findMainGantJson = Util.convertToJSON(findMainGant);
          await Promise.all(
            findMainGantJson.map(async (value) => {
              await SdSyoKoteiMainGannt.query()
                .where({
                  seq: value.seq,
                })
                .update({
                  proc_to: moment(value.proc_to)
                    .add(rangerAffterChange, "hours")
                    .format(FORMAT_DATE_TIME),
                  proc_from: moment(value.proc_from)
                    .add(rangerAffterChange, "hours")
                    .format(FORMAT_DATE_TIME),
                }, trx);
            })
          );
        }
      }
      else {
        // /** #CASE 4
        //  *  case change Proc_to
        //  *
        //  */
        let findMainGant = await SdSyoKoteiMainGannt.query()
          .where({
            koteikeikaku_no: koteikeikaku.koteikeikaku_no,
            koutei_code: koutei_code,
            syo_koutei_code: koteikeikaku.syo_koutei_code,
            syo_koutei_koho_seq: koteikeikaku.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: koteikeikaku.syo_koutei_syurui_seq,
            seihin_code: koteikeikaku.seihin_code
          })
          .fetch();
        let hasMaintGannt = false
        findMainGant = Util.convertToJSON(findMainGant);
        const mainSub = findMainGant
          .map((gant, index) => {
            if (gant.main_sub === typeOfMainGant.main) hasMaintGannt = true
            return {
              seq: gant.seq,
              type: gant.main_sub,
              proc_to: gant.proc_to,
              proc_from: gant.proc_from,
              range: Util.diffTwoDateAsHour(gant.proc_from, gant.proc_to, "sub"),
              distance:
                index === findMainGant.length - 1
                  ? 0
                  : moment(findMainGant[index + 1].proc_to).hour() -
                  moment(gant.proc_from).hour(),
            };
          });
        if (!mainSub.length) throw messHasNoGant
        const rangerChange = timeUpdate.range - timeSet.range
        let result = [];
        let mantGant = mainSub.reduce((acc, curr) => {
          if (curr.type === typeOfMainGant.main) acc = [...acc, curr];
          return acc;
        }, []).sort((a, b) => {
          return moment(a.proc_to).diff(moment(b.proc_to))
        });
        let beforeGant = mainSub.reduce((acc, curr) => {
          if (curr.type === typeOfMainGant.before) acc = [...acc, curr];
          return acc;
        }, []).sort((a, b) => {
          return moment(a.proc_to).diff(moment(b.proc_to))
        });
        let afterGant = mainSub.reduce((acc, curr) => {
          if (curr.type === typeOfMainGant.after) acc = [...acc, curr];
          return acc;
        }, []).sort((a, b) => {
          return moment(a.proc_to).diff(moment(b.proc_to))
        });

        if (!hasMaintGannt) {
          /**case mo rong ben phai*/
          if (timeSet.range > timeUpdate.range) throw messCanNotMove
          if (
            timeSet.proc_from === timeUpdate.proc_from &&
            timeSet.proc_to < timeUpdate.proc_to
          ) {
            if (!afterGant.length) throw messCanNotMove
            afterGant = afterGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange),
              }
            })
            console.log("cpon cacacac")
          }
          /**case mo rong ben trai*/
          if (
            timeSet.proc_to === timeUpdate.proc_to &&
            timeSet.proc_from > timeUpdate.proc_from
          ) {
            if (!beforeGant.length) throw messCanNotMove
            beforeGant = beforeGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange, "sub"),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange, "sub"),
              }
            })
          }
          /**
       * case thu nhỏ bên phai
       */
        } else {
          
          if (
            timeSet.proc_to > timeUpdate.proc_to &&
            timeSet.proc_from === timeUpdate.proc_from
          ) {
            let hasMainOutOfRange = false
            mantGant = mantGant.map((v, i) => {
              if (Util.compareTwoDate(v.proc_to, koteikeikaku_update.proc_to)) {
                hasMainOutOfRange = true
                /**case main gannt < 0 ====> throw err */
                if (Math.abs(rangerChange) >= v.range)
                  throw messCanNotMove
                v.proc_to = koteikeikaku_update.proc_to
              }
              return { ...v }
            })
            if (!hasMainOutOfRange)
              mantGant = mantGant.map((v, i) => {
                if (Util.diffTwoDateAsHour(v.proc_to, mantGant[mantGant.length - 1].proc_to, "sub") === 0) {
                  /**case main gannt < 0 ====> throw err */
                  if (Math.abs(rangerChange) >= v.range)
                    throw messCanNotMove
                  v.proc_to = Util.addDateWithHour(v.proc_to, rangerChange)

                }
                return { ...v }
              })
            afterGant = afterGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange),
              }
            })
          }
          /**
           * case thu nhỏ bên trai
           */
          if (
            timeSet.proc_to === timeUpdate.proc_to &&
            timeSet.proc_from < timeUpdate.proc_from
          ) {
            let hasMainOutOfRange = false
            mantGant = mantGant.map((v, i) => {
              if (Util.compareTwoDate(koteikeikaku_update.proc_from, v.proc_from)) {
                hasMainOutOfRange = true
                /**case main gannt < 0 ====> throw err */
                if (rangerChange > v.range)
                  throw messCanNotMove
                v.proc_from = koteikeikaku_update.proc_from
              }
              return { ...v }
            })

            if (!hasMainOutOfRange)
              mantGant = mantGant.map((v, i) => {
                if (Util.diffTwoDateAsHour(v.proc_from, mantGant[0].proc_from, "sub") === 0) {
                  /**case main gannt < 0 ====> throw err */
                  if (Math.abs(rangerChange) >= v.range)
                    throw messCanNotMove
                  v.proc_from = Util.addDateWithHour(v.proc_from, rangerChange, "sub")
                }
                return { ...v }
              })
            beforeGant = beforeGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange, "sub"),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange, "sub"),
              }
            })
          }
          /**
           * case mở rong ben phai
           */
          if (
            timeSet.proc_from === timeUpdate.proc_from &&
            timeSet.proc_to < timeUpdate.proc_to
          ) {
            mantGant[mantGant.length - 1].proc_to = Util.addDateWithHour(mantGant[mantGant.length - 1].proc_to, rangerChange)
            afterGant = afterGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange),
              }
            })
          }
          /**case mo rong ben trai*/
          if (
            timeSet.proc_to === timeUpdate.proc_to &&
            timeSet.proc_from > timeUpdate.proc_from
          ) {
            mantGant[0].proc_from = Util.addDateWithHour(mantGant[0].proc_from, rangerChange, "sub")
            beforeGant = beforeGant.map((v, i) => {
              return {
                ...v,
                proc_to: Util.addDateWithHour(v.proc_to, rangerChange, "sub"),
                proc_from: Util.addDateWithHour(v.proc_from, rangerChange, "sub"),
              }
            })
              console.log("🖕👅🖕 ~ file: StSeisansijiController.js ~ line 888 ~ StSeisansijiController ~ beforeGant=beforeGant.map ~ beforeGant123123", beforeGant)
          }
        }
        // selfTask.startTimeStamp < itR.endTimeStamp &&
        // selfTask.endTimeStamp > itR.startTimeStamp
        /**
         * Check overlap
         */
        if (beforeGant.length) {
          beforeGant.map((v, i) => {
            const find = beforeGant.find((_v,_i) => {
              if (v.seq === _v.seq) return false
             return Util.compareTwoDate(_v.proc_to, v.proc_from) && Util.compareTwoDate(v.proc_to, _v.proc_from)
            })
            if (find) throw messCanNotMove
          })
        }
        if (afterGant.length) {
          afterGant.map((v, i) => {
            const find = afterGant.find((_v,_i) =>{
              if (v.seq === _v.seq) return false
              return Util.compareTwoDate(_v.proc_to, v.proc_from) && Util.compareTwoDate(v.proc_to, _v.proc_from)
              })
            if (find) throw messCanNotMove
          })
        }
        if (beforeGant.length && afterGant.length)
          beforeGant.map((v, i) => {
            const find = afterGant.find(_v => Util.compareTwoDate(_v.proc_to, v.proc_from) && Util.compareTwoDate(v.proc_to, _v.proc_from))
            if (find) throw messCanNotMove
          })
        result = [...beforeGant, ...mantGant, ...afterGant]
        result.map(v => {
          if (Util.compareTwoDate(v.proc_to, koteikeikaku_update.proc_to) || Util.compareTwoDate(koteikeikaku_update.proc_from, v.proc_from))
            throw `${messCanNotMove}`
        })
        if (result.length) {
          await Promise.all(
            result.map(async (value) => {
              await SdSyoKoteiMainGannt.query()
                .where({
                  seq: value.seq,
                })
                .update({
                  proc_to: moment(value.proc_to).format(FORMAT_DATE_TIME),
                  proc_from: moment(value.proc_from).format(FORMAT_DATE_TIME),
                }, trx);
            })
          );
        }
      }

      trx.commit();

      return response.json({
        success: true,
        statusCode: 200,
        data: true,
        // error: `エラーを発生しました： ${error}`
      });
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  async groupBySyokoutei({ request, response }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      // const seihinCodes = !isEmpty(params.seihin_keyword)
      //   ? (
      //     await SmSeihinMst.query()
      //       .where(function () {
      //         this.where(
      //           "seihin_name",
      //           "LIKE",
      //           "%" + params.seihin_keyword + "%"
      //         ).orWhere(
      //           "seihin_code",
      //           "LIKE",
      //           "%" + params.seihin_keyword + "%"
      //         );
      //       })
      //       .fetch()
      //   ).toJSON()
      //   : undefined;

      const query = StSeisansiji.query();

      let strQuery = [];
      let queryParams = [];

      // if (seihinCodes) {
      //   if (!seihinCodes.length) {
      //     return response.json({
      //       success: true,
      //       message: "Success",
      //       pagination: {
      //         total: 0,
      //         perPage,
      //         page,
      //       },
      //       data: [],
      //     });
      //   }
      //   strQuery.push(
      //     `(${seihinCodes
      //       .map((item) => {
      //         queryParams.push(item.seihin_code);
      //         return `seihin_code = ?`;
      //       })
      //       .join(" OR ")})`
      //   );
      // }

      if (!isEmpty(params.sort_field) && !isEmpty(params.sort_order)) {
        query.orderBy(params.sort_field, params.sort_order);
      }
      if (!isEmpty(params.start_datetime) && !isEmpty(params.nouki_datetime)) {
        query.whereRaw(
          "(start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime >= ? AND nouki_datetime <= ?)",
          [
            params.start_datetime,
            params.start_datetime,
            params.nouki_datetime,
            params.nouki_datetime,
            params.start_datetime,
            params.nouki_datetime,
          ]
        );
      }
      query.whereRaw(
        strQuery.map((str) => `(${str})`).join(" and "),
        queryParams
      );

      let stSeisansijiList = await query
        .with("seihin", (builder) => {
          builder.with("s_m_koutei_list");
        })
        .paginate(page, perPage);
      const mapSubProcessDenpyos = {};
      let list = stSeisansijiList.toJSON().data;
      list.forEach((item) => {
        if (item.__meta__ && item.__meta__.sdSyoKoteiDenpyos) {
          item.__meta__.sdSyoKoteiDenpyos.toJSON().forEach((den) => {
            mapSubProcessDenpyos[
              `${den.koteikeikaku_no} -- ${den.seihin_code} -- ${den.koutei_code} -- ${den.syo_koutei_code} -- ${den.syo_koutei_seq} -- ${den.syo_koutei_junjo}`
            ] = den;
          });
        }
      });

      let koutei_list = [];
      for (let i = 0; i < list.length; i++) {
        if (list[i]["s-d-syo-kotei-main-gaint-list"].rows.length != 0) {
          for (
            let j = 0;
            j < list[i]["s-d-syo-kotei-main-gaint-list"].rows.length;
            j++
          ) {
            koutei_list.push(
              { ...list[i]["s-d-syo-kotei-main-gaint-list"].rows[j]["$attributes"], seihin: list[i].seihin }
            );
          }
        }
      }
      koutei_list = koutei_list.filter(
        (record) =>
        Util.compareTwoDate(Util.addDateWithHour(record.proc_to,7),params.start_datetime)
      );
      const koutei_data = _.mapValues(
        _.groupBy(koutei_list, "koutei_code"),
        (clist) => clist.map((koutei) => _.omit(koutei, "koutei_code"))
      );

      koutei_list = [];
      for (const koutei in koutei_data) {

        koutei_list.push({
          kotei: koutei_data[koutei][0].seihin.s_m_koutei_list.find(v => v.koutei_code === koutei),
          koutei_code: koutei,
          syokoutei: koutei_data[koutei],
        });
      }

      for (let i = 0; i < koutei_list.length; i++) {
        koutei_list[i].syokoutei = _.mapValues(
          _.groupBy(koutei_list[i].syokoutei, "syo_koutei_code"),
          (clist) =>
            clist.map((syokoutei) => _.omit(syokoutei, "syo_koutei_code"))
        );
      }

      for (let i = 0; i < koutei_list.length; i++) {
        let syo_koutei_list = [];
        for (const syokoutei in koutei_list[i].syokoutei) {
          const find = koutei_list[i].kotei && koutei_list[i].kotei["s-m-syokoutei-list"].find(v => v["syo_koutei_code"] === syokoutei) || ""
          syo_koutei_list.push({
            syokoutei_name: Util.getString(find,'syo_koutei.syo_kotei_name',''),
            syokoutei_code: syokoutei,
            time: koutei_list[i].syokoutei[syokoutei],
          });
        }
        koutei_list[i].syokoutei = syo_koutei_list;
      }

      const syokouteiKohoDenpyo = await Database.select("*").from(
        "s_d_syokoutei_koho_denpyo"
      );
      if(params.seihin_keyword){
        koutei_list=koutei_list.filter(v => v.koutei_code === params.seihin_keyword || v.kotei["kotei-name"] === params.seihin_keyword  )
      }
      koutei_list.map((v, i) => {
        v.syokoutei.map((_v, _i) => {
          koutei_list[i].syokoutei[_i].time = _v.time.filter(___v => {
            const find = syokouteiKohoDenpyo.find(
              (denpyo) =>
                denpyo.koteikeikaku_no === ___v.koteikeikaku_no &&
                denpyo.seihin_code === ___v.seihin_code &&
                denpyo.koutei_code === v.koutei_code &&
                denpyo.syo_koutei_syurui_seq === ___v.syo_koutei_syurui_seq &&
                denpyo.syo_koutei_koho_seq === ___v.syo_koutei_koho_seq
              // && denpyo.saiyo_flag
            );
            return find && find.saiyo_flag;
          })
        });
      });

      koutei_list.map((v, i) => {
        v.syokoutei.map((_v, _i) => {
          let tempt = []
          let time = _.groupBy(_v.time, (data) => {
            return [data.syo_koutei_koho_seq, data.syo_koutei_syurui_seq, data.koteikeikaku_no].join("-")
          })
          const keyOfTime = Object.keys(time)
          keyOfTime.map(v => {
            const temptV = [...time[v]]
            time[v].sort((a, b) => {
              return moment(b.proc_to).diff(moment(a.proc_to))
            })
            const timeRevert = temptV.sort((a, b) => {
              return moment(a.proc_from).diff(moment(b.proc_from))
            })
            tempt.push({
              ...time[v][0],
              proc_from: timeRevert[0].proc_from,
              proc_to: time[v][0].proc_to
            })
          })
          koutei_list[i].syokoutei[_i].time = tempt
        });

      });

      let res = {
        success: true,
        message: "Success",
        pagination: {
          ...stSeisansijiList.pages,
          total: parseInt(stSeisansijiList.pages.total),
        },
        data: koutei_list,
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }



  async groupBySouchi({ request, response }) {
    try {
      let params = request.all();

      const query = StSeisansiji.query();

      if (!isEmpty(params.start_datetime) && !isEmpty(params.nouki_datetime)) {
        query.whereRaw(
          "(start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime >= ? AND nouki_datetime <= ?)",
          [
            params.start_datetime,
            params.start_datetime,
            params.nouki_datetime,
            params.nouki_datetime,
            params.start_datetime,
            params.nouki_datetime,
          ]
        );
      }

      const stSeisansijiList = (await query.fetch()).toJSON();

      const seihinGroups = stSeisansijiList.reduce((gr, item) => {
        gr[item.seihin_code] = gr[item.seihin_code] || { suryo: 0, list: [] };
        gr[item.seihin_code].suryo += +item.suryo;
        gr[item.seihin_code].list.push(item);
        return gr;
      }, {});

      const { seihinsMax, seihinsMin } = Object.keys(seihinGroups).reduce(
        (hs, k) => {
          const { max, min } = seihinGroups[k].list.reduce(
            (m, item) => {
              m.max = m.max
                ? moment(item.nouki_datetime).valueOf() >
                  moment(m.max.nouki_datetime).valueOf()
                  ? item
                  : m.max
                : item;

              m.min = m.min
                ? moment(item.start_datetime).valueOf() <
                  moment(m.min.start_datetime).valueOf()
                  ? item
                  : m.min
                : item;

              return m;
            },
            { max: null, min: null }
          );
          hs.seihinsMax[k] = max;
          hs.seihinsMin[k] = min;

          return hs;
        },
        { seihinsMax: {}, seihinsMin: {} }
      );

      const seihinCodes = stSeisansijiList.map((item) => item.seihin_code);

      const syokouteiSets = seihinCodes.length
        ? (
          await SmSyokouteiSet.query()
            .where("seihin_code", "IN", seihinCodes)
            .fetch()
        ).toJSON()
        : [];

      const {
        uniqueSyokouteiCodes,
        uniqueSyokouteiSets,
      } = syokouteiSets.reduce(
        (hs, item) => {
          hs.uniqueSyokouteiCodes[item.syo_koutei_code] = true;
          hs.uniqueSyokouteiSets[
            `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`
          ] = item;
          return hs;
        },
        { uniqueSyokouteiCodes: {}, uniqueSyokouteiSets: {} }
      );
      if (_.isEmpty(uniqueSyokouteiSets) == false) {
        (
          await SdSyoKoteiMainGannt.query()
            .whereRaw(
              `
            ${Object.keys(uniqueSyokouteiSets)
                .map((key) => {
                  const cond = uniqueSyokouteiSets[key];
                  return `(seihin_code = '${cond.seihin_code}' AND koutei_code = '${cond.koutei_code}' AND syo_koutei_code = '${cond.syo_koutei_code}')`;
                })
                .join(" OR ")}
        `,
              []
            )
            .fetch()
        )
          .toJSON()
          .forEach((item) => {
            const syokouteiSet =
              uniqueSyokouteiSets[
              `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`
              ];
            syokouteiSet.mainGanntList = syokouteiSet.mainGanntList || [];
            syokouteiSet.mainGanntList.push(item);
          });
      } else {
        const syokouteiSet = {};
        syokouteiSet.mainGanntList = [];
      }

      const syoList = (
        await SmSyokouteiSyoList.query()
          .where("syo_koutei_code", "IN", Object.keys(uniqueSyokouteiCodes))
          .fetch()
      )
        .toJSON()
        .filter((item) => !!seihinGroups[item.seihin_code]);

      let souchisCode = {};

      const souchiGroups = syoList.reduce((gr, item) => {
        souchisCode[item.souchi_code] = true;

        gr[item.souchi_code] = gr[item.souchi_code] || { list: [] };
        gr[item.souchi_code].list.push(item);
        return gr;
      }, {});

      souchisCode = Object.keys(souchisCode);

      const souchisMaster = souchisCode.length
        ? (
          await SmSouchiMst.query()
            .where("souchi_code", "IN", souchisCode)
            .fetch()
        )
          .toJSON()
          .reduce((hs, item) => {
            hs[item.souchi_code] = item;
            return hs;
          }, {})
        : {};

      const list = [];
      for (const key in souchiGroups) {
        if (souchiGroups.hasOwnProperty(key)) {
          const group = souchiGroups[key];
          const {
            start_datetime,
            nouki_datetime,
            suryo,
            mae_setup_time,
            std_setup_time,
            ato_setup_time,
            mainGanntList,
          } = group.list.reduce(
            (m, item) => {
              const seihinMin = seihinsMin[item.seihin_code];
              const seihinMax = seihinsMax[item.seihin_code];

              if (seihinMin) {
                m.start_datetime = m.start_datetime
                  ? moment(seihinMin.start_datetime).valueOf() <
                    moment(m.start_datetime).valueOf()
                    ? seihinMin.start_datetime
                    : m.start_datetime
                  : seihinMin.start_datetime;
              }
              if (seihinMax) {
                m.nouki_datetime = m.nouki_datetime
                  ? moment(seihinMax.nouki_datetime).valueOf() >
                    moment(m.nouki_datetime).valueOf()
                    ? seihinMax.nouki_datetime
                    : m.nouki_datetime
                  : seihinMax.nouki_datetime;
              }

              if (seihinGroups[item.seihin_code]) {
                m.suryo += seihinGroups[item.seihin_code].suryo;
              }

              const syokouteiSet =
                uniqueSyokouteiSets[
                `${item.seihin_code} - ${item.koutei_code} - ${item.syo_koutei_code}`
                ];
              if (syokouteiSet) {
                m.mae_setup_time += +syokouteiSet.mae_setup_time;
                m.std_setup_time += +syokouteiSet.std_setup_time;
                m.ato_setup_time += +syokouteiSet.ato_setup_time;
                m.mainGanntList.push(...(syokouteiSet.mainGanntList || []));
              }

              return m;
            },
            {
              start_datetime: null,
              nouki_datetime: null,
              suryo: 0,
              mae_setup_time: 0,
              std_setup_time: 0,
              ato_setup_time: 0,
              mainGanntList: [],
            }
          );

          if (!souchiGroups[key].daisu) {
            souchiGroups[key].daisu = 0;
          }

          let set_proc_time = [];
          for (let i = 0; i < mainGanntList.length; i++) {
            set_proc_time.push(mainGanntList[i]);
          }

          let test_moment = [];
          mainGanntList.forEach((record) => test_moment.push(moment(record.proc_from).utc().format()))

          // let filterDate = mainGanntList.filter(record => 
          //   moment(record.proc_from).format('YYYY-MM-DD') === params.start_datetime.split('T')[0]
          // );

          let std_proc_syohin_su = [];
          for (let i = 0; i < mainGanntList.length; i++) {
            let std_proc_sokudo = await SmSyokouteiSet.query().where({
              seihin_code: mainGanntList[i].seihin_code,
              koutei_code: mainGanntList[i].koutei_code,
              syo_koutei_code: mainGanntList[i].syo_koutei_code,
              syo_koutei_syurui_seq: mainGanntList[i].syo_koutei_syurui_seq,
              syo_koutei_koho_seq: mainGanntList[i].syo_koutei_koho_seq,
            }).first();
            std_proc_sokudo = std_proc_sokudo ? std_proc_sokudo['std_proc_sokudo'] : 0;
            mainGanntList[i]['std_proc_syohin_su'] = ((new Date(mainGanntList[i]['proc_to']).getTime() - new Date(mainGanntList[i]['proc_from']).getTime()) / 60000 / 60) * 60 * std_proc_sokudo;
            std_proc_syohin_su.push(mainGanntList[i]['std_proc_syohin_su']);
          }

          std_proc_syohin_su = std_proc_syohin_su.reduce((accum, item) => accum + item, 0);

          let sagyo_jikoku = [];
          for (let i = 0; i < set_proc_time.length; i++) {
            sagyo_jikoku.push(new Date(set_proc_time[i].proc_from));
            sagyo_jikoku.push(new Date(set_proc_time[i].proc_to));
          }
          let proc_to = await new Date(Math.max.apply(null, sagyo_jikoku));
          let proc_from = await new Date(Math.min.apply(null, sagyo_jikoku));

          const mainSub = ["m", "a", "b"];
          let main_Sub = {};
          (main_Sub.m = []), (main_Sub.a = []), (main_Sub.b = []);

          mainSub.forEach((record) => {
            for (let i = 0; i < set_proc_time.length; i++) {
              if (set_proc_time[i].main_sub == record) {
                main_Sub[record].push(((new Date(set_proc_time[i].proc_to).getTime() - new Date(set_proc_time[i].proc_from).getTime()) / 3600000) * 60);
              }
            }
          });

          for (let main_sub in main_Sub) {
            main_Sub[main_sub] = main_Sub[main_sub].reduce(
              (accum, item) => accum + item,
              0
            );
          }

          let resultMainSub = 0, ty_le = 0, time_result = 0;
          for (let sub in main_Sub) {
            resultMainSub += main_Sub[sub];
          }
          let kadojikoku = await Database.from("s_s_kadojikoku").where("set_date", params.start_datetime);
          let res = await Database.select("*").from("s_s_kihon_setting");
          let kihon_setting_time;
          if (kadojikoku.length != 0) {
            if((kadojikoku[0].kadojikoku_from != null && kadojikoku[0].kadojikoku_to != null) && (kadojikoku[0].kadojikoku_from != "" && kadojikoku[0].kadojikoku_to != "")) {
              let kadojikoku_time = String(Number(kadojikoku[0].kadojikoku_to) - Number(kadojikoku[0].kadojikoku_from)).match(/.{1,2}/g);
              time_result = Number(kadojikoku_time[0]) * 60 + Number(kadojikoku_time[1]);
            } else {
              kihon_setting_time = String(Number(res[0].kadojikoku_to) - Number(res[0].kadojikoku_from)).match(/.{1,2}/g);
              time_result = Number(kihon_setting_time[0]) * 60 + Number(kihon_setting_time[1]);
            }
          } else {
            kihon_setting_time = String(Number(res[0].kadojikoku_to) - Number(res[0].kadojikoku_from)).match(/.{1,2}/g);
            time_result = Number(kihon_setting_time[0]) * 60 + Number(kihon_setting_time[1]);
          }

          ty_le = (resultMainSub / (souchisMaster[key].daisu * time_result)) * 100;

          list.push({
            test_moment,
            souchi_code: key,
            souchi_name: souchisMaster[key] ? souchisMaster[key].souchi_name : "",
            daisu: souchisMaster[key].daisu,
            mae_setup_time,
            std_setup_time,
            ato_setup_time,
            proc_from,
            proc_to,
            suryo: std_proc_syohin_su,
            ty_le,
            main_Sub,
            start_datetime,
            nouki_datetime,
            std_proc_syohin_su,
            s_d_syo_kotei_main_gain_list: mainGanntList,
          });
        }
      }

      let res = {
        success: true,
        message: "Success",
        data: list,
      };

      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }
  /**
   * @swagger
   * /st-seisansiji/create:
   *   post:
   *     description: Tạo chỉ thị sản xuất
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiCreate
   *          schema:
   *            $ref: '#/definitions/StSeisansijiCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        start_datetime: "string",
        koteikeikaku_no: "string",
        suryo: "integer",
        seihin_code: "required|string|max:20",
        nouki_datetime: "string",
        lock_flag: "boolean",
        kado_jikangai_flag: "boolean",
        gannt_color: "string",
      };
      const data = request.only(Object.keys(rules));

      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }
      let resCheck = await SmSeihinMst.query()
        .where({
          seihin_code: data.seihin_code,
        })
        .first();
      if (isEmpty(resCheck)) {
        return response.json({
          success: false,
          message: "seihin_code not exist",
          statusCode: 501,
          errors: validation.messages(),
        });
      }

      const trx = await Database.beginTransaction();

      if (data.koteikeikaku_no) {
        data.koteikeikaku_no = data.koteikeikaku_no.trim();
        if (!data.koteikeikaku_no) {
          return response.json({
            success: false,
            message: "koteikeikaku_no invalid",
            statusCode: 501,
            errors: validation.messages(),
          });
        }
        let existedKoteikeikakuno = await StSeisansiji.query()
          .where({
            koteikeikaku_no: data.koteikeikaku_no,
          })
          .first();
        if (existedKoteikeikakuno) {
          return response.json({
            success: false,
            message: "Duplicate koteikeikanu",
            statusCode: 501,
            errors: validation.messages(),
          });
        }
      }

      await SmSeihinMst.query()
        .where({
          seihin_code: data.seihin_code,
        })
        .update({
          gannt_color: data.gannt_color,
        }, trx);

      delete data.gannt_color;

      let res = await StSeisansiji.create(data, trx);
      let responseData = {
        success: true,
        message: "create success",
        data: { ...res, seihin_name: resCheck.seihin_name }
      };

      /**
       *  Update to s_d_syokoutei_koho_denpyo
       *    - koteikeikaku_no: res.koteikeikaku_no
       *    - seihin_code: res.seihin_code
       *    - (table: smSyokouteiKoho) -> [koutei_code, syo_koutei_koho_seq, syo_koutei_syurui_seq] 
       * 
       */

      let smKohoList = await Database.table('s_m_syokoutei_koho')
        // .select(Database.raw('Distinct on(koutei_code) koutei_code'), 'seihin_code', 'syo_koutei_koho_seq', 'syo_koutei_syurui_seq')
        .select('koutei_code', 'seihin_code', 'syo_koutei_koho_seq', 'syo_koutei_syurui_seq')
        .where({ seihin_code: data.seihin_code })

      let memorySeq = [];

      // Check and add flag = true to first support tool
      smKohoList = smKohoList.map(obj => {
        if (!memorySeq.includes(obj.syo_koutei_syurui_seq)) {
          memorySeq.push(obj.syo_koutei_syurui_seq)
          return {
            ...obj, saiyo_flag: true
          }
        }
        return { ...obj }
      })


      if (smKohoList && smKohoList.length > 0) {
        smKohoList = smKohoList.map(obj => ({ ...obj, koteikeikaku_no: res.koteikeikaku_no }))
        await SdSyokouteiKohoDenpyo.query().insert(smKohoList, trx)
      }

      /**
       *  Add gantt from main gantt
       */

      let setGantt = await Database.table('s_m_syokoutei_set_gannt')
        .where({ seihin_code: data.seihin_code })

      if (setGantt && setGantt.length > 0) {
        setGantt = setGantt.map(obj => {
          return {
            ...obj,
            koteikeikaku_no: res.koteikeikaku_no,
            proc_from: moment(convertSecondToDayTime(data.start_datetime, obj.proc_from)).zone(TIME_ZONE),
            proc_to: moment(convertSecondToDayTime(data.start_datetime, obj.proc_to)).zone(TIME_ZONE)
          }
        })

        await SdSyoKoteiMainGannt.query().insert(setGantt, trx)
        await SdSyoKoteiMainGanntJ.query().insert(setGantt, trx)
      }

      trx.commit();

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/update:
   *   post:
   *     description: Api update StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiCreate
   *          schema:
   *            $ref: '#/definitions/StSeisansijiUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ request, response }) {
    try {
      let rules = {
        koteikeikaku_no: "required|string|max:20",
        koteikeikaku_seq: "required|string|max:2",
        lock_flag: "boolean",
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }

      await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .update(data);
      const res = await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res,
      };

      // Redis.keys("StSeisansiji_*", async function(err, rows) {
      //   for (var i in rows){
      //     await Redis.del(rows[i])
      //   }
      // })

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/lock:
   *   post:
   *     description: Api lock StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiCreate
   *          schema:
   *            $ref: '#/definitions/StSeisansijiUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  // lock product
  async lock({ request, response }) {
    try {
      let rules = {
        koteikeikaku_no: "required|string|max:20",
        koteikeikaku_seq: "required|string|max:2",
        lock_flag: "boolean",
      };
      const data = request.only(Object.keys(rules));
      const params = request.all();
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }
      await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .update({
          suryo: params.suryo,
          start_datetime: moment(params.start_datetime).format(
            "YYYY-MM-DD HH:mm:ss"
          ),
          nouki_datetime: moment(params.nouki_datetime).format(
            "YYYY-MM-DD HH:mm:ss"
          ),
        });
      // Update children of koutei with lock_flag
      await SdSyoKoteiMainGannt.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
        })
        .update({
          lock_flag: data.lock_flag,
        });

      const updatedStSeisansiji = await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .first();
      const responseData = {
        success: true,
        message: "データを変更しました",
        data: updatedStSeisansiji,
      };

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/lock-by-date:
   *   post:
   *     description: Api lock StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *       - name: start_datetime
   *         description: start_datetime
   *         in: query
   *         type: string
   *       - name: lock_flag
   *         description: lock_flag
   *         in: query
   *         type: boolean
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async lockByDate({ request, response }) {
    try {
      let rules = {
        start_datetime: "string",
        lock_flag: "boolean",
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }
      if (isEmpty(data.lock_flag)) {
        data.lock_flag = true;
      }
      // await StSeisansiji.query().where({
      //   start_datetime: data.start_datetime
      // }).update(data)
      await StSeisansiji.query()
        .where("start_datetime", data.start_datetime)
        .update({ lock_flag: data.lock_flag });

      let responseData = {
        success: true,
        message: "lock success",
      };

      // Redis.keys("StSeisansiji_*", async function(err, rows) {
      //   for (var i in rows){
      //     await Redis.del(rows[i])
      //   }
      // })

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * Display a single StSeisansiji.
   * GET StSeisansiji/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /st-seisansiji/detail:
   *   get:
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API list StSeisansiji
   *     parameters:
   *       - name: koteikeikaku_no
   *         description: page
   *         in: query
   *         type: string
   *       - name: koteikeikaku_seq
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response }) {
    try {
      let rules = {
        koteikeikaku_no: "required",
        koteikeikaku_seq: "required",
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }
      let res = await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res,
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/delete:
   *   post:
   *     description: Api delete StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiDelete
   *          schema:
   *            $ref: '#/definitions/StSeisansijiDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ request, response }) {
    try {
      let rules = {
        koteikeikaku_no: "required|string|max:20",
        koteikeikaku_seq: "required|string|max:2",
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }
      await StSeisansiji.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
          koteikeikaku_seq: data.koteikeikaku_seq,
        })
        .delete();

      // delete support tool in s_d_syokoutei_koho_denpyo
      await SdSyokouteiKohoDenpyo.query()
        .where({
          koteikeikaku_no: data.koteikeikaku_no,
        })
        .delete();

      let responseData = {
        success: true,
        message: "削除しました。",
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/map-koutei:
   *   post:
   *     description: Api create StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiCreate
   *          schema:
   *            $ref: '#/definitions/StSeisansijiMapKoutei'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async mapKoutei({ request, response }) {
    try {
      let rules = {
        delete_all: "integer",
        seihin_code: "string",
        kado_jikangai_flag: "boolean",
        isCreateNew: "boolean",
        koutei_code: "array",
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }

      if (!isEmpty(data.seihin_code)) {
        let resCheck = await SmSeihinMst.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .fetch();
        if (isEmpty(resCheck.rows)) {
          if (!isEmpty(data.isCreateNew) && data.isCreateNew === true) {
            let seihin_code_new = Util.getCode();
            let seihin_new = await SmSeihinMst.create({
              ...resCheck.rows[0],
              seihin_code: seihin_code_new,
            });
            data.seihin_code = seihin_new.seihin_code;
          } else {
            return response.json({
              success: false,
              message: "seihin_code not exist",
              statusCode: 501,
              errors: validation.messages(),
            });
          }
        }
      }
      if (data.delete_all == 1) {
        await SmKouteiSet.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .delete();
      }
      if (!isEmpty(data.koutei_code)) {
        let arrKouteiCode = data.koutei_code;
        let numberAdded = 0;
        for (let x in arrKouteiCode) {
          let check = await SmKouteiSet.query()
            .where({
              seihin_code: data.seihin_code,
              koutei_code: arrKouteiCode[x],
            })
            .fetch();

          let checkSmKouteiMst = await SmKouteiMst.query()
            .where({
              koutei_code: arrKouteiCode[x],
            })
            .fetch();

          if (isEmpty(check.rows) && !isEmpty(checkSmKouteiMst.rows)) {
            const map = await SmKouteiSet.create({
              seihin_code: data.seihin_code,
              koutei_code: arrKouteiCode[x],
            });
            if (map) {
              numberAdded++;
            }
          }
        }
        let responseData = {
          success: false,
          message: "Empty map created",
        };
        if (numberAdded > 0) {
          responseData = {
            success: true,
            message: `Create success ${numberAdded} map`,
          };
        }

        return response.json(responseData);
      } else {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          error: "Koutei code is empty",
        });
      }
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/list-map-koutei:
   *   get:
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API list StSeisansiji
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/StSeisansijiMapKouteiResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async listMapKoutei({ request, response }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmKouteiSet.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows,
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/copy:
   *   post:
   *     description: Api create StSeisansiji
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: StSeisansijiCopy
   *          schema:
   *            $ref: '#/definitions/StSeisansijiCopy'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async copy({ request, response }) {
    try {
      let params = request.all();
      let rules = {
        start_datetime: "string",
        suryo: "integer",
        seihin_code: "required|string|max:20",
        nouki_datetime: "string",
        lock_flag: "boolean",
        kado_jikangai_flag: "boolean",
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages(),
        };
        return response.json(responseData);
      }

      if (!isEmpty(data.seihin_code)) {
        let resCheck = await SmSeihinMst.query()
          .where({
            seihin_code: data.seihin_code,
          })
          .fetch();
        if (isEmpty(resCheck.rows)) {
          return response.json({
            success: false,
            message: "seihin_code not exist",
            statusCode: 501,
            errors: validation.messages(),
          });
        }
      }

      if (params.koteikeikaku_no_new) {
        params.koteikeikaku_no_new = params.koteikeikaku_no_new.trim();
        if (!params.koteikeikaku_no_new) {
          return response.json({
            success: false,
            message: "koteikeikaku_no invalid",
            statusCode: 500,
            errors: validation.messages(),
          });
        }
        const check_kouteikeikaku_no = await StSeisansiji.query()
          .where({
            seihin_code: params.seihin_code,
            koteikeikaku_no: params.koteikeikaku_no_new,
          })
          .fetch();
        if (!isEmpty(check_kouteikeikaku_no.rows)) {
          return response.json({
            success: false,
            message: "Duplicate kouteikeikaku",
            statusCode: 500,
          });
        }
      }

      data.lock_flag = false;
      if (
        !isEmpty(params.koteikeikaku_no_new) &&
        params.koteikeikaku_no_new.length > 0
      ) {
        data.koteikeikaku_no = params.koteikeikaku_no_new;
        for (
          let i = 0;
          i < params["s-d-syo-kotei-main-gaint-list"].length;
          i++
        ) {
          delete params["s-d-syo-kotei-main-gaint-list"][i][
            "s-d-syo-kotei-main-syain-list"
          ];
          delete params["s-d-syo-kotei-main-gaint-list"][i].__meta__;
          const data_grantt = {
            ...params["s-d-syo-kotei-main-gaint-list"][i],
            koteikeikaku_no: params.koteikeikaku_no_new,
          };
          await SdSyoKoteiMainGannt.create(data_grantt);
        }
      }
      const data_copy = await StSeisansiji.create(data);
      if (isEmpty(params.koteikeikaku_no_new)) {
        for (
          let i = 0;
          i < params["s-d-syo-kotei-main-gaint-list"].length;
          i++
        ) {
          delete params["s-d-syo-kotei-main-gaint-list"][i][
            "s-d-syo-kotei-main-syain-list"
          ];
          delete params["s-d-syo-kotei-main-gaint-list"][i].__meta__;
          const data_grantt = {
            ...params["s-d-syo-kotei-main-gaint-list"][i],
            koteikeikaku_no: data_copy.koteikeikaku_no,
          };
          await SdSyoKoteiMainGannt.create(data_grantt);
        }
      }

      console.log('data_copy', data_copy.toJSON(), params.koteikeikaku_no);
      let smKohoList = await Database.table('s_d_syokoutei_koho_denpyo')
        .select('*')
        .where({ koteikeikaku_no: params.koteikeikaku_no })
      if (smKohoList && smKohoList.length > 0) {
        smKohoList = smKohoList.map(obj => ({ ...obj, koteikeikaku_no: data_copy.toJSON().koteikeikaku_no }));
        await SdSyokouteiKohoDenpyo.query().insert(smKohoList)
      }
      return response.json({
        success: true,
        statusCode: 200,
      });
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }

  /**
   * @swagger
   * /st-seisansiji/del-cache:
   *   get:
   *     description: Api del all cache
   *     security:
   *     tags:
   *       - StSeisansiji
   *     summary: Sample API
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delCache({ request, response }) {
    try {
      Redis.keys("*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
      return response.json({
        success: true,
        statusCode: 200,
      });
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`,
      });
    }
  }
}

module.exports = StSeisansijiController;
