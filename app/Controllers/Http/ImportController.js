'use strict'

const Util = use('App/helpers/Util')
const SeisansijiExport = use('App/Commands/SeisansijiExport')
const SmSeihinMst = use('App/Models/SmSeihinMst')
const StSeisansiji = use('App/Models/StSeisansiji')
const SmKouteiSet = use('App/Models/SmKouteiSet')
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet')
const SmKouteiMst = use('App/Models/SmKouteiMst')
const SmSyokouteiMst = use('App/Models/SmSyokouteiMst')
const SmSagyoMst = use('App/Models/SmSagyoMst')
const SmSouchiMst = use('App/Models/SmSouchiMst')
const SmSyokouteiSyoList = use('App/Models/SmSyokouteiSyoList')

const Excel = require('exceljs')
const Helpers = use('Helpers')

const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')
const Logger = use('Logger')
const Redis = use('Redis')
class ImportController {

  /**
   * @swagger
   * /export/main:
   *   post:
   *     description: Api create Export
   *     security:
   *     tags:
   *       - Export
   *     summary: Sample API
   *     parameters:
   *       - name: start_date
   *         description: start_date
   *         in: query
   *         type: string
   *       - name: end_date
   *         description: end_date
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async main({request, response}) {
    try {
      let params = request.all()
      const start = !isEmpty(params.start_date) ? params.start_date : ''
      const end = !isEmpty(params.end_date) ? params.end_date : ''
      if(isEmpty(start) || isEmpty(end)){
        let responseData = {
          success: false,
          message: 'start date or end date empty',
        }
        return response.json(responseData)
      }
      let name_file = await SeisansijiExport.handle({start: start, end: end})
      let responseData = {
        success: true,
        message: 'export success',
        name_file: name_file
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error('error', error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /import/seisansiji:
   *   post:
   *     description: Api create ImportSeisansiji
   *     security:
   *     tags:
   *       - Import
   *     summary: Sample API
   *     parameters:
   *        - in: formData
   *          name: file
   *          type: file
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async seisansiji({request, response}) {
    try {
      let upload = request.file('file')
      let fname = `${new Date().getTime()}.${upload.extname}`
      let dir = 'uploads/'

      //move uploaded file into custom folder
      await upload.move(Helpers.tmpPath(dir), {
        name: fname
      })

      if (!upload.moved()) {
        Logger.error('upload', upload.error())
        return response.status(500).send({
          success: false,
          statusCode: 500,
          error: `Error moving files`
        })
      }

      let filelocation = 'tmp/' + dir + fname
      var workbook = new Excel.Workbook()
      workbook = await workbook.xlsx.readFile(filelocation)
      let explanation = workbook.getWorksheet(1) // get sheet name
      let colComment = explanation.getColumn('C') //column name
      colComment.eachCell(async (cell, rowNumber) => {
        if (rowNumber > 1) {
          let koteikeikaku_no = explanation.getCell('A' + rowNumber).value
          let seihin_code = explanation.getCell('B' + rowNumber).value
          let seihin_name = explanation.getCell('C' + rowNumber).value
          let suryo = explanation.getCell('D' + rowNumber).value
          let start_datetime = explanation.getCell('E' + rowNumber).value
          let nouki_datetime = explanation.getCell('F' + rowNumber).value
          let curl_id = explanation.getCell('G' + rowNumber).value

          //custom field name in database to variable
          await SmSeihinMst.findOrCreate(
            {seihin_code: seihin_code},
            {
              seihin_code: seihin_code,
              seihin_name: seihin_name,
            }
          )

          let inputDataSeisansiji = {
            koteikeikaku_no: koteikeikaku_no,
            seihin_code: seihin_code,
            suryo: suryo,
            start_datetime: start_datetime,
            nouki_datetime: nouki_datetime,
            // curl_id: curl_id,
          }
          Logger.info('inputData', inputDataSeisansiji)
          await StSeisansiji.create(inputDataSeisansiji)
        }
      })
      Redis.keys("*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
      let responseData = {
        success: true,
        message: 'import success',
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error('error', error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * @swagger
   * /import/souchi:
   *   post:
   *     description: Api create ImportSouchi
   *     security:
   *     tags:
   *       - Import
   *     summary: Sample API
   *     parameters:
   *        - in: formData
   *          name: file
   *          type: file
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async souchi({request, response}) {
    try {
      let upload = request.file('file')
      let fname = `${new Date().getTime()}.${upload.extname}`
      let dir = 'uploads/'

      //move uploaded file into custom folder
      await upload.move(Helpers.tmpPath(dir), {
        name: fname
      })

      if (!upload.moved()) {
        Logger.error('upload', upload.error())
        return response.status(500).send({
          success: false,
          statusCode: 500,
          error: `Error moving files`
        })
      }

      let filelocation = 'tmp/' + dir + fname
      var workbook = new Excel.Workbook()
      workbook = await workbook.xlsx.readFile(filelocation)
      let explanation = workbook.getWorksheet(1) // get sheet name
      let colComment = explanation.getColumn('C') //column name
      colComment.eachCell(async (cell, rowNumber) => {
        if (rowNumber > 1) {
          let souchi_name = explanation.getCell('A' + rowNumber).value
          let souchi_code = explanation.getCell('B' + rowNumber).value
          let daisu = explanation.getCell('C' + rowNumber).value

          let inputDataSouchi = {
            souchi_name: souchi_name,
            souchi_code: souchi_code,
            daisu: daisu,
          }

          await SmSouchiMst.findOrCreate(
            {souchi_code: inputDataSouchi.souchi_code},
            inputDataSouchi
          )

        }
      })
      Redis.keys("*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
      let responseData = {
        success: true,
        message: 'import success',
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error('error', error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /import/syokoutei:
   *   post:
   *     description: Api create ImportSyokoutei
   *     security:
   *     tags:
   *       - Import
   *     summary: Sample API
   *     parameters:
   *        - in: formData
   *          name: file
   *          type: file
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async syokoutei({request, response}) {
    try {
      let upload = request.file('file')
      let fname = `${new Date().getTime()}.${upload.extname}`
      let dir = 'uploads/'

      //move uploaded file into custom folder
      await upload.move(Helpers.tmpPath(dir), {
        name: fname
      })

      if (!upload.moved()) {
        Logger.error('upload', upload.error())
        return response.status(500).send({
          success: false,
          statusCode: 500,
          error: `Error moving files`
        })
      }

      let filelocation = 'tmp/' + dir + fname
      var workbook = new Excel.Workbook()
      workbook = await workbook.xlsx.readFile(filelocation)
      let explanation = workbook.getWorksheet(1) // get sheet name
      let colComment = explanation.getColumn('C') //column name
      colComment.eachCell(async (cell, rowNumber) => {
        if (rowNumber > 1) {
          let seihin_name = explanation.getCell('A' + rowNumber).value
          let seihin_code = explanation.getCell('B' + rowNumber).value
          let kotei_name = explanation.getCell('C' + rowNumber).value
          let koutei_code = explanation.getCell('D' + rowNumber).value
          let syo_kotei_name = explanation.getCell('E' + rowNumber).value
          let syo_koutei_code = explanation.getCell('F' + rowNumber).value
          let sagyo_name = explanation.getCell('G' + rowNumber).value
          let sagyo_code = explanation.getCell('H' + rowNumber).value
          let souchi_name = explanation.getCell('I' + rowNumber).value
          let souchi_code = explanation.getCell('J' + rowNumber).value
          let mae_setup_time = explanation.getCell('K' + rowNumber).value
          let std_proc_sokudo = explanation.getCell('L' + rowNumber).value
          let ato_setup_time = explanation.getCell('M' + rowNumber).value
          let post_end_aft = explanation.getCell('N' + rowNumber).value
          let interval = explanation.getCell('O' + rowNumber).value
          let post_start_bef = explanation.getCell('P' + rowNumber).value
          let post_start_aft = explanation.getCell('Q' + rowNumber).value
          let post_end_bef = explanation.getCell('R' + rowNumber).value
          // let post_end_aft = explanation.getCell('S' + rowNumber).value

          await SmSeihinMst.findOrCreate(
            {seihin_code: seihin_code},
            {
              seihin_code: seihin_code,
              seihin_name: seihin_name
            }
          )

          await SmKouteiMst.findOrCreate(
            {koutei_code: koutei_code},
            {
              koutei_code: koutei_code,
              kotei_name: kotei_name
            }
          )

          await SmSyokouteiMst.findOrCreate(
            {syo_koutei_code: syo_koutei_code},
            {
              syo_kotei_name: syo_kotei_name,
              syo_koutei_code: syo_koutei_code
            }
          )

          await SmKouteiSet.findOrCreate(
            {
              seihin_code: seihin_code,
              koutei_code: koutei_code
            },
            {
              seihin_code: seihin_code,
              koutei_code: koutei_code
            }
          )

          await SmSagyoMst.findOrCreate(
            {
              sagyo_code: sagyo_code
            },
            {
              sagyo_code: sagyo_code,
              sagyo_name: sagyo_name
            }
          )

          await SmSouchiMst.findOrCreate(
            {
              souchi_code: souchi_code
            },
            {
              souchi_code: souchi_code,
              souchi_name: souchi_name
            }
          )

          let resSmSyokouteiSet = await SmSyokouteiSet.findOrCreate(
            {
              seihin_code: seihin_code,
              koutei_code: koutei_code,
              syo_koutei_code: syo_koutei_code,
              syo_koutei_seq: 0
            },{
            seihin_code: seihin_code,
            koutei_code: koutei_code,
            syo_koutei_code: syo_koutei_code,
            // std_proc_syohin_su: std_proc_syohin_su,
            // max_proc_syohin_su: max_proc_syohin_su,
            // min_proc_syohin_su: min_proc_syohin_su,
            std_proc_sokudo: std_proc_sokudo,
            // max_proc_sokudo: max_proc_sokudo,
            // min_proc_sokudo: min_proc_sokudo,
            // std_kadoritu: std_kadoritu,
            // std_setup_time: std_setup_time,
            mae_setup_time: mae_setup_time,
            ato_setup_time: ato_setup_time,
            post_start_aft: post_start_aft,
            post_end_aft: post_end_aft,
            post_start_bef: post_start_bef,
            post_end_bef: post_end_bef
          })

          await SmSyokouteiSyoList.create({
            seihin_code: seihin_code,
            koutei_code: koutei_code,
            syo_koutei_code: syo_koutei_code,
            syo_koutei_seq: resSmSyokouteiSet.syo_koutei_seq,
            sagyo_code: sagyo_code,
            souchi_code: souchi_code,
          })
        }
      })
      let responseData = {
        success: true,
        message: 'import success',
      }

      Redis.keys("*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })

      return response.json(responseData)
    } catch (error) {
      Logger.error('error', error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

}

module.exports = ImportController
