'use strict'
const SmHan = use('App/Models/SmHan')
const SmHanKoutei = use('App/Models/SmHanKoutei')
const SmKouteiMst = use('App/Models/SmKouteiMst')
const SsKosuKengen = use('App/Models/SsKosuKengen')
const SsSystemSetting = use('App/Models/SsSystemSetting')
const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmHans
 */
class SmHanController {
  /**
   * Show a list of all SmHans.
   * GET SmHans
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-han/list:
   *   get:
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API list SmHan
   *     parameters:
   *       - name: han_name
   *         description: han_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({request, response, view}) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20
      const query = SmHan.query()
      if (!isEmpty(params.han_name)) {
        query.where('han_name', 'LIKE', '%' + params.han_name + '%')
      }
      let data = await query.paginate(page, perPage)
      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmHan.
   * GET SmHans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-han/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API
   *     parameters:
   *       - name: han_name
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({params, request, response, view}) {
    try {
      let rules = {
        han_name: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmHan
        .query()
        .where({'han_name': data.han_name})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmHan.
   * POST SmHans
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han/create:
   *   post:
   *     description: Api create SmHan
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanCreate
   *          schema:
   *            $ref: '#/definitions/SmHanCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({request, response}) {
    try {
      let rules = {
        han_name: 'required|string',
        pass: 'required|string',
      }
      let rulesKouteiCodes = {
        koutei_codes: 'array',
      }
      const data = request.only(Object.keys(rules))
      const dataKouteiCodes = request.only(Object.keys(rulesKouteiCodes))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      const check = await SmHan.query().where({
        'han_name': data.han_name
      }).first()
      if (!isEmpty(check)) {
        let responseData = {
          success: false,
          message: 'SmHan ' + data.han_name + ' exist',
        }
        return response.json(responseData)
      }
      let res = await SmHan.create(data)

      if(!isEmpty(dataKouteiCodes.koutei_codes)){
        let arrSmKoutei = dataKouteiCodes.koutei_codes
        //xóa map cũ
        await SmHanKoutei.query().where({'han_name': data.han_name}).delete()
        //thêm map mới
        for (let x in arrSmKoutei) {
          await SmHanKoutei.create({koutei_code: arrSmKoutei[x], han_name: data.han_name})
        }
      }

      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmHan details.
   * PUT or PATCH SmHans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han/update:
   *   post:
   *     description: Api update SmHan
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanCreate
   *          schema:
   *            $ref: '#/definitions/SmHanUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({params, request, response}) {
    try {
      let rules = {
        han_name: 'required|string',
        pass: 'string',
      }
      let rulesUpdate = {
        data_update: 'object',
      }
      const data = request.only(Object.keys(rules))
      const dataUpdate = request.only(Object.keys(rulesUpdate))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      if(isEmpty(dataUpdate.data_update)){
        let responseData = {
          success: false,
          message: 'data update empty',
        }
        return response.json(responseData)
      }
      let dataKouteiCodes = {
        koutei_codes: []
      }
      if(!isEmpty(dataUpdate.data_update.koutei_codes)){
        dataKouteiCodes.koutei_codes = dataUpdate.data_update.koutei_codes
      }
      delete dataUpdate.data_update.koutei_codes

      let han_name = data.han_name
      if(!isEmpty(dataUpdate.data_update.han_name)){
        han_name = dataUpdate.data_update.han_name
      }
      await SmHan.query().where(data).update(dataUpdate.data_update)
      const res = await SmHan.query().where({
        'han_name': han_name
      }).fetch()

      if(!isEmpty(dataKouteiCodes.koutei_codes)){
        //xóa map cũ
        await SmHanKoutei.query().where({'han_name': data.han_name}).delete()
        //thêm map mới
        let arrSmKoutei = dataKouteiCodes.koutei_codes
        for (let x in arrSmKoutei) {
            await SmHanKoutei.create({koutei_code: arrSmKoutei[x], han_name: han_name})
        }
      }

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmHan with id.
   * DELETE SmHans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-han/delete:
   *   post:
   *     description: Api delete SmHan
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanDelete
   *          schema:
   *            $ref: '#/definitions/SmHanDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({params, request, response}) {
    try {
      let rules = {
        han_name: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmHan
        .query()
        .where({
          'han_name': data.han_name
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * @swagger
   * /sm-han/login:
   *   post:
   *     description: Api create SmHan
   *     security:
   *     tags:
   *       - SmHan
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmHanLogin
   *          schema:
   *            $ref: '#/definitions/SmHanLogin'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmHanResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async login({request, response}) {
    try {
      let rules = {
        han_name: 'required|string',
        pass: 'required|string'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      const checkLogin = await SmHan.query().where({
        'han_name': data.han_name,
        'pass': data.pass,
      }).first()
      if (isEmpty(checkLogin)) {
        let responseData = {
          success: false,
          message: 'The account or password is incorrect'
        }
        return response.json(responseData)
      }

      let res = await SmHanKoutei
        .query()
        .where({'han_name': data.han_name})
        .fetch()
      let arrKoutei = []
      res.rows.map((row) => {
        arrKoutei.push(row.koutei_code)
      })

      const kosuKengen = await SsKosuKengen
      .query()
      .where({'han_id': checkLogin.id})
      .first();

      const systemSetting = await SsSystemSetting.query().last();

      let resData = []
      if(!isEmpty(arrKoutei)){
        resData = await SmKouteiMst.query().whereIn('koutei_code', arrKoutei).fetch()
      }
      let responseData = {
        success: true,
        message: 'login success',
        data: resData,
        permissions: kosuKengen,
        systemSettings: systemSetting
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmHanController
