'use strict'
const SmSagyoMst = use('App/Models/SmSagyoMst')

const Util = use('App/helpers/Util')
const {validateAll} = use('Validator')
const isEmpty = require('lodash.isempty')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSagyoMsts
 */
class SmSagyoMstController {
  /**
   * Show a list of all SmSagyoMsts.
   * GET SmSagyoMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-sagyo-mst/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSagyoMst
   *     summary: Sample API list SmSagyoMst
   *     parameters:
   *       - name: sagyo_code
   *         description: sagyo_code
   *         in: query
   *         type: string
   *       - name: sagyo_name
   *         description: sagyo_name
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSagyoMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list ({ request, response, view }) {
    try {
      let params = request.all()
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

      const query = SmSagyoMst.query()
      query.orderBy('created_at', 'desc')
      if(!isEmpty(params.sagyo_code)){
        query.where('sagyo_code', 'LIKE', '%'+params.sagyo_code+'%')
      }
      if(!isEmpty(params.sagyo_name)){
        query.where('sagyo_name', 'LIKE', '%'+params.sagyo_name+'%')
      }
      let data = await query.paginate(page, perPage)

      let res = {
        success: true,
        pagination: {...data.pages, total: parseInt(data.pages.total)},
        data: data.rows
      }
      return response.json(res)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Display a single SmSagyoMst.
   * GET SmSagyoMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-sagyo-mst/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSagyoMst
   *     summary: Sample API
   *     parameters:
   *       - name: sagyo_code
   *         description: page
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSagyoMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail ({ params, request, response, view }) {
    try {
      let rules = {
        sagyo_code: 'required'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      let res = await SmSagyoMst
        .query()
        .where({'sagyo_code': data.sagyo_code})
        .fetch()
      let responseData = {
        success: true,
        message: 'success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Create/save a new SmSagyoMst.
   * POST SmSagyoMsts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-sagyo-mst/create:
   *   post:
   *     description: Api create SmSagyoMst
   *     security:
   *     tags:
   *       - SmSagyoMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSagyoMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSagyoMstCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSagyoMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create ({ request, response }) {
    try {
      let rules = {
        sagyo_code: 'string|max:8',
        sagyo_name: 'required|string|max:128',
        biko: 'string|max:512'
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.status(501).json(responseData)
      }

      if(isEmpty(data.sagyo_code)){
        data.sagyo_code = Util.getCode();
      }
      //check code in db
      const check = await SmSagyoMst.query().where({
        'sagyo_code': data.sagyo_code
      }).fetch()
      if(!isEmpty(check.rows)){
        return response.status(501).json({
          success: false,
          message: 'sagyo_code exist',
          statusCode: 501,
        })
      }

      let res = await SmSagyoMst.create(data)
      let responseData = {
        success: true,
        message: 'create success',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }


  /**
   * Update SmSagyoMst details.
   * PUT or PATCH SmSagyoMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-sagyo-mst/update:
   *   post:
   *     description: Api update SmSagyoMst
   *     security:
   *     tags:
   *       - SmSagyoMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSagyoMstCreate
   *          schema:
   *            $ref: '#/definitions/SmSagyoMstUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSagyoMstResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update ({ params, request, response }) {
    try {
      let rules = {
        sagyo_code: 'required|string:max:8',
        sagyo_name: 'string|max:128',
        biko: 'string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }

      await SmSagyoMst.query().where({
        'sagyo_code': data.sagyo_code
      }).update(data)
      const res = await SmSagyoMst.query().where({
        'sagyo_code': data.sagyo_code
      }).fetch()

      let responseData = {
        success: true,
        message: 'データを変更しました',
        data: res
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error + '')
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }

  /**
   * Delete a SmSagyoMst with id.
   * DELETE SmSagyoMsts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-sagyo-mst/delete:
   *   post:
   *     description: Api delete SmSagyoMst
   *     security:
   *     tags:
   *       - SmSagyoMst
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSagyoMstDelete
   *          schema:
   *            $ref: '#/definitions/SmSagyoMstDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete ({ params, request, response }) {
    try {
      let rules = {
        sagyo_code: 'required|string',
      }
      const data = request.only(Object.keys(rules))
      const validation = await validateAll(data, rules)
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        }
        return response.json(responseData)
      }
      await SmSagyoMst
        .query()
        .where({
          'sagyo_code': data.sagyo_code
        })
        .delete()
      let responseData = {
        success: true,
        message: '削除しました。',
      }
      return response.json(responseData)
    } catch (error) {
      console.error(error)
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      })
    }
  }
}

module.exports = SmSagyoMstController
