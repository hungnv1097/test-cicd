'use strict'
const _ = require('lodash')
const isEmpty = require('lodash.isempty')
const StaffFree = use('App/Models/StaffFree')
const SdSyoKoteiMainSyain = use('App/Models/SdSyoKoteiMainSyain')
const Database = use('Database')

class StaffFreeController {
	async list({ request, response }) {
		try {
			let params = request.all()
			const page = !isEmpty(params.page) ? parseInt(params.page) : 1
			const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

			const data = await Database.table('staff_free')
				.leftJoin('syain', 'staff_free.syain_id', 'syain.id')
				.select([
					'syain.syain_name',
					'staff_free.*'
				])
				.paginate(page, perPage);

			const addedStaff = await Database.table('s_d_syo_kotei_main_syain')
				.where({
					koteikeikaku_no: params.koteikeikaku_no,
					seihin_code: params.seihin_code,
					koutei_code: params.koutei_code,
					syo_koutei_code: params.syo_koutei_code,
					syo_koutei_seq: params.syo_koutei_seq,
					syo_koutei_junjo: params.syo_koutei_junjo,
				})
				.leftJoin('syain', 's_d_syo_kotei_main_syain.syain_id', 'syain.id')
				.select([
					's_d_syo_kotei_main_syain.syain_id',
				])
				.paginate(page, perPage);

			const res = {
				success: true,
				message: "Success",
				pagination: {
					perPage: data.perPage,
					page: data.page,
					lastPage: data.lastPage,
					total: parseInt(data.total)
				},
				data: data.data.filter((item) => !addedStaff.data.map((item2) => item2.syain_id).includes(item.syain_id))
			}

			return response.json(res)
		} catch (error) {
			console.error(error)
			return response.status(400).send({
				success: false,
				statusCode: 400,
				error: `エラーを発生しました： ${error}`
			})
		}
	}

	async listAdded({ request, response }) {
		try {
			let params = request.all()
			const page = !isEmpty(params.page) ? parseInt(params.page) : 1
			const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20

			const data = await Database.table('staff_free')
				.leftJoin('syain', 'staff_free.syain_id', 'syain.id')
				.select([
					'syain.syain_name',
					'staff_free.*'
				])
				.paginate(page, perPage);

			const addedStaff = await Database.table('s_d_syo_kotei_main_syain')
				.where({
					koteikeikaku_no: params.koteikeikaku_no,
					seihin_code: params.seihin_code,
					koutei_code: params.koutei_code,
					syo_koutei_code: params.syo_koutei_code,
					syo_koutei_seq: params.syo_koutei_seq,
					syo_koutei_junjo: params.syo_koutei_junjo,
				})
				.leftJoin('syain', 's_d_syo_kotei_main_syain.syain_id', 'syain.id')
				.select([
					's_d_syo_kotei_main_syain.syain_id',
				])
				.paginate(page, perPage);

			const res = {
				success: true,
				message: "Success",
				pagination: {
					perPage: data.perPage,
					page: data.page,
					lastPage: data.lastPage,
					total: parseInt(data.total)
				},
				data: data.data.filter((item) => addedStaff.data.map((item2) => item2.syain_id).includes(item.syain_id))
			}

			return response.json(res)
		} catch (error) {
			return response.status(400).send({
				success: false,
				statusCode: 400,
				error: `エラーを発生しました： ${error}`
			})
		}
	}

	async addStaff({ params, request, response }) {
		try {
			const data = request.body;
			const freeStaffs = data.list_selected_staff;

			await SdSyoKoteiMainSyain.query().where({
				koteikeikaku_no: data.koteikeikaku_no,
				seihin_code: data.seihin_code,
				koutei_code: data.koutei_code,
				syo_koutei_code: data.syo_koutei_code,
				syo_koutei_seq: data.syo_koutei_seq,
				syo_koutei_junjo: data.syo_koutei_junjo
			}).delete();

			await Promise.all(
				freeStaffs.map(staff => SdSyoKoteiMainSyain.query().insert({
					koteikeikaku_no: data.koteikeikaku_no,
					seihin_code: data.seihin_code,
					koutei_code: data.koutei_code,
					syo_koutei_code: data.syo_koutei_code,
					syo_koutei_seq: data.syo_koutei_seq,
					syo_koutei_junjo: data.syo_koutei_junjo,
					proc_from: data.proc_from,
					proc_to: data.proc_to,
					seq: data.seq,
					freestaff_flag: staff.free_flag,
					syain_id: staff.syain_id
				}))
			);

			let responseData = {
				success: true,
				message: 'add staff success',
			}

			return response.json(responseData)
		} catch (error) {
			return response.status(400).send({
				success: false,
				statusCode: 400,
				error: `エラーを発生しました： ${error}`
			})
		}
	}

	async changeStaff({ params, request, response }) {
		try {
			const data = request.body;

			await SdSyoKoteiMainSyain.query().where({
				koteikeikaku_no: data.current.koteikeikaku_no,
				seihin_code: data.current.seihin_code,
				koutei_code: data.current.koutei_code,
				seq: data.current.seq,
				syain_id: data.staff,
				syo_koutei_code: data.current.syo_koutei_code,
				syo_koutei_seq: data.current.syo_koutei_seq,
				syo_koutei_junjo: data.current.syo_koutei_junjo
			}).update({
        koteikeikaku_no: data.new.koteikeikaku_no,
				seihin_code: data.new.seihin_code,
				koutei_code: data.new.koutei_code,
				seq: data.new.seq,
				syo_koutei_code: data.new.syo_koutei_code,
				syo_koutei_seq: data.new.syo_koutei_seq,
				syo_koutei_junjo: data.new.syo_koutei_junjo,
				proc_from: data.new.proc_from,
				proc_to:  data.new.proc_to
      });

			let responseData = {
				success: true,
				message: 'Change staff success',
			}

			return response.json(responseData)
		} catch (error) {
			return response.status(400).send({
				success: false,
				statusCode: 400,
				error: `エラーを発生しました： ${error}`
			})
		}
	}
}

module.exports = StaffFreeController
