"use strict";
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Util = require("../../helpers/Util");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSyokouteiSetGannts
 */
class Sm_SyokouteiSetGanntController {
  /**
   * Show a list of all SmSyokouteiSetGannts.
   * GET SmSyokouteiSetGannts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /s-m-syokoutei-set-gannt/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API list SmSyokouteiSetGannt
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmSyokouteiSetGannt.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      if (!isEmpty(params.koutei_code)) {
        query.where("koutei_code", "LIKE", "%" + params.koutei_code + "%");
      }
      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmSyokouteiSetGannt.
   * GET SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /s-m-syokoutei-set-gannt/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_code
   *         description: page
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "required|integer",
        syo_koutei_junjo: "required|integer",
        syo_koutei_syurui_seq: "required|integer",
        syo_koutei_koho_seq: "required|integer",

      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SmSyokouteiSetGannt.query()
        .where({ 
          seihin_code: data.seihin_code, 
          koutei_code: data.koutei_code,
          syo_koutei_code: data.syo_koutei_code,
          syo_koutei_seq: data.syo_koutei_seq,
          syo_koutei_junjo: data.syo_koutei_junjo,
          syo_koutei_syurui_seq: data.syo_koutei_syurui_seq,
          syo_koutei_koho_seq: data.syo_koutei_koho_seq
        }).fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new SmSyokouteiSetGannt.
   * POST SmSyokouteiSetGannts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /s-m-syokoutei-set-gannt/create:
   *   post:
   *     description: Api create SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "integer",
        main_sub: "string",
        proc_from: "integer",
        proc_to: "integer",
        std_proc_syohin_su: "integer",
        proc_sokudo: "integer",
        syo_koutei_junjo: "required|integer",
        color: "string",
        syo_koutei_syurui_seq: "required|integer",
        syo_koutei_koho_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      
      let res = await SmSyokouteiSetGannt.create(data);
      let responseData = {
        success: true,
        message: "create success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SmSyokouteiSetGannt details.
   * PUT or PATCH SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /s-m-syokoutei-set-gannt/update:
   *   post:
   *     description: Api update SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer",
        seihin_code: "string|max:20",
        koutei_code: "string|max:20",
        syo_koutei_code: "string|max:20",
        syo_koutei_seq: "integer",
        main_sub: "string",
        proc_from: "integer",
        proc_to: "integer",
        std_proc_syohin_su: "integer",
        proc_sokudo: "integer",
        syo_koutei_junjo: "required|integer",
        color: "string",
        syo_koutei_syurui_seq: "required|integer",
        syo_koutei_koho_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .update(data);
      const res = await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Create/save a new sdsyokoteimaingannt.
   * POST smsyokouteisets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * Delete a SmSyokouteiSetGannt with id.
   * DELETE SmSyokouteiSetGannts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /s-m-syokoutei-set-gannt/delete:
   *   post:
   *     description: Api delete SmSyokouteiSetGannt
   *     security:
   *     tags:
   *       - SmSyokouteiSetGannt
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSetGanntDelete
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSetGanntDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      await SmSyokouteiSetGannt.query()
        .where({
          seq: data.seq
        })
        .delete();
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = Sm_SyokouteiSetGanntController;
