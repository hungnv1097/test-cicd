"use strict";
const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");

const SmSagyoMst = use("App/Models/SmSagyoMst");
const SmSouchiMst = use("App/Models/SmSouchiMst");

const { validateAll } = use("Validator");
const isEmpty = require("lodash.isempty");
const Redis = use("Redis");
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with SmSyokouteiSyoLists
 */
class SmSyokouteiSyoListController {
  /**
   * Show a list of all SmSyokouteiSyoLists.
   * GET SmSyokouteiSyoLists
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/list:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API list SmSyokouteiSyoList
   *     parameters:
   *       - name: seihin_code
   *         description: seihin_code
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: koutei_code
   *         in: query
   *         type: string
   *       - name: page
   *         description: page
   *         in: query
   *         type: integer
   *       - name: perPage
   *         description: perPage
   *         in: query
   *         type: integer
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              pagination:
   *                  type: object
   *                  $ref: '#/definitions/Pagination'
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async list({ request, response, view }) {
    try {
      let params = request.all();
      const page = !isEmpty(params.page) ? parseInt(params.page) : 1;
      const perPage = !isEmpty(params.perPage) ? parseInt(params.perPage) : 20;

      const query = SmSyokouteiSyoList.query();
      if (!isEmpty(params.seihin_code)) {
        query.where("seihin_code", "LIKE", "%" + params.seihin_code + "%");
      }
      if (!isEmpty(params.koutei_code)) {
        query.where("koutei_code", "LIKE", "%" + params.koutei_code + "%");
      }
      let data = await query.paginate(page, perPage);

      let res = {
        success: true,
        pagination: { ...data.pages, total: parseInt(data.pages.total) },
        data: data.rows
      };
      return response.json(res);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Display a single SmSyokouteiSyoList.
   * GET SmSyokouteiSyoLists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/detail:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *       - name: seihin_code
   *         description: page
   *         in: query
   *         type: string
   *       - name: koutei_code
   *         description: perPage
   *         in: query
   *         type: string
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async detail({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required",
        koutei_code: "required"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SmSyokouteiSyoList.query()
        .where({ seihin_code: data.seihin_code, koutei_code: data.koutei_code })
        .fetch();
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
  /**
   * Display a single SmSyokouteiSyoList.
   * GET SmSyokouteiSyoLists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/detail2:
   *   get:
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */

  async detail2({ params, request, response, view }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = (
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_seq: data.syo_koutei_seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];
      let responseData = {
        success: true,
        message: "success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
  /**
   * Create/save a new SmSyokouteiSyoList.
   * POST SmSyokouteiSyoLists
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/create:
   *   post:
   *     description: Api create SmSyokouteiSyoList
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSyoListCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSyoListCreate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async create({ request, response }) {
    try {
      const data = request.only(Object.keys(SmSyokouteiSyoList.rules));
      const validation = await validateAll(data, SmSyokouteiSyoList.rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      let res = await SmSyokouteiSyoList.create(data);
      let responseData = {
        success: true,
        message: "create success",
        data: res
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Update SmSyokouteiSyoList details.
   * PUT or PATCH SmSyokouteiSyoLists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/update:
   *   post:
   *     description: Api update SmSyokouteiSyoList
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSyoListCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSyoListUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async update({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "string|max:20",
        koutei_code: "string|max:20",
        syo_koutei_code: "string|max:20",
        syo_koutei_seq: "integer",
        seq: "required|integer",
        sagyo_code: "string",
        souchi_code: "string"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      await SmSyokouteiSyoList.query()
        .where({
          seq: data.seq
        })
        .update(data);
      const res = await SmSyokouteiSyoList.query()
        .where({
          seq: data.seq
        })
        .fetch();

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };

      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Upsert souchi code, saygo code.
   * POST
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/upsert-souchi-code-saygo-code:
   *   post:
   *     description: Api update SmSyokouteiSyoList
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSyoListCreate
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSyoListUpdate'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *           properties:
   *              data:
   *                  type: object
   *                  $ref: '#/definitions/SmSyokouteiSyoListResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async upsertSouchiCodeAndSaygoCode({ params, request, response }) {
    try {
      let rules = {
        seihin_code: "required|string|max:20",
        koutei_code: "required|string|max:20",
        syo_koutei_code: "required|string|max:20",
        syo_koutei_seq: "required|integer",
        sagyo_code: "string",
        souchi_code: "string",
        syo_koutei_koho_seq: "required|integer",
        syo_koutei_syurui_seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }

      if (data.sagyo_code) {
        const existMst = (
          await SmSagyoMst.query()
            .where({
              sagyo_code: data.sagyo_code
            })
            .limit(1)
            .fetch()
        ).toJSON();
        if (!existMst.length) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Saygo code not exist in Master`
          });
        }
      }

      if (data.souchi_code) {
        const existMst = (
          await SmSouchiMst.query()
            .where({
              souchi_code: data.souchi_code
            })
            .limit(1)
            .fetch()
        ).toJSON();
        if (!existMst.length) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            error: `Souchi code not exist in Master`
          });
        }
      }

      let existSyoKouteiSyo = (
        await SmSyokouteiSyoList.query()
          .where({
            seihin_code: data.seihin_code,
            koutei_code: data.koutei_code,
            syo_koutei_code: data.syo_koutei_code,
            syo_koutei_seq: data.syo_koutei_seq,
            syo_koutei_koho_seq: data.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: data.syo_koutei_syurui_seq
          })
          .limit(1)
          .fetch()
      ).toJSON()[0];

      if (!existSyoKouteiSyo) {
        existSyoKouteiSyo = await SmSyokouteiSyoList.create(data);
      } else {
        await SmSyokouteiSyoList.query()
          .where({
            seq: existSyoKouteiSyo.seq
          })
          .update(data);
      }

      const res = (
        await SmSyokouteiSyoList.query()
          .where({
            seq: existSyoKouteiSyo.seq
          })
          .fetch()
      ).toJSON()[0];

      let responseData = {
        success: true,
        message: "データを変更しました",
        data: res
      };

      return response.json(responseData);
    } catch (error) {
      console.error(error + "");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }

  /**
   * Delete a SmSyokouteiSyoList with id.
   * DELETE SmSyokouteiSyoLists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * @swagger
   * /sm-syokoutei-syo-list/delete:
   *   post:
   *     description: Api delete SmSyokouteiSyoList
   *     security:
   *     tags:
   *       - SmSyokouteiSyoList
   *     summary: Sample API
   *     parameters:
   *        - in: body
   *          name: SmSyokouteiSyoListDelete
   *          schema:
   *            $ref: '#/definitions/SmSyokouteiSyoListDelete'
   *     responses:
   *       200:
   *         description: Success result
   *         schema:
   *           $ref: '#/definitions/SuccessResponse'
   *       401:
   *         description: Failed result
   *         schema:
   *           $ref: '#/definitions/ErrorResponse'
   *
   */
  async delete({ params, request, response }) {
    try {
      let rules = {
        seq: "required|integer"
      };
      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);
      if (validation.fails()) {
        let responseData = {
          success: false,
          message: validation.messages()[0].message,
          statusCode: 501,
          errors: validation.messages()
        };
        return response.json(responseData);
      }
      await SmSyokouteiSyoList.query()
        .where({
          seq: data.seq
        })
        .delete();
      let responseData = {
        success: true,
        message: "削除しました。"
      };
      return response.json(responseData);
    } catch (error) {
      console.error(error);
      return response.status(400).send({
        success: false,
        statusCode: 400,
        error: `エラーを発生しました： ${error}`
      });
    }
  }
}

module.exports = SmSyokouteiSyoListController;
