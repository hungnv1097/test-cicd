'use strict'

const { Command } = require('@adonisjs/ace');
const StSeisansiji = use('App/Models/StSeisansiji')
const SmSeihinMst = use('App/Models/SmSeihinMst')
const SmKouteiMst = use('App/Models/SmKouteiMst')
const SmKouteiSet = use('App/Models/SmKouteiSet')
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet')
const SmSyokouteiSyoList = use('App/Models/SmSyokouteiSyoList')
const SdSyoKoteiDenpyo = use('App/Models/SdSyoKoteiDenpyo')
const SsKihonSetting = use('App/Models/SsKihonSetting')
const SdSyoKoteiMainGannt = use('App/Models/SdSyoKoteiMainGannt')
const SdSyoKoteiMainGanntJ = use('App/Models/SdSyoKoteiMainGanntJ')

const Excel = require('exceljs');
const moment = require('moment');
const Util = use('App/helpers/Util')


class SeisansijiExport extends Command {
  static get signature () {
    return `seisansiji:export 
            {start: Start date}
            {end: End date}
    `
  }

  static get description () {
    return 'Test export seisansiji to excel'
  }

  static async handle (args, options) {
    // this.info('Dummy implementation for seisansiji:export command');

    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('Seisansiji');
    worksheet.columns = [
      { header: '製品名 製品コード 伝票番号', key: 'product', width: 20 },
      { header: '開始日時 納期', key: 'start_time', width: 20 },
      { header: '数量', key: 'suryo', width: 10 },
      { header: '工程名 工程コード', key: 'koutei', width: 20 },
      { header: '小工程名 小工程コード', key: 'syokoutei', width: 20 },
      { header: '担当者', key: 'syo_kotei_enpyo', width: 20 },
      { header: '装置名 承知コード', key: 'souchi', width: 20 },
      { header: '準備時間  固定時間  後処理時間', key: 'process_time', width: 20 },
    ];
    //Add header for range gannt
    let start_date = new Date(args.start).getTime() / 1000;
    const gannt_ranges = await SeisansijiExport.fillRangeHeader(worksheet, start_date);
    //Format header
    worksheet.getRow(1).eachCell((cell) => {
      cell.font = { bold: true };
      cell.alignment = {vertical: 'top', horizontal: 'left', wrapText: true}
    });
    // start-date <= query.start-date <= end-date | start-date <= query.end-date <= end-date
    const query = StSeisansiji.query();
    query.whereRaw('(start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime <= ? and nouki_datetime >= ?) OR (start_datetime >= ? and nouki_datetime <= ?)', [args.start, args.start, args.end, args.end, args.start, args.end]);
    // query.where('start_datetime', '>=', args.start);
    // query.where('nouki_datetime', '<=', args.end);
    const items = await query.with('seihin', (builder) => {
      builder.with('s_m_koutei_list')
    }).fetch();

    let num_row = 2;
    for (let item of items.toJSON()) {
      let first_row = num_row;
      let product = item.seihin.seihin_name + "\r\n" + item.seihin.seihin_code + "\r\n" + item.koteikeikaku_no;
      let start_time = moment(item.start_datetime).format('YYYY-MM-DD HH:mm') + "\r\n" + moment(item.nouki_datetime).format('YYYY-MM-DD HH:mm');
      let suryo = item.suryo;

      for (let s_m_koutei of item.seihin.s_m_koutei_list){
        let koutei_first_row = num_row;
        let koutei = s_m_koutei.koutei_code + "\n" + s_m_koutei["kotei-name"];
        for(let s_m_syokoutei of s_m_koutei["s-m-syokoutei-list"]){
          let syokoutei = s_m_syokoutei.syo_koutei.syo_kotei_name + "\r\n" + s_m_syokoutei.syo_koutei.syo_koutei_code;
          //Get SdSyoKoteiDenpyo
          const sdSyoKoteiDenpyo =  await SdSyoKoteiDenpyo.query()
            .where({
              'seihin_code': s_m_syokoutei.seihin_code,
              'koteikeikaku_no': item.koteikeikaku_no,
              'koutei_code': s_m_syokoutei.koutei_code,
              'syo_koutei_code': s_m_syokoutei.syo_koutei_code,
              'syo_koutei_seq': s_m_syokoutei.syo_koutei_seq,
              'syo_koutei_junjo': s_m_syokoutei.syo_koutei_junjo
            })
            .first();
          let syo_kotei_enpyo = "";
          if(sdSyoKoteiDenpyo){
            syo_kotei_enpyo = (sdSyoKoteiDenpyo.tantosya)?sdSyoKoteiDenpyo.tantosya:""
          }
          //Get time process
          let process_time = (s_m_syokoutei.mae_setup_time)?s_m_syokoutei.mae_setup_time:"" + "\r\n"
            + (s_m_syokoutei.std_setup_time)?s_m_syokoutei.std_setup_time:"" + "\r\n"
            + (s_m_syokoutei.ato_setup_time)?s_m_syokoutei.ato_setup_time:"";
          //Get souchi
          let souchi = "";
          for (let syo of s_m_syokoutei["s-m-syokoutei-syo-list"]){
            souchi += syo.souchi.souchi_name + "/" + syo.souchi.souchi_code + "\r\n";
          }
          //Fill row
          let row_data = worksheet.getRow(num_row);
          row_data.values = {syokoutei: syokoutei,syo_kotei_enpyo:syo_kotei_enpyo, souchi:souchi, process_time:process_time};
          // worksheet.addRow({syokoutei: syokoutei,syo_kotei_enpyo:syo_kotei_enpyo, souchi:souchi, process_time:process_time});
          row_data.eachCell((cell) => {
            cell.font = { bold: false };
            cell.alignment = {vertical: 'middle', horizontal: 'left', wrapText: true}
          });

          //Fill gannt
          const mainGannts =  await SdSyoKoteiMainGanntJ.query() //TODO Change to main gain
            .where({
              'seihin_code': s_m_syokoutei.seihin_code,
              'koteikeikaku_no': item.koteikeikaku_no,
              'koutei_code': s_m_syokoutei.koutei_code,
              'syo_koutei_code': s_m_syokoutei.syo_koutei_code,
              'syo_koutei_seq': s_m_syokoutei.syo_koutei_seq,
              'syo_koutei_junjo': s_m_syokoutei.syo_koutei_junjo
            }).fetch();
          for (let gannt_instance of mainGannts.toJSON()){
            await SeisansijiExport.fillGannt(worksheet, num_row, gannt_instance, gannt_ranges);
          }
          num_row++;
        }
        let koutei_last_row = num_row - 1;
        if(koutei_last_row > koutei_first_row){
          //Merge cell koutei
          worksheet.mergeCells('D' + koutei_first_row + ':' + 'D' + koutei_last_row);
        }
        worksheet.getCell('D' + koutei_first_row).value = koutei;
        worksheet.getCell('D' + koutei_first_row).font = { bold: false };
        worksheet.getCell('D' + koutei_first_row).alignment = {vertical: 'middle', horizontal: 'left', wrapText: true}
      }
      let last_row = num_row - 1;
      if(last_row > first_row){
        //Merge product
        worksheet.mergeCells('A' + first_row + ':' + 'A' + last_row);
        //Merge Time
        worksheet.mergeCells('B' + first_row + ':' + 'B' + last_row);
        //Merge number
        worksheet.mergeCells('C' + first_row + ':' + 'C' + last_row);
      }
      worksheet.getCell('A' + first_row).value = product;
      worksheet.getCell('A' + first_row).font = { bold: false };
      worksheet.getCell('A' + first_row).alignment = {vertical: 'middle', horizontal: 'left', wrapText: true}

      worksheet.getCell('B' + first_row).value = start_time;
      worksheet.getCell('B' + first_row).font = { bold: false };
      worksheet.getCell('B' + first_row).alignment = {vertical: 'middle', horizontal: 'left', wrapText: true}

      worksheet.getCell('C' + first_row).value = suryo;
      worksheet.getCell('C' + first_row).font = { bold: false };
      worksheet.getCell('C' + first_row).alignment = {vertical: 'middle', horizontal: 'left', wrapText: true}
    }
    let path_file = '/uploads/seisansiji-report'+moment()+'.xlsx'
    await workbook.xlsx.writeFile('public'+path_file);
    return path_file
  }

  static async fillRangeHeader(worksheet, start_date) {
    //Get range need created
    const ss_kihon_setting = await SsKihonSetting.query().first();
    let ranges = [];
    let start_column = 9;
    let row_header = worksheet.getRow(1);

    if(ss_kihon_setting.default_dai_chu_syo_flag == 'd'){
      //Step with month
      let start_date_on_range = start_date;
      let max_range = ss_kihon_setting.disp_scall_d_hani/ss_kihon_setting.disp_scall_d_tani;
      for (let i = 0; i < max_range; i++){
        //Each range has 4 column
        let start_date_on_step = start_date_on_range;
        for (let j = 0; j < 4; j++){
          let min_time = start_date_on_range;
          let max_time = min_time + 3600*24*Util.daysInMonthFromTimestamp(start_date_on_range)/4;
          ranges.push({
            min_time:min_time,
            max_time:max_time,
            column: start_column + j,
            step_time: 3600*24*Util.daysInMonthFromTimestamp(start_date_on_range)/4
          });
          start_date_on_range = max_time;
          worksheet.getColumn(start_column+j).width = 5;
        }
        //Create header with start of range
        // merge by top-left, bottom-right
        worksheet.mergeCells(1,start_column,1,start_column + 3);
        row_header.getCell(start_column).value = moment.unix(start_date_on_step).format("MM/YYYY");
        row_header.getCell(start_column).alignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
        start_column +=4;
      }
    }else if (ss_kihon_setting.default_dai_chu_syo_flag == 'c'){
      //Step with day
      let start_date_on_range = start_date;
      let max_range = ss_kihon_setting.disp_scall_c_hani/ss_kihon_setting.disp_scall_c_tani;
      let step_time = ss_kihon_setting.disp_scall_c_tani*3600*24; //Convert day to seconds
      for (let i = 0; i < max_range; i++){
        //Each range has 4 column
        let start_date_on_step = start_date_on_range;
        for (let j = 0; j < 4; j++){
          let min_time = start_date_on_range;
          let max_time = min_time + step_time/4;
          ranges.push({
            min_time:min_time,
            max_time:max_time,
            column: start_column + j,
            step_time: step_time/4
          });
          start_date_on_range = max_time;
          worksheet.getColumn(start_column+j).width = 5;
        }
        //Create header with start of range
        // merge by top-left, bottom-right
        worksheet.mergeCells(1,start_column,1,start_column + 3);
        row_header.getCell(start_column).value = moment.unix(start_date_on_step).format("DD/MM/YYYY");
        row_header.getCell(start_column).alignment = {vertical: 'middle', horizontal: 'center', wrapText: true}
        start_column +=4;
      }
    }else{
      //Step with min
      let start_date_on_range = start_date;
      let max_range = ss_kihon_setting.disp_scall_s_hani*60/ss_kihon_setting.disp_scall_s_tani;
      let step_time = ss_kihon_setting.disp_scall_s_tani*60; //Convert min to seconds
      for (let i = 0; i < max_range; i++){
        //Each range has 4 column
        let start_date_on_step = start_date_on_range;
        for (let j = 0; j < 4; j++){
          let min_time = start_date_on_range;
          let max_time = min_time + step_time/4;
          ranges.push({
            min_time:min_time,
            max_time:max_time,
            column: start_column + j,
            step_time: step_time/4
          });
          start_date_on_range = max_time;
          worksheet.getColumn(start_column+j).width = 5;
        }
        //Create header with start of range
        // merge by top-left, bottom-right
        worksheet.mergeCells(1,start_column,1,start_column + 3);
        row_header.getCell(start_column).value = moment.unix(start_date_on_step).format("HH:mm");
        row_header.getCell(start_column).alignment = {vertical: 'middle', horizontal: 'center', wrapText: true}
        start_column +=4;
      }
    }
    return ranges;
  }

  static async fillGannt(worksheet, num_row, gannt_instance, gannt_ranges) {
    let gannt_from_time = new Date(gannt_instance.proc_from).getTime() / 1000;
    let gannt_to_time = new Date(gannt_instance.proc_to).getTime() / 1000;
    let time_valid = gannt_from_time;
    for (let gannt_range of gannt_ranges){
      if(gannt_from_time <= gannt_range.min_time){
        time_valid += gannt_range.step_time;
        if(time_valid <= gannt_range.max_time && time_valid >= gannt_range.min_time){
          //Add row
          let row_data = worksheet.getRow(num_row);
          row_data.getCell(gannt_range.column).value = "x";
          row_data.getCell(gannt_range.column).font = { bold: false };
          row_data.getCell(gannt_range.column).alignment = {vertical: 'middle', horizontal: 'center'}
        }
      }
      if(gannt_to_time < gannt_range.max_time){
        break;
      }
    }
  }
}

module.exports = SeisansijiExport;
