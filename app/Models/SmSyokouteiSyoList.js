'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Redis = use('Redis')

class SmSyokouteiSyoList extends Model {
  static get table () {
    return 's_m_syokoutei_syo_list'
  }

  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(smSyokouteiSyoListInstance) => {
      let lastSeq = await SmSyokouteiSyoList.query().getMax('seq')
      smSyokouteiSyoListInstance.seq = lastSeq + 1
    })

    this.addHook('afterSave', async(smSyokouteiSyoListInstance) => {
      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
    })

    this.addHook('afterDelete', async(smSyokouteiSyoListInstance) => {
      Redis.keys("SmSyokouteiSyoList_*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      seihin_code: 'required|string|max:20',
      koutei_code: 'required|string|max:20',
      syo_koutei_code: 'required|string|max:20',
      syo_koutei_seq: 'required|integer',
      sagyo_code: 'string',
      souchi_code: 'string'
    }
  }
  /**
   * @swagger
   * definition:
   *   SmSyokouteiSyoListCreate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       sagyo_code:
   *         type: string
   *       souchi_code:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSyoListUpdate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       sagyo_code:
   *         type: string
   *       souchi_code:
   *         type: string
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSyoListDelete:
   *     properties:
   *       seq:
   *         type: integer
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSyoListResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       sagyo_code:
   *         type: string
   *       souchi_code:
   *         type: string
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSyoListMainResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       sagyo:
   *         type: object
   *         $ref: '#/definitions/SmSagyoMstResponse'
   *       souchi:
   *         type: object
   *         $ref: '#/definitions/SmSouchiMstResponse'
   */
  sagyo () {
    return this.hasOne('App/Models/SmSagyoMst', 'sagyo_code', 'sagyo_code')
  }

  souchi () {
    return this.hasOne('App/Models/SmSouchiMst', 'souchi_code', 'souchi_code')
  }
}

module.exports = SmSyokouteiSyoList
