'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SdSyoKoteiDenpyo extends Model {
  static get table () {
    return 's_d_syokoutei_denpyo'
  }

  static boot() {
    super.boot()
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      koteikeikaku_no: 'required|string|max:20',
      seihin_code: 'required|string|max:20',
      koutei_code: 'required|string|max:20',
      syo_koutei_code: 'required|string|max:20',
      syo_koutei_seq: 'required|integer',
      syo_koutei_junjo: 'required|integer',
      tantosya: 'string',
    }
  }

  static get rulesUpdate () {
    return {
      koteikeikaku_no: 'string|max:20',
      seihin_code: 'string|max:20',
      koutei_code: 'string|max:20',
      syo_koutei_code: 'string|max:20',
      syo_koutei_seq: 'integer',
      syo_koutei_junjo: 'integer',
      tantosya: 'string',
    }
  }

  static get rulesDelete () {
    return {
      koteikeikaku_no: 'string|max:20',
      seihin_code: 'string|max:20',
      koutei_code: 'string|max:20',
      syo_koutei_code: 'string|max:20',
      syo_koutei_seq: 'integer',
      syo_koutei_junjo: 'integer'
    }
  }

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiDenpyoCreate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       tantosya:
   *         type: string
   *     required:
   *       - koteikeikaku_no
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - syo_koutei_junjo
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiDenpyoUpdate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       tantosya:
   *         type: string
   *     required:
   *       - koteikeikaku_no
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - syo_koutei_junjo
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiDenpyoDelete:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *     required:
   *       - koteikeikaku_no
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - syo_koutei_junjo
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiDenpyoResponse:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       tantosya:
   *         type: string
   */
}

module.exports = SdSyoKoteiDenpyo
