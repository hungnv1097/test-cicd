'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const SmHanKoutei = use('App/Models/SmHanKoutei')
class SmHan extends Model {
  static get table () {
    return 's_m_han'
  }

  static get updatedAtColumn () {
    return ''
  }

  static get computed () {
    return ['smHanKouteis']
  }

  static boot() {
    super.boot()
    this.addHook('afterPaginate', async(smHanInstances) => {
      for (let smHanInstance of smHanInstances) {
        const hanKouteis =  await SmHanKoutei.query()
          .where({'han_name': smHanInstance.han_name})
          .fetch()
        smHanInstance.$sideLoaded.smHanKouteis = hanKouteis
      }
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmHanLogin:
   *     properties:
   *       han_name:
   *         type: string
   *       pass:
   *         type: string
   *     required:
   *       - han_name
   *       - pass
   */

  /**
   * @swagger
   * definition:
   *   SmHanCreate:
   *     properties:
   *       han_name:
   *         type: string
   *       pass:
   *         type: string
   *       koutei_codes:
   *         type: array
   *         items:
   *            type: string
   *     required:
   *       - han_name
   */

  /**
   * @swagger
   * definition:
   *   SmHanUpdate:
   *     properties:
   *       han_name:
   *         type: string
   *       pass:
   *         type: string
   *       data_update:
   *         type: object
   *     required:
   *       - han_name
   *       - pass
   */

  /**
   * @swagger
   * definition:
   *   SmHanDelete:
   *     properties:
   *       han_name:
   *         type: string
   *     required:
   *       - han_name
   */

  /**
   * @swagger
   * definition:
   *   SmHanResponse:
   *     properties:
   *       han_name:
   *         type: string
   *       pass:
   *         type: string
   *       smHanKouteis:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmHanKouteiResponse'
   */
}

module.exports = SmHan
