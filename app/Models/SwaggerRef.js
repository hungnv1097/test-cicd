'use strict'
/**
 * @swagger
 * definition:
 *   Pagination:
 *     type: object
 *     properties:
 *       total:
 *         type: integer
 *       perPage:
 *         type: integer
 *       page:
 *         type: integer
 */

/**
 * @swagger
 * definition:
 *   SuccessResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *       message:
 *         type: string
 */

/**
 * @swagger
 * definition:
 *   ErrorResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *       message:
 *         type: string
 *       statusCode:
 *          type: integer
 *       errors:
 *         type: object
 */

/**
 * @swagger
 * definition:
 *   MenuResponse:
 *     type: object
 *     properties:
 *       jikan_tanni:
 *         type: string
 *       dataColRes:
 *         type: object
 */
