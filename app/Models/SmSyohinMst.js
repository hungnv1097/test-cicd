'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')}
 *
 * Master Product
 * */
const Model = use('Model')

class SmSyohinMst extends Model {
  static get table () {
    return 's_m_syohin_mst'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSyohinMstCreate:
   *     properties:
   *       syohin_code:
   *         type: string
   *       syohin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *     required:
   *       - syohin_name
   */

  /**
   * @swagger
   * definition:
   *   SmSyohinMstUpdate:
   *     properties:
   *       syohin_code:
   *         type: string
   *       syohin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *     required:
   *       - syohin_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyohinMstDelete:
   *     properties:
   *       syohin_code:
   *         type: string
   *     required:
   *       - syohin_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyohinMstResponse:
   *     type: object
   *     properties:
   *       syohin_code:
   *         type: string
   *       syohin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *
   *
   */


}

module.exports = SmSyohinMst
