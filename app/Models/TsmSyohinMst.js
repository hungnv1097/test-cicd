'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TsmSyohinMst extends Model {
  static get table() {
    return 't_s_m_syohin_mst'
  }
  static get primaryKey () {
    return 'seihin_id'
  }
  static get createdAtColumn () {
    return null
  }
  static get updatedAtColumn () {
    return 'update_date'
  }
}

module.exports = TsmSyohinMst
