'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
/**
 * Production instruction data
 */
const Model = use('Model')
const Logger = use('Logger')
const Util = use('App/helpers/Util')
const isEmpty = require('lodash.isempty')
const SdSyoKoteiMainGannt = use('App/Models/SdSyoKoteiMainGannt')
const SdSyoKoteiMainGanntJ = use('App/Models/SdSyoKoteiMainGanntJ')

const SdSyoKoteiDenpyo = use('App/Models/SdSyoKoteiDenpyo')

// const Redis = use('Redis')

class StSeisansiji extends Model {
  static get table() {
    return 's_t_seisansiji'
  }
  static get computed () {
    return ['s-d-syo-kotei-main-gaint-list', 's-d-syo-kotei-main-gaint-j-list', 's-d-syo-koutei-denpyo-list']
  }

  static boot() {
    super.boot()
    let i = 0;
    this.addHook('beforeCreate', async(stSeisansijiInstance) => {
      if(isEmpty(stSeisansijiInstance.koteikeikaku_no)){
        let koteikeikaku_no = Util.randomStr(10)
        stSeisansijiInstance.koteikeikaku_no = koteikeikaku_no
      }
      stSeisansijiInstance = Util.formatTime(stSeisansijiInstance);
      let lastSeq = await StSeisansiji.query().where({'koteikeikaku_no': stSeisansijiInstance.koteikeikaku_no}).getMax('koteikeikaku_seq');
      i++;
      stSeisansijiInstance.koteikeikaku_seq = lastSeq + i;
    })

    this.addHook('afterPaginate', async(stSeisansijiInstances) => {
      for (let stSeisansijiInstance of stSeisansijiInstances) {
        const mainGannts =  await SdSyoKoteiMainGannt.query()
          .where({'seihin_code': stSeisansijiInstance.seihin_code, 'koteikeikaku_no': stSeisansijiInstance.koteikeikaku_no})
          .fetch()
        stSeisansijiInstance.$sideLoaded.mainGannts = mainGannts

        const mainGanntJs =  await SdSyoKoteiMainGanntJ.query()
          .where({'seihin_code': stSeisansijiInstance.seihin_code, 'koteikeikaku_no': stSeisansijiInstance.koteikeikaku_no})
          .fetch()
        stSeisansijiInstance.$sideLoaded.mainGanntJs = mainGanntJs

        const sdSyoKoteiDenpyos =  await SdSyoKoteiDenpyo.query()
          .where({'seihin_code': stSeisansijiInstance.seihin_code, 'koteikeikaku_no': stSeisansijiInstance.koteikeikaku_no})
          .fetch()

        stSeisansijiInstance.$sideLoaded.sdSyoKoteiDenpyos = sdSyoKoteiDenpyos
      }
    })

    // this.addHook('afterSave', async(stSeisansijiInstance) => {
    //   Redis.keys("StSeisansiji_*", async function(err, rows) {
    //     for (var i in rows){
    //       await Redis.del(rows[i])
    //     }
    //   })
    // })
    //
    // this.addHook('afterDelete', async(SmBefaftGroupInstance) => {
    //   Redis.keys("StSeisansiji_*", async function(err, rows) {
    //     for (var i in rows){
    //       await Redis.del(rows[i])
    //     }
    //   })
    // })

  }

  static get primaryKey() {
    return null
  }

  // static get createdAtColumn() {
  //   return null
  // }
  // static get updatedAtColumn() {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   StSeisansijiCreate:
   *     properties:
   *       start_datetime:
   *         type: string
   *       koteikeikaku_no:
   *         type: string
   *       suryo:
   *         type: integer
   *       seihin_code:
   *         type: string
   *       nouki_datetime:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *       kado_jikangai_flag:
   *         type: boolean
   *     required:
   *       - seihin_code
   */


  /**
   * @swagger
   * definition:
   *   StSeisansijiCopy:
   *     properties:
   *       start_datetime:
   *         type: string
   *       suryo:
   *         type: integer
   *       seihin_code:
   *         type: string
   *       nouki_datetime:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *       kado_jikangai_flag:
   *         type: boolean
   *       s_m_koutei_list:
   *         type: array
   *     required:
   *       - seihin_code
   */

  /**
   * @swagger
   * definition:
   *   StSeisansijiUpdate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       koteikeikaku_seq:
   *         type: string
   *       start_datetime:
   *         type: string
   *       suryo:
   *         type: integer
   *       seihin_code:
   *         type: string
   *       nouki_datetime:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *       kado_jikangai_flag:
   *         type: boolean
   *     required:
   *       - koteikeikaku_no
   *       - koteikeikaku_seq
   */

  /**
   * @swagger
   * definition:
   *   StSeisansijiLock:
   *     properties:
   *       start_datetime:
   *         type: string
   *     required:
   *       - start_datetime
   */

  /**
   * @swagger
   * definition:
   *   StSeisansijiDelete:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       koteikeikaku_seq:
   *         type: string
   *     required:
   *       - koteikeikaku_no
   *       - koteikeikaku_seq
   */

  /**
   * @swagger
   * definition:
   *   StSeisansijiResponse:
   *     type: object
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       koteikeikaku_seq:
   *         type: string
   *       start_datetime:
   *         type: string
   *       suryo:
   *         type: integer
   *       nouki_datetime:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *       kado_jikangai_flag:
   *         type: boolean
   *       seihin:
   *         type: object
   *         $ref: '#/definitions/SmSeihinMstMainResponse'
   *       s-d-syo-kotei-main-gaint-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SdSyoKoteiMainGanntMainResponse'
   *       s-d-syo-kotei-main-gaint-j-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SdSyoKoteiMainGanntMainJResponse'
   *       s-d-syo-koutei-denpyo-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SdSyoKoteiDenpyoResponse'
   */
  seihin () {
    return this.hasOne('App/Models/SmSeihinMst', 'seihin_code', 'seihin_code')
  }

  getSDSyoKoteiMainGaintList () {
    return this.mainGannts
  }

  getSDSyoKoteiMainGaintJList () {
    return this.mainGanntJs
  }

  getSDSyoKoteiDenpyoList () {
    return this.sdSyoKoteiDenpyos
  }

  /**
   * @swagger
   * definition:
   *   StSeisansijiMapKoutei:
   *     properties:
   *       delete_all:
   *         type: integer
   *       seihin_code:
   *         type: string
   *       kado_jikangai_flag:
   *         type: boolean
   *       isCreateNew:
   *         type: boolean
   *       koutei_code:
   *         type: array
   *         items:
   *            type: string
   */


  /**
   * @swagger
   * definition:
   *   StSeisansijiMapKouteiResponse:
   *     type: object
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: tring
   */

}

module.exports = StSeisansiji
