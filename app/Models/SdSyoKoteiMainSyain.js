'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SdSyoKoteiMainSyain extends Model {
  static get table () {
    return 's_d_syo_kotei_main_syain'
  }
  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(sdSyoKoteiMainGanntInstance) => {
      // sdSyoKoteiMainGanntInstance.syo_koutei_seq = 0 // Fix this number, reverse using for future.
      let lastSeq = await SdSyoKoteiMainSyain.query().getMax('seq')
      sdSyoKoteiMainGanntInstance.seq = lastSeq + 1
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      koteikeikaku_no: 'required|string|max:20',
      seihin_code: 'required|string|max:20',
      koutei_code: 'required|string|max:20',
      syo_koutei_code: 'required|string|max:20',
      syo_koutei_seq: 'required|integer',
      syo_koutei_junjo: 'required|integer',
      syain_id: 'integer',
      proc_from: 'string',
      proc_to: 'string',
      freestaff_flag: 'boolean',
    }
  }

  static get rulesUpdate () {
    return {
      seq: 'required|integer',
      koteikeikaku_no: 'string|max:20',
      seihin_code: 'string|max:20',
      koutei_code: 'string|max:20',
      syo_koutei_code: 'string|max:20',
      syo_koutei_seq: 'integer',
      syo_koutei_junjo: 'integer',
      syain_id: 'integer',
      proc_from: 'string',
      proc_to: 'string',
      freestaff_flag: 'boolean',
    }
  }
  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainSyainCreate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       syain_id:
   *         type: integer
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       freestaff_flag:
   *         type: boolean
   *     required:
   *       - koteikeikaku_no
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - syo_koutei_junjo
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainSyainUpdate:
   *     properties:
   *       seq:
   *         type: integer
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       syain_id:
   *         type: integer
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       freestaff_flag:
   *         type: boolean
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainSyainDelete:
   *     properties:
   *       seq:
   *         type: integer
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainSyainResponse:
   *     properties:
   *       seq:
   *         type: integer
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       syain_id:
   *         type: integer
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       freestaff_flag:
   *         type: boolean
   */
}

module.exports = SdSyoKoteiMainSyain
