'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmBefaftGroupSyosai extends Model {
  static get table () {
    return 's_m_befaft_group_syosai'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupSyosaiCreate:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - befaft_group_id
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupSyosaiUpdate:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       data_update:
   *         type: object
   *     required:
   *       - befaft_group_id
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupSyosaiDelete:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - befaft_group_id
   *       - syo_koutei_code
   *       - seihin_code
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupSyosaiResponse:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   */
}

module.exports = SmBefaftGroupSyosai
