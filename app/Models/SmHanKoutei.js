'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmHanKoutei extends Model {
  static get table () {
    return 's_m_han_koutei'
  }

  static boot() {
    super.boot()
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmHanKouteiCreate:
   *     properties:
   *       han_name:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - han_name
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmHanKouteiUpdate:
   *     properties:
   *       han_name:
   *         type: string
   *       koutei_code:
   *         type: string
   *       data_update:
   *         type: object
   *     required:
   *       - han_name
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmHanKouteiDelete:
   *     properties:
   *       han_name:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - han_name
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmHanKouteiResponse:
   *     properties:
   *       han_name:
   *         type: string
   *       koutei_code:
   *         type: string
   */
}

module.exports = SmHanKoutei
