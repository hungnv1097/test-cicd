'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const SmKouteiMst = use('App/Models/SmKouteiMst');
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet');
const Redis = use('Redis')
const Util = use('App/helpers/Util')

class SmKouteiSet extends Model {
  static get table() {
    return 's_m_koutei_set'
  }

  static get computed() {
    return ['kotei-name', 's-m-syokoutei-list']
  }

  static boot() {
    super.boot()
    let i = 0;
    this.addHook('beforeCreate', async (SmKouteiSetInstance) => {
      let lastSeq = await SmKouteiSet.query().getMax('koutei_junjo');
      i++;
      SmKouteiSetInstance.koutei_junjo = lastSeq + i;
    })

    this.addHook('afterFetch', async (smKouteiSetInstances) => {
      for (let smKouteiSetInstance of smKouteiSetInstances) {
        let koutei;
        let key_cache = 'SmKouteiMst_' + smKouteiSetInstance.koutei_code
        const cachedData = await Redis.get(key_cache)
        let startTime = Date.now()
        if (cachedData) {
          koutei = JSON.parse(cachedData)
        } else {
          koutei = await SmKouteiMst.query()
            .where({
              'koutei_code': smKouteiSetInstance.koutei_code,
            })
            .first()
          await Redis.set(key_cache, JSON.stringify(koutei))
        }
        let endTime = Date.now()
        smKouteiSetInstance.$sideLoaded.kotei_name = koutei.kotei_name


        let syoKouteiList;
        let key_cache_syoKouteiList = 'SmSyokouteiSet_' + smKouteiSetInstance.seihin_code + '_' + smKouteiSetInstance.koutei_code
        const cachedData_syoKouteiList = await Redis.get(key_cache_syoKouteiList)

        let startTime1 = Date.now()
        if (cachedData_syoKouteiList) {
          syoKouteiList = JSON.parse(cachedData_syoKouteiList)
        } else {
          syoKouteiList = await SmSyokouteiSet.query().with('syo_koutei')
            .where({'seihin_code': smKouteiSetInstance.seihin_code, 'koutei_code': smKouteiSetInstance.koutei_code})
            .fetch()
          await Redis.set(key_cache_syoKouteiList, JSON.stringify(syoKouteiList))
          syoKouteiList = await Redis.get(key_cache_syoKouteiList);
          syoKouteiList = JSON.parse(syoKouteiList)
        }

        let endTime1 = Date.now()
        smKouteiSetInstance.$sideLoaded.smSyokouteiList = syoKouteiList
      }
    })
  }

  getKoteiName() {
    return this.kotei_name
  }

  getSMSyokouteiList() {
    return this.smSyokouteiList
  }

  static get primaryKey() {
    return null
  }

  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }
  /**
   * @swagger
   * definition:
   *   SmKouteiSetCreate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiSetUpdate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       koutei_junjo:
   *         type: integer
   *     required:
   *       - seihin_code
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiMstUpdateSetCreate:
   *     properties:
   *       koutei_code:
   *         type: string
   *       syo_koutei_set:
   *         type: array
   *         items:
   *            type: object
   *            $ref: '#/definitions/SmSyokouteiSetCreate'
   *     required:
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiSetMainResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       koutei_junjo:
   *         type: integer
   *       koutei_name:
   *         type: string
   *       s-m-syokoutei-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmSyokouteiSetMainResponse'
   *
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiSetDelete:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiSetResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       koutei_junjo:
   *         type: integer
   */
  s_m_syokoutei_syurui_list () {
    return this
      .hasMany('App/Models/SmSyokouteiSyurui', 'koutei_code', 'koutei_code')
  }
}

module.exports = SmKouteiSet
