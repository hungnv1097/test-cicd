'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsKihonSetting extends Model {
  static get table () {
    return 's_s_kihon_setting'
  }

  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(SsKihonSettingInstance) => {
      let lastSeq = await SsKihonSetting.query().getMax('default_workset_id')
      SsKihonSettingInstance.default_workset_id = lastSeq + 1
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      default_dai_chu_syo_flag: 'required|string|max:1',
      vu_staff_skill_flag: 'required|string|max:1',
      migiude_staff_skill_url: 'string|max:1024',
      kadojikoku_from: 'required|string|max:4',
      kadojikoku_to: 'required|string|max:4',
      kyukei1_from: 'string|max:4',
      kyukei1_to: 'string|max:4',
      kyukei2_from: 'string|max:4',
      kyukei2_to: 'string|max:4',
      kyukei3_from: 'string|max:4',
      kyukei3_to: 'string|max:4',
      disp_scall_d_tani: 'string|max:2',
      disp_scall_d_hani: 'integer',
      disp_scall_c_tani: 'string|max:2',
      disp_scall_c_hani: 'integer',
      disp_scall_s_tani: 'string|max:2',
      disp_scall_s_hani: 'integer',
      text_mode: 'boolean',
      course_type: 'string|max:2',
      direct_text_input_flag: 'boolean',
    }
  }

  static get rulesUpdate () {
    return {
      default_workset_id: 'required|integer',
      default_dai_chu_syo_flag: 'string|max:1',
      vu_staff_skill_flag: 'string|max:1',
      migiude_staff_skill_url: 'string|max:1024',
      kadojikoku_from: 'required|string|max:4',
      kadojikoku_to: 'required|string|max:4',
      kyukei1_from: 'string|max:4',
      kyukei1_to: 'string|max:4',
      kyukei2_from: 'string|max:4',
      kyukei2_to: 'string|max:4',
      kyukei3_from: 'string|max:4',
      kyukei3_to: 'string|max:4',
      disp_scall_d_tani: 'string|max:2',
      disp_scall_d_hani: 'integer',
      disp_scall_c_tani: 'string|max:2',
      disp_scall_c_hani: 'integer',
      disp_scall_s_tani: 'string|max:2',
      disp_scall_s_hani: 'integer',
      text_mode: 'boolean',
      course_type: 'string|max:2',
      direct_text_input_flag: 'boolean',
    }
  }

  /**
   * @swagger
   * definition:
   *   SsKihonSettingCreate:
   *     properties:
   *       default_dai_chu_syo_flag:
   *         type: string
   *       vu_staff_skill_flag:
   *         type: string
   *       migiude_staff_skill_url:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       disp_scall_d_tani:
   *         type: string
   *       disp_scall_d_hani:
   *         type: integer
   *       disp_scall_c_tani:
   *         type: string
   *       disp_scall_c_hani:
   *         type: integer
   *       disp_scall_s_tani:
   *         type: string
   *       disp_scall_s_hani:
   *         type: integer
   *       text_mode:
   *         type: boolean
   *       course_type:
   *         type: string
   *     required:
   *       - default_dai_chu_syo_flag
   *       - vu_staff_skill_flag
   *       - kadojikoku_from
   *       - kadojikoku_to
   */

  /**
   * @swagger
   * definition:
   *   SsKihonSettingUpdate:
   *     properties:
   *       default_workset_id:
   *         type: integer
   *       default_dai_chu_syo_flag:
   *         type: string
   *       vu_staff_skill_flag:
   *         type: string
   *       migiude_staff_skill_url:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       disp_scall_d_tani:
   *         type: string
   *       disp_scall_d_hani:
   *         type: integer
   *       disp_scall_c_tani:
   *         type: string
   *       disp_scall_c_hani:
   *         type: integer
   *       disp_scall_s_tani:
   *         type: string
   *       disp_scall_s_hani:
   *         type: integer
   *       text_mode:
   *         type: boolean
   *       course_type:
   *         type: string
   *     required:
   *       - default_workset_id
   */


  /**
   * @swagger
   * definition:
   *   SsKihonSettingDelete:
   *     properties:
   *       default_workset_id:
   *     required:
   *       - default_workset_id
   */


  /**
   * @swagger
   * definition:
   *   SsKihonSettingResponse:
   *     properties:
   *       default_workset_id:
   *         type: integer
   *       default_dai_chu_syo_flag:
   *         type: string
   *       vu_staff_skill_flag:
   *         type: string
   *       migiude_staff_skill_url:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       disp_scall_d_tani:
   *         type: string
   *       disp_scall_d_hani:
   *         type: integer
   *       disp_scall_c_tani:
   *         type: string
   *       disp_scall_c_hani:
   *         type: integer
   *       disp_scall_s_tani:
   *         type: string
   *       disp_scall_s_hani:
   *         type: integer
   *       text_mode:
   *         type: boolean
   *       course_type:
   *         type: string
   */
}

module.exports = SsKihonSetting
