'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmKonsaiGroupSyosai extends Model {
  static get table () {
    return 's_m_konsai_group_syosai'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupSyosaiCreate:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - konsai_group_id
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupSyosaiUpdate:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       data_update:
   *         type: object
   *     required:
   *       - konsai_group_id
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupSyosaiDelete:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *     required:
   *       - konsai_group_id
   *       - syo_koutei_code
   *       - seihin_code
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupSyosaiResponse:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       syo_koutei_code:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   */
}

module.exports = SmKonsaiGroupSyosai
