'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsCourse extends Model {
  static get table () {
    return 's_s_course'
  }

  static boot() {
    super.boot()
    // this.addHook('beforeCreate', async(SsCourseInstance) => {
    //   let lastSeq = await SsCourse.query().getMax('default_workset_id')
    //   SsCourseInstance.default_workset_id = lastSeq + 1
    // })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      shikibetu: 'required|string|max:10',
      set_date: 'required|string',
    }
  }

  static get rulesUpdate () {
    return {
      shikibetu: 'required|string|max:10',
      set_date: 'required|string',
    }
  }

  /**
   * @swagger
   * definition:
   *   SsCourseCreate:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   *     required:
   *       - shikibetu
   *       - set_date
   */

  /**
   * @swagger
   * definition:
   *   SsCourseUpdate:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   *       data_update:
   *         type: object
   *     required:
   *       - shikibetu
   *       - set_date
   */


  /**
   * @swagger
   * definition:
   *   SsCourseDelete:
   *     properties:
   *       shikibetu:
   *         type: string
   *     required:
   *       - shikibetu
   */


  /**
   * @swagger
   * definition:
   *   SsCourseResponse:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   */
}

module.exports = SsCourse
