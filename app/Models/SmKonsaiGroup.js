'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmKonsaiGroup extends Model {
  static get table () {
    return 's_m_konsai_group'
  }

  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(SmKonsaiGroupInstance) => {
      let lastSeq = await SmKonsaiGroup.query().getMax('konsai_group_id')
      SmKonsaiGroupInstance.konsai_group_id = lastSeq + 1
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupCreate:
   *     properties:
   *       konsai_group_name:
   *         type: string
   *     required:
   *       - konsai_group_name
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupUpdate:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       konsai_group_name:
   *         type: string
   *     required:
   *       - konsai_group_id
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupDelete:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *     required:
   *       - konsai_group_id
   */

  /**
   * @swagger
   * definition:
   *   SmKonsaiGroupResponse:
   *     properties:
   *       konsai_group_id:
   *         type: integer
   *       konsai_group_name:
   *         type: string
   */
}

module.exports = SmKonsaiGroup
