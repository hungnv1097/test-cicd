'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StaffFree extends Model {
  static get table () {
    return 'staff_free'
  }
}

module.exports = StaffFree
