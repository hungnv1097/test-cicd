'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmBefaftGroup extends Model {
  static get table () {
    return 's_m_befaft_group'
  }

  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(SmBefaftGroupInstance) => {
      let lastSeq = await SmBefaftGroup.query().getMax('befaft_group_id')
      SmBefaftGroupInstance.befaft_group_id = lastSeq + 1
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupCreate:
   *     properties:
   *       befaft_group_name:
   *         type: string
   *     required:
   *       - befaft_group_name
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupUpdate:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       befaft_group_name:
   *         type: string
   *     required:
   *       - befaft_group_id
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupDelete:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *     required:
   *       - befaft_group_id
   */

  /**
   * @swagger
   * definition:
   *   SmBefaftGroupResponse:
   *     properties:
   *       befaft_group_id:
   *         type: integer
   *       befaft_group_name:
   *         type: string
   */
}

module.exports = SmBefaftGroup
