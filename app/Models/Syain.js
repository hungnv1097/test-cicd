'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Syain extends Model {
  static get table () {
    return 'syain'
  }
}

module.exports = Syain
