'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet');
const SdSyoKoteiMainGannt = use('App/Models/SdSyoKoteiMainGannt');

class SmSouchiMst extends Model {
  static get table () {
    return 's_m_souchi_mst'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSouchiMstCreate:
   *     properties:
   *       souchi_code:
   *         type: string
   *       souchi_name:
   *         type: string
   *       daisu:
   *         type: integer
   *       biko:
   *         type: string
   *     required:
   *       - souchi_name
   */

  /**
   * @swagger
   * definition:
   *   SmSouchiMstUpdate:
   *     properties:
   *       souchi_code:
   *         type: string
   *       souchi_name:
   *         type: string
   *       daisu:
   *         type: integer
   *       biko:
   *         type: string
   *     required:
   *       - souchi_code
   */

  /**
   * @swagger
   * definition:
   *   SmSouchiMstDelete:
   *     properties:
   *       souchi_code:
   *         type: string
   *     required:
   *       - souchi_code
   */

  /**
   * @swagger
   * definition:
   *   SmSouchiMstResponse:
   *     properties:
   *       souchi_code:
   *         type: string
   *       souchi_name:
   *         type: string
   *       daisu:
   *         type: integer
   *       biko:
   *         type: string
   */
}

module.exports = SmSouchiMst
