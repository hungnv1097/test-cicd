'use strict'
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Redis = use('Redis')

class SmKouteiMst extends Model {
  static get table () {
    return 's_m_koutei_mst'
  }

  static boot() {
    super.boot()

    this.addHook('afterSave', async(smKouteiMstInstance) => {
      Redis.keys("SmKouteiMst_*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
    })

    this.addHook('afterDelete', async(smKouteiMstInstance) => {
      Redis.keys("SmKouteiMst_*", async function(err, rows) {
        for (var i in rows){
          await Redis.del(rows[i])
        }
      })
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmKouteiMstResponse:
   *     properties:
   *       koutei_code:
   *         type: string
   *       kotei_name:
   *         type: string
   *       biko:
   *         type: string
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiMstCreate:
   *     properties:
   *       koutei_code:
   *         type: string
   *       kotei_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - syohin_name
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiMstUpdate:
   *     properties:
   *       koutei_code:
   *         type: string
   *       kotei_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiMstDelete:
   *     properties:
   *       koutei_code:
   *         type: string
   *     required:
   *       - koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmKouteiMainResponse:
   *     properties:
   *       koutei_code:
   *         type: string
   *       koutei_name:
   *         type: string
   *       s_m_syokoutei_list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmSyokouteiSetMainResponse'
   *
   */

  s_m_syokoutei_list () {
    return this
      .belongsToMany('App/Models/SmSyokouteiSet', 'koutei_code', 'seihin_code', 'koutei_code', 'seihin_code')
      .pivotTable('s_m_koutei_set')
  }
}

module.exports = SmKouteiMst
