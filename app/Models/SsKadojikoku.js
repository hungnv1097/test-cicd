'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
class SsKadojikoku extends Model {
  static get table () {
    return 's_s_kadojikoku'
  }


  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }


  /**
   * @swagger
   * definition:
   *   SsKadojikokuCreate:
   *     properties:
   *       set_date:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       hi_kadobi:
   *         type: boolean
   *     required:
   *       - set_date
   *       - hi_kadobi
   */

  /**
   * @swagger
   * definition:
   *   SsKadojikokuUpdate:
   *     properties:
   *       set_date:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       hi_kadobi:
   *         type: boolean
   *     required:
   *       - set_date
   *       - hi_kadobi
   */

  /**
   * @swagger
   * definition:
   *   SsKadojikokuDelete:
   *     properties:
   *       han_name:
   *         type: string
   *     required:
   *       - han_name
   */

  /**
   * @swagger
   * definition:
   *   SsKadojikokuResponse:
   *     properties:
   *       set_date:
   *         type: string
   *       kadojikoku_from:
   *         type: string
   *       kadojikoku_to:
   *         type: string
   *       kyukei1_from:
   *         type: string
   *       kyukei1_to:
   *         type: string
   *       kyukei2_from:
   *         type: string
   *       kyukei2_to:
   *         type: string
   *       kyukei3_from:
   *         type: string
   *       kyukei3_to:
   *         type: string
   *       hi_kadobi:
   *         type: boolean
   */
}

module.exports = SsKadojikoku
