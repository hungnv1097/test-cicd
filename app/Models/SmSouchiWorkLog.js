'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const SmSyokouteiSet = use('App/Models/SmSyokouteiSet');
const SdSyoKoteiMainGannt = use('App/Models/SdSyoKoteiMainGannt');

class SmSouchiWorkLog extends Model {
  static get table () {
    return 'souchi_work_log'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }


  /**
   * @swagger
   * definition:
   *   SmSouchiWorkLogResponse:
   *     properties:
   *       souchi_code:
   *         type: string
   *       souchi_name:
   *         type: string
   *       min_start_datetime:
   *         type: string
   *       max_nouki_datetime:
   *         type: string
   *       sum_mae_setup_time:
   *         type: integer
   *       sum_std_setup_time:
   *         type: integer
   *       sum_ato_setup_time:
   *         type: integer
   */
}

module.exports = SmSouchiWorkLog
