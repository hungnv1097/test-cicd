'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsDispnameManager extends Model {
  static get table () {
    return 's_s_dispname_manager'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      gamen_code: 'required|string|max:20',
      komoku_code: 'required|string|max:20',
      hyoji_name: 'string|max:64',
      default_name: 'string|max:64',
      hyouji_flag: 'boolean',
    }
  }

  /**
   * @swagger
   * definition:
   *   SsDispnameManagerCreate:
   *     properties:
   *       gamen_code:
   *         type: string
   *       komoku_code:
   *         type: string
   *       hyoji_name:
   *         type: string
   *       default_name:
   *         type: string
   *       hyouji_flag:
   *         type: boolean
   *     required:
   *       - gamen_code
   *       - komoku_code
   */

  /**
   * @swagger
   * definition:
   *   SsDispnameManagerUpdate:
   *     properties:
   *       gamen_code:
   *         type: string
   *       komoku_code:
   *         type: string
   *       hyoji_name:
   *         type: string
   *       default_name:
   *         type: string
   *       hyouji_flag:
   *         type: boolean
   *     required:
   *       - gamen_code
   *       - komoku_code
   */

  /**
   * @swagger
   * definition:
   *   SsDispnameManagerDelete:
   *     properties:
   *       gamen_code:
   *         type: string
   *       komoku_code:
   *         type: string
   *     required:
   *       - gamen_code
   *       - komoku_code
   */

  /**
   * @swagger
   * definition:
   *   SsDispnameManagerResponse:
   *     properties:
   *       gamen_code:
   *         type: string
   *       komoku_code:
   *         type: string
   *       hyoji_name:
   *         type: string
   *       default_name:
   *         type: string
   *       hyouji_flag:
   *         type: boolean
   */
}

module.exports = SsDispnameManager
