'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsSystemSetting extends Model {
  static get table () {
    return 's_s_system_setting'
  }
  
  static get primaryKey () {
    return 'nafuda_riyo_settei'
  }
}

module.exports = SsSystemSetting
