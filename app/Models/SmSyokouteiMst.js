'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSyokouteiMst extends Model {
  static get table () {
    return 's_m_syokoutei_mst'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSyokouteiMstCreate:
   *     properties:
   *       syo_koutei_code:
   *         type: string
   *       syo_kotei_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - syo_kotei_name
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiMstUpdateSetCreate:
   *     properties:
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_set:
   *         type: array
   *         items:
   *            type: object
   *            $ref: '#/definitions/SmSyokouteiSetCreate'
   *     required:
   *       - syo_koutei_code
   */


  /**
   * @swagger
   * definition:
   *   SmSyokouteiMstUpdate:
   *     properties:
   *       syo_koutei_code:
   *         type: string
   *       syo_kotei_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiMstResponse:
   *     properties:
   *       syo_koutei_code:
   *         type: string
   *       syo_kotei_name:
   *         type: string
   *       biko:
   *         type: string
   */
}

module.exports = SmSyokouteiMst
