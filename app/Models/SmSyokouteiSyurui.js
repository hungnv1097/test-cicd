'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSyokouteiSyurui extends Model {
    static get table () {
        return 's_m_syokoutei_syurui'
    }

    static get primaryKey () {
        return 'syo_koutei_syurui_seq'
    }

    static boot() {
      super.boot()
      let i = 0;
      this.addHook('beforeCreate', async (SmSyokouteiSyuruiInstance) => {
        let lastSeq = await SmSyokouteiSyurui.query().getMax('syokoutei_syurui_junjo');
        i++;
        SmSyokouteiSyuruiInstance.syokoutei_syurui_junjo = lastSeq + i;
      })
    }

  s_m_syokoutei_koho_list () {
    return this
      .hasMany('App/Models/SmSyokouteiKoho', 'syo_koutei_syurui_seq', 'syo_koutei_syurui_seq')
  }
}

module.exports = SmSyokouteiSyurui
