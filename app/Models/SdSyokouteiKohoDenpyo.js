'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SdSyokouteiKohoDenpyo extends Model {
  static get table () {
    return 's_d_syokoutei_koho_denpyo'
  }

  static get primaryKey () {
    return null
  }

  static get rules () {
    return {
      seihin_code: 'required|string|max:20',
      koutei_code: 'required|string|max:8',
      koteikeikaku_no: 'required|string|max:20',
      syo_koutei_syurui_seq: 'required|integer',
      syo_koutei_koho_seq: 'integer',
      saiyo_flag: 'boolean',
      syo_koutei_koho_memo: 'string|max:128'
    }
  }

}

module.exports = SdSyokouteiKohoDenpyo
