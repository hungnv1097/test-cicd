'use strict'

const Util = require('../helpers/Util')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const SdSyoKoteiMainSyain = use('App/Models/SdSyoKoteiMainSyain')

class SdSyoKoteiMainGannt extends Model {
  static get table () {
    return 's_d_syo_kotei_main_gannt'
  }

  static get computed () {
    return ['s-d-syo-kotei-main-syain-list']
  }

  static boot() {
    super.boot()
    let i =0;
    this.addHook('beforeCreate', async(sdSyoKoteiMainGanntInstance) => {
      // sdSyoKoteiMainGanntInstance.syo_koutei_seq = 0 // Fix this number, reverse using for future.
      sdSyoKoteiMainGanntInstance = Util.formatTime(sdSyoKoteiMainGanntInstance);
      console.log("sdSyoKoteiMainGanntInstance", sdSyoKoteiMainGanntInstance)
      let lastSeq = await SdSyoKoteiMainGannt.query().getMax('seq');
      i++;
      sdSyoKoteiMainGanntInstance.seq = lastSeq + i;
    });

    this.addHook('afterFetch', async(stSeisansijiInstances) => {
      for (let stSeisansijiInstance of stSeisansijiInstances) {
        const mainSyain =  await SdSyoKoteiMainSyain.query()
          .leftJoin('syain', 'syain.id', 's_d_syo_kotei_main_syain.syain_id')
          .where({
            'seihin_code': stSeisansijiInstance.seihin_code,
            'koteikeikaku_no': stSeisansijiInstance.koteikeikaku_no, 
            'koutei_code': stSeisansijiInstance.koutei_code, 
            'seq': stSeisansijiInstance.seq
          })
          .select(['syain_name', 'id', 'freestaff_flag'])
          .fetch()
        stSeisansijiInstance.$sideLoaded.mainSyain = mainSyain
      }
    });
  }

  getSDSyoKoteiMainSyainList () {
    return this.mainSyain
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      koteikeikaku_no: 'required|string|max:20',
      seihin_code: 'required|string|max:20',
      koutei_code: 'required|string|max:20',
      syo_koutei_code: 'required|string|max:20',
      syo_koutei_seq: 'required|integer',
      syo_koutei_junjo: 'required|integer',
      main_sub: 'string|max:1',
      proc_from: 'string',
      proc_to: 'string',
      std_proc_syohin_su: 'integer',
      proc_sokudo: 'integer',
      lock_flag: 'boolean',
      syo_koutei_syurui_seq: 'required|integer',
      syo_koutei_koho_seq: 'required|integer',
    }
  }

  static get rulesUpdate () {
    return {
      koteikeikaku_no: 'string|max:20',
      seihin_code: 'string|max:20',
      koutei_code: 'string|max:20',
      syo_koutei_code: 'string|max:20',
      syo_koutei_seq: 'integer',
      syo_koutei_junjo: 'integer',
      seq: 'required|integer',
      main_sub: 'string|max:1',
      proc_from: 'string',
      proc_to: 'string',
      std_proc_syohin_su: 'integer',
      proc_sokudo: 'integer',
      lock_flag: 'boolean',
      syo_koutei_syurui_seq: 'required|integer',
      syo_koutei_koho_seq: 'required|integer',
    }
  }
  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntCreate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       std_proc_syohin_su:
   *         type: string
   *       proc_sokudo:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *     required:
   *       - koteikeikaku_no
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - syo_koutei_junjo
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntUpdate:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       std_proc_syohin_su:
   *         type: string
   *       proc_sokudo:
   *         type: string
   *       lock_flag:
   *         type: boolean
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntMultipleCreateUpdate:
   *     properties:
   *        syokotei_main_gannt_list:
   *          type: array
   *          items:
   *            type: object
   *            $ref: '#/definitions/SdSyoKoteiMainGanntUpdate'
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntDelete:
   *     properties:
   *       seq:
   *         type: integer
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntResponse:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       std_proc_syohin_su:
   *         type: integer
   *       proc_sokudo:
   *         type: integer
   *       lock_flag:
   *         type: boolean
   */

  /**
   * @swagger
   * definition:
   *   SdSyoKoteiMainGanntMainResponse:
   *     properties:
   *       koteikeikaku_no:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       std_proc_syohin_su:
   *         type: string
   *       proc_sokudo:
   *         type: string
   *       lock_flag:
   *         type: boolean
   */
}

module.exports = SdSyoKoteiMainGannt
