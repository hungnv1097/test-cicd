'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSeihinMst extends Model {
  static get table () {
    return 's_m_seihin_mst'
  }

  static get primaryKey () {
    return null
  }

  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSeihinMstCreate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       seihin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *     required:
   *       - syohin_name
   */

  /**
   * @swagger
   * definition:
   *   SmSeihinMstUpdate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       seihin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *     required:
   *       - syohin_code
   *       - syohin_name
   */

  /**
   * @swagger
   * definition:
   *   SmSeihinMstDelete:
   *     properties:
   *       seihin_code:
   *         type: string
   *     required:
   *       - seihin_code
   */

  /**
   * @swagger
   * definition:
   *   SmSeihinMstResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       seihin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   */
  /**
   * @swagger
   * definition:
   *   SmSeihinMstMainResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       seihin_name:
   *         type: string
   *       def_kado_jikangai_flag:
   *         type: boolean
   *       biko:
   *         type: string
   *       s_m_koutei_list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmKouteiSetMainResponse'
   */

  s_m_koutei_list () {
    return this
      .hasMany('App/Models/SmKouteiSet', 'seihin_code', 'seihin_code')
  }

  seihin () {
    return this.hasOne('App/Models/SmSeihinMst', 'seihin_code', 'seihin_code')
  }

}

module.exports = SmSeihinMst
