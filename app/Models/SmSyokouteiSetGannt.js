"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class SmSyokouteiSetGannt extends Model {
  static get table() {
    return "s_m_syokoutei_set_gannt";
  }

  static boot() {
    super.boot();
    this.addHook("beforeCreate", async smSyokouteiSetGanntInstance => {
      let lastSeq = (await SmSyokouteiSetGannt.query().getMax("seq")) || 0;
      smSyokouteiSetGanntInstance.seq =
        smSyokouteiSetGanntInstance.seq || lastSeq + 1;
    });
  }

  static get primaryKey() {
    return null;
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetGanntCreate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: integer
   *       proc_to:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       proc_sokudo:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       color:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   *       - syo_koutei_seq
   *       - seq
   *       - syo_koutei_junjo
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetGanntUpdate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: integer
   *       proc_to:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       proc_sokudo:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       color:
   *         type: string
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetGanntMultipleCreateUpdate:
   *     properties:
   *        syokoutei_set_gannt_list:
   *          type: array
   *          items:
   *            type: object
   *            $ref: '#/definitions/SmSyokouteiSetGanntUpdate'
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetGanntDelete:
   *     properties:
   *       seq:
   *         type: integer
   *     required:
   *       - seq
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetGanntResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       seq:
   *         type: integer
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: integer
   *       proc_to:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       proc_sokudo:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       color:
   *         type: string
   */
}

module.exports = SmSyokouteiSetGannt;
