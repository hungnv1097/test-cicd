'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSyokouteiKoho extends Model {
  static get table () {
    return 's_m_syokoutei_koho'
  }

  static get primaryKey () {
    return 'syo_koutei_koho_seq'
  }

  static boot() {
    super.boot()
    let i = 0;
    this.addHook('beforeCreate', async (SmSyokouteiKohoInstance) => {
      let lastSeq = await SmSyokouteiKoho.query().getMax('syokoutei_koho_junjo');
      i++;
      SmSyokouteiKohoInstance.syokoutei_koho_junjo = lastSeq + i;
    })
  }

  s_m_syokoutei_list () {
    return this
      .hasMany('App/Models/SmSyokouteiSet', 'syo_koutei_koho_seq', 'syo_koutei_koho_seq')
  }

  s_d_syokoutei_koho_denpyo () {
    return this.hasOne('App/Models/SdSyokouteiKohoDenpyo', 'syo_koutei_koho_seq', 'syo_koutei_koho_seq')
  }
}

module.exports = SmSyokouteiKoho
