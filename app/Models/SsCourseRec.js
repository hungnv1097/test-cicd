'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsCourseRec extends Model {
  static get table () {
    return 's_s_course_rec'
  }

  static boot() {
    super.boot()
    this.addHook('beforeCreate', async(SsCourseRecInstance) => {
      let lastSeq = await SsCourseRec.query().getMax('no')
      SsCourseRecInstance.no = lastSeq + 1
    })
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  static get rules () {
    return {
      shikibetu: 'required|string|max:10',
      set_date: 'required|string',
      set_date_original: 'required|string',
      // no: 'required|integer',
      course_from: 'string',
      course_to: 'string',
      course_time_from: 'string|max:4',
      course_time_to: 'string|max:4',
      kouseihiritu: 'integer',
    }
  }

  static get rulesUpdate () {
    return {
      shikibetu: 'string|max:10',
      set_date: 'string',
      no: 'required|integer',
      course_from: 'string',
      course_to: 'string',
      course_time_from: 'string|max:4',
      course_time_to: 'string|max:4',
      kouseihiritu: 'integer',
    }
  }

  /**
   * @swagger
   * definition:
   *   SsCourseRecCreate:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   *       course_from:
   *         type: string
   *       course_to:
   *         type: string
   *       course_time_from:
   *         type: string
   *       course_time_to:
   *         type: string
   *       kouseihiritu:
   *         type: integer
   *     required:
   *       - shikibetu
   *       - set_date
   *       - no
   */

  /**
   * @swagger
   * definition:
   *   SsCourseRecUpdate:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   *       course_from:
   *         type: string
   *       course_to:
   *         type: string
   *       course_time_from:
   *         type: string
   *       course_time_to:
   *         type: string
   *       kouseihiritu:
   *         type: integer
   *     required:
   *       - shikibetu
   *       - set_date
   *       - no
   */


  /**
   * @swagger
   * definition:
   *   SsCourseRecDelete:
   *     properties:
   *       no:
   *         type: integer
   *     required:
   *       - shikibetu
   *       - no
   */


  /**
   * @swagger
   * definition:
   *   SsCourseRecResponse:
   *     properties:
   *       shikibetu:
   *         type: string
   *       set_date:
   *         type: string
   *       no:
   *         type: integer
   *       course_from:
   *         type: string
   *       course_to:
   *         type: string
   *       course_time_from:
   *         type: string
   *       course_time_to:
   *         type: string
   *       kouseihiritu:
   *         type: integer
   */
}

module.exports = SsCourseRec
