'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TstSeisansiji extends Model {
  static get table() {
    return 't_s_t_seisansiji'
  }

  /**
   * @swagger
   * definition:
   *   TstSeisansijiCreate:
   *     properties:
   *       fromdate:
   *          type: string
   *       koteikeikaku_no:
   *          type: string
   *       seihin_code:
   *         type: string
   *       seihin_suuryou:
   *         type: integer
   *       seihin_nouki:
   *         type: string
   *       seihin_name_add:
   *         type: string
   *     required:
   *       - koteikeikaku_no
   */

  /**
   * @swagger
   * definition:
   *   TstSeisansijiDelete:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koteikeikaku_no:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koteikeikaku_no
   */


  /**
   * @swagger
   * definition:
   *   TstSeisansijiUpdate:
   *     properties:
   *       seihin_id:
   *         type: integer
   *       seihin_name:
   *         type: string
   *       biko:
   *         type: string
   *       seihin_code:
   *         type: string
   *       koteikeikaku_no:
   *         type: string
   *       koutei_group_add:
   *         type: string
   *       koutei_group:
   *         type: array
   *       proc_date:
   *         type: string
   *     required:
   *       - seihin_id
   *       - seihin_name
   *       - biko
   *       - seihin_code
   *       - koteikeikaku_no
   */
}

module.exports = TstSeisansiji
