'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Login extends Model {
  static get table () {
    return 'login'
  }

  static get updatedAtColumn () {
    return 'update_date'
  }
}

module.exports = Login
