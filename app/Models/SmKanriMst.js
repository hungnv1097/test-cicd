'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmKanriMst extends Model {
  static get table () {
    return 's_m_kanri_mst'
  }


  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

}

module.exports = SmKanriMst
