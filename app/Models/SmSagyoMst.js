'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSagyoMst extends Model {
  static get table () {
    return 's_m_sagyo_mst'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }

  /**
   * @swagger
   * definition:
   *   SmSagyoMstCreate:
   *     properties:
   *       sagyo_code:
   *         type: string
   *       sagyo_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - sagyo_name
   */

  /**
   * @swagger
   * definition:
   *   SmSagyoMstUpdate:
   *     properties:
   *       sagyo_code:
   *         type: string
   *       sagyo_name:
   *         type: string
   *       biko:
   *         type: string
   *     required:
   *       - sagyo_code
   */

  /**
   * @swagger
   * definition:
   *   SmSagyoMstDelete:
   *     properties:
   *       sagyo_code:
   *         type: string
   *     required:
   *       - sagyo_code
   */

  /**
   * @swagger
   * definition:
   *   SmSagyoMstResponse:
   *     properties:
   *       sagyo_code:
   *         type: string
   *       sagyo_name:
   *         type: string
   *       biko:
   *         type: string
   */
}

module.exports = SmSagyoMst
