'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SmSouchiMainGannt extends Model {
  static get table () {
    return 'souchi_main_gannt'
  }

  static get primaryKey () {
    return null
  }
  // static get createdAtColumn () {
  //   return null
  // }
  // static get updatedAtColumn () {
  //   return null
  // }


  /**
   * @swagger
   * definition:
   *   SmSouchiMainGanntResponse:
   *     properties:
   *       souchi_code:
   *         type: string
   *       souchi_name:
   *         type: string
   *       main_sub:
   *         type: string
   *       proc_from:
   *         type: string
   *       proc_to:
   *         type: string
   *       color:
   *         type: string
   */
}

module.exports = SmSouchiMainGannt
