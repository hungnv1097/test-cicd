"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const SmSyokouteiSyoList = use("App/Models/SmSyokouteiSyoList");
const SmSyokouteiSetGannt = use("App/Models/SmSyokouteiSetGannt");
const Logger = use("Logger");
const Redis = use("Redis");
const Util = use("App/helpers/Util");

class SmSyokouteiSet extends Model {
  static get table() {
    return "s_m_syokoutei_set";
  }

  static get computed() {
    return ["s-m-syokoutei-syo-list", "s-m-syokoutei-set-gannt-list"];
  }

  static boot() {
    super.boot();
    this.addHook("beforeCreate", async smSyokouteiSetInstance => {
      smSyokouteiSetInstance.syo_koutei_seq = 0; // Fix seq to 0
      let lastSeq = await SmSyokouteiSet.query().getMax("syo_koutei_junjo");
      smSyokouteiSetInstance.syo_koutei_junjo = lastSeq + 1;
    });

    this.addHook("afterFetch", async smSyokouteiSetInstances => {
      for (let smSyokouteiSetInstance of smSyokouteiSetInstances) {
        let smSyokouteiSyoList;
        let smSyokouteiSetGannts;
        // let key_cache = 'SmSyokouteiSyoList_' + smSyokouteiSetInstance.seihin_code + '_' + smSyokouteiSetInstance.koutei_code + '_' + smSyokouteiSetInstance.syo_koutei_code
        // const cachedData = await Redis.get(key_cache)

        let startTime = Date.now();
        smSyokouteiSyoList = await SmSyokouteiSyoList.query()
          .with("sagyo")
          .with("souchi")
          .where({
            seihin_code: smSyokouteiSetInstance.seihin_code,
            koutei_code: smSyokouteiSetInstance.koutei_code,
            syo_koutei_code: smSyokouteiSetInstance.syo_koutei_code,
            syo_koutei_koho_seq: smSyokouteiSetInstance.syo_koutei_koho_seq,
            syo_koutei_syurui_seq: smSyokouteiSetInstance.syo_koutei_syurui_seq
          })
          .fetch();
        // if (cachedData) {
        //   smSyokouteiSyoList = JSON.parse(cachedData)
        //   console.log("==================SmSyokouteiSyoList in Cache")
        // } else {
        //   smSyokouteiSyoList = await SmSyokouteiSyoList.query().with('sagyo').with('souchi')
        //     .where({
        //       'seihin_code': smSyokouteiSetInstance.seihin_code,
        //       'koutei_code': smSyokouteiSetInstance.koutei_code,
        //       'syo_koutei_code': smSyokouteiSetInstance.syo_koutei_code,
        //     })
        //     .fetch();
        //   await Redis.set(key_cache, JSON.stringify(smSyokouteiSyoList))
        // }
        let endTime = Date.now();
        smSyokouteiSetInstance.$sideLoaded.smSyokouteiSyoList = smSyokouteiSyoList;

        //Load set gannt
        // let key_cache_set_gannt =
        //   "SmSyokouteiSetGannt" +
        //   smSyokouteiSetInstance.seihin_code +
        //   "_" +
        //   smSyokouteiSetInstance.koutei_code +
        //   "_" +
        //   smSyokouteiSetInstance.syo_koutei_code +
        //   "_" +
        //   smSyokouteiSetInstance.syo_koutei_seq +
        //   "_" +
        //   smSyokouteiSetInstance.syo_koutei_junjo;

        // const cachedDataSetGannt = await Redis.get(key_cache_set_gannt);

        let startTime1 = Date.now();
        smSyokouteiSetGannts = await SmSyokouteiSetGannt.query()
          .where({
            seihin_code: smSyokouteiSetInstance.seihin_code,
            koutei_code: smSyokouteiSetInstance.koutei_code,
            syo_koutei_code: smSyokouteiSetInstance.syo_koutei_code,
            syo_koutei_seq: smSyokouteiSetInstance.syo_koutei_seq,
            syo_koutei_junjo: smSyokouteiSetInstance.syo_koutei_junjo
          })
          .fetch();
        // if (cachedDataSetGannt) {
        //   smSyokouteiSetGannts = JSON.parse(cachedDataSetGannt);
        //   console.log("==================SmSyokouteiSetGannt in Cache");
        // } else {
        //   smSyokouteiSetGannts = await SmSyokouteiSetGannt.query()
        //     .where({
        //       seihin_code: smSyokouteiSetInstance.seihin_code,
        //       koutei_code: smSyokouteiSetInstance.koutei_code,
        //       syo_koutei_code: smSyokouteiSetInstance.syo_koutei_code,
        //       syo_koutei_seq: smSyokouteiSetInstance.syo_koutei_seq,
        //       syo_koutei_junjo: smSyokouteiSetInstance.syo_koutei_junjo
        //     })
        //     .fetch();
        //   await Redis.set(
        //     key_cache_set_gannt,
        //     JSON.stringify(smSyokouteiSetGannts)
        //   );
        // }
        let endTime1 = Date.now();
        smSyokouteiSetInstance.$sideLoaded.smSyokouteiSetGannts = smSyokouteiSetGannts;
      }
    });

    this.addHook("afterSave", async smSyokouteiSetInstances => {
      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
    });

    this.addHook("afterDelete", async smSyokouteiSetInstances => {
      Redis.keys("SmSyokouteiSet_*", async function (err, rows) {
        for (var i in rows) {
          await Redis.del(rows[i]);
        }
      });
    });
  }

  getSMSyokouteiSyoList() {
    return this.smSyokouteiSyoList;
  }

  getSMSyokouteiSetGanntList() {
    return this.smSyokouteiSetGannts;
  }

  static get primaryKey() {
    return null;
  }

  // static get createdAtColumn() {
  //   return null
  // }
  // static get updatedAtColumn() {
  //   return null
  // }

  static get rules() {
    return {
      seihin_code: "required|string|max:20",
      koutei_code: "required|string|max:20",
      syo_koutei_code: "required|string|max:20",
      syo_koutei_junjo: "integer",
      syo_koutei_seq: "integer",
      std_proc_syohin_su: "integer",
      max_proc_syohin_su: "integer",
      min_proc_syohin_su: "integer",
      std_proc_sokudo: "integer",
      max_proc_sokudo: "integer",
      min_proc_sokudo: "integer",
      std_kadoritu: "integer",
      std_setup_time: "integer",
      mae_setup_time: "integer",
      ato_setup_time: "integer",
      hituyo_ninzu: "integer",
      highskill_ninzu: "integer",
      post_start_bef: "integer",
      post_start_aft: "integer",
      post_end_bef: "integer",
      post_end_aft: "integer",
      post_heiretu_flag: "integer",
      syo_koutei_syurui_seq: "integer",
      syo_koutei_koho_seq: "integer"
    };
  }

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetCreate:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       std_proc_syohin_su:
   *         type: integer
   *       max_proc_syohin_su:
   *         type: integer
   *       min_proc_syohin_su:
   *         type: integer
   *       std_proc_sokudo:
   *         type: integer
   *       max_proc_sokudo:
   *         type: integer
   *       min_proc_sokudo:
   *         type: integer
   *       std_kadoritu:
   *         type: integer
   *       std_setup_time:
   *         type: integer
   *       mae_setup_time:
   *         type: integer
   *       ato_setup_time:
   *         type: integer
   *       hituyo_ninzu:
   *         type: integer
   *       highskill_ninzu:
   *         type: integer
   *       post_start_bef:
   *         type: integer
   *       post_start_aft:
   *         type: integer
   *       post_end_bef:
   *         type: integer
   *       post_end_aft:
   *         type: integer
   *       post_heiretu_flag:
   *         type: boolean
   *       sagyo_mst_souchi_mst:
   *         type: array
   *         items:
   *            type: object
   *            properties:
   *                sagyo_code:
   *                    type: string
   *                souchi_code:
   *                    type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetUpdate:
   *     properties:
   *       syo_koutei_junjo:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       max_proc_syohin_su:
   *         type: integer
   *       min_proc_syohin_su:
   *         type: integer
   *       std_proc_sokudo:
   *         type: integer
   *       max_proc_sokudo:
   *         type: integer
   *       min_proc_sokudo:
   *         type: integer
   *       std_kadoritu:
   *         type: integer
   *       std_setup_time:
   *         type: integer
   *       mae_setup_time:
   *         type: integer
   *       ato_setup_time:
   *         type: integer
   *       hituyo_ninzu:
   *         type: integer
   *       highskill_ninzu:
   *         type: integer
   *       post_start_bef:
   *         type: integer
   *       post_start_aft:
   *         type: integer
   *       post_end_bef:
   *         type: integer
   *       post_end_aft:
   *         type: integer
   *       post_heiretu_flag:
   *         type: boolean
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetUpdateWorkDevice:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       sagyo_mst_souchi_mst:
   *         type: array
   *         items:
   *            type: object
   *            properties:
   *                sagyo_code:
   *                    type: string
   *                souchi_code:
   *                    type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetDelete:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *     required:
   *       - seihin_code
   *       - koutei_code
   *       - syo_koutei_code
   */

  /**
   * @swagger
   * definition:
   *   SmSyokouteiSetResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei_code:
   *         type: string
   *       syo_koutei_seq:
   *         type: integer
   *       syo_koutei_junjo:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       max_proc_syohin_su:
   *         type: integer
   *       min_proc_syohin_su:
   *         type: integer
   *       std_proc_sokudo:
   *         type: integer
   *       max_proc_sokudo:
   *         type: integer
   *       min_proc_sokudo:
   *         type: integer
   *       std_kadoritu:
   *         type: integer
   *       std_setup_time:
   *         type: integer
   *       mae_setup_time:
   *         type: integer
   *       ato_setup_time:
   *         type: integer
   *       hituyo_ninzu:
   *         type: integer
   *       highskill_ninzu:
   *         type: integer
   *       post_start_bef:
   *         type: integer
   *       post_start_aft:
   *         type: integer
   *       post_end_bef:
   *         type: integer
   *       post_end_aft:
   *         type: integer
   *       post_heiretu_flag:
   *         type: boolean
   *
   * @swagger
   * definition:
   *   SmSyokouteiSetMainResponse:
   *     properties:
   *       seihin_code:
   *         type: string
   *       koutei_code:
   *         type: string
   *       syo_koutei:
   *         type: object
   *         $ref: '#/definitions/SmSyokouteiMstResponse'
   *       syo_koutei_seq:
   *         type: integer
   *       std_proc_syohin_su:
   *         type: integer
   *       max_proc_syohin_su:
   *         type: integer
   *       min_proc_syohin_su:
   *         type: integer
   *       std_proc_sokudo:
   *         type: integer
   *       max_proc_sokudo:
   *         type: integer
   *       min_proc_sokudo:
   *         type: integer
   *       std_kadoritu:
   *         type: integer
   *       std_setup_time:
   *         type: integer
   *       mae_setup_time:
   *         type: integer
   *       ato_setup_time:
   *         type: integer
   *       hituyo_ninzu:
   *         type: integer
   *       highskill_ninzu:
   *         type: integer
   *       post_start_bef:
   *         type: integer
   *       post_start_aft:
   *         type: integer
   *       post_end_bef:
   *         type: integer
   *       post_end_aft:
   *         type: integer
   *       post_heiretu_flag:
   *         type: boolean
   *       s-m-syokoutei-syo-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmSyokouteiSyoListMainResponse'
   *       s-m-syokoutei-set-gannt-list:
   *         type: array
   *         items:
   *             type: object
   *             $ref: '#/definitions/SmSyokouteiSetGanntResponse'
   *
   */
  syo_koutei() {
    return this.hasOne(
      "App/Models/SmSyokouteiMst",
      "syo_koutei_code",
      "syo_koutei_code"
    );
  }
}

module.exports = SmSyokouteiSet;
