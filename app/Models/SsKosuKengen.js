'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SsKosuKengen extends Model {
  static get table () {
    return 's_s_kosu_kengen'
  }
}

module.exports = SsKosuKengen
